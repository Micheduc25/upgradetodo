import 'package:flutter/material.dart';
import 'package:upgrade_to_do/utils/periods.dart';
import 'package:upgrade_to_do/utils/settings.dart';

class HomeScreenSettings {
  HomeScreenSettings(
      {@required this.title,
      @required this.userId,
      @required this.settings,
      this.showDrawer = true,
      this.adminMode = false,
      this.disableTaskCheck = false,
      this.isSupervisor = false,
      this.isSuperAdmin = false,
      @required this.supId,
      @required this.isSupervisorMode,
      @required this.userRole,
      @required this.offlineMode,
      @required this.hasSupervisor,
      @required this.adminId,
      @required this.period,
      this.initialTab = 0,
      @required this.userName});

  final String title;
  final bool isSupervisorMode;

  final String userName;
  final bool isSuperAdmin;
  final int supId;
  final int userId;
  final Settings settings;
  final bool showDrawer;
  final bool adminMode;
  final bool disableTaskCheck;
  final bool isSupervisor;
  final String userRole;
  final bool offlineMode;
  final bool hasSupervisor;
  final int adminId;
  final Periods period;
  final int initialTab;
}
