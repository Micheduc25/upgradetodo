///this class defines methods which function in validating input fields' values

class Validator {
  static String emailValidator(String email) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(email))
      return 'Entrez une addresse email valide';
    else
      return null;
  }

  static String passwordValidator(String password) {
    if (password.isEmpty) {
      return "Votre mot de passe ne doit pas etre null";
    } else if (password.length < 4) {
      return "Votre mot de passe doit contenir au moin 8 caractères";
    } else
      return null;
  }

  static String textValidator(String value) {
    if (value.isEmpty) {
      return "Cette entrée ne doit pas etre vide";
    } else {
      return null;
    }
  }
}

// {"password":"michra25","updated_at":"2020-05-05 19:52:15","created_at":"2020-05-05 19:52:15","username":"NdjockJunior","id":506}
