import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

class Settings {
  Settings(StreamingSharedPreferences prefs)
      : userName = prefs.getString("userName", defaultValue: "default"),
        password = prefs.getString("password", defaultValue: "default"),
        serverUrl = prefs.getString("serverUrl",
            defaultValue: "http://192.168.0.101:8000"),
        offlineMode = prefs.getBool("offlineMode", defaultValue: false),
        firstLogin = prefs.getBool("firstLogin", defaultValue: true);

  Preference<String> userName;
  Preference<String> password;
  Preference<String> serverUrl;
  Preference<bool> offlineMode;
  Preference<bool> firstLogin;
}
