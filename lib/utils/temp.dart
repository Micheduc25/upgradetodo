import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:upgrade_to_do/main.dart';

class PopMenuTest extends StatefulWidget {
  @override
  _PopMenuTestState createState() => _PopMenuTestState();
}

class _PopMenuTestState extends State<PopMenuTest>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  double maxValue;

  @override
  void initState() {
    maxValue = .5;
    controller = new AnimationController(
        vsync: this, duration: Duration(milliseconds: 1000));
    controller.addListener(() {
      if (mounted) setState(() {});
    });

    // controller.addStatusListener((status) {

    // });

    controller.forward();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material App Bar'),
          actions: <Widget>[
            PopupMenuButton<String>(
              icon: Icon(
                Icons.more_vert,
                size: 30,
              ),
              itemBuilder: (context) => [
                PopupMenuItem(
                  value: "val0",
                  child: PopupMenuButton(
                    offset: Offset(100, 0),
                    icon: Icon(
                      Icons.record_voice_over,
                      size: 30,
                      color: upgradeOrange,
                    ),
                    itemBuilder: (context) => [
                      PopupMenuItem<String>(
                        value: "val1",
                        child: Text("Choice here"),
                      )
                    ],
                    onSelected: (val) {
                      Navigator.of(context).pop();
                    },
                    onCanceled: () {
                      Navigator.of(context).pop();
                    },
                  ),
                )
              ],
              onSelected: (value) {
                print("selected");
                // Navigator.of(context).pop();
              },
            )
          ],
        ),
        body: _Home(controller: controller),
      ),
    );
  }
}

class _Home extends StatelessWidget {
  const _Home({
    Key key,
    @required this.controller,
  }) : super(key: key);

  final AnimationController controller;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            CircularPercentIndicator(
              backgroundColor: upgradeOrange,
              progressColor: upgradeBrown,
              circularStrokeCap: CircularStrokeCap.round,
              lineWidth: 10,
              radius: MediaQuery.of(context).size.width * 0.4,
              percent:
                  // 0.7,
                  controller.value,
              center: Text(
                '${(controller.value * 100).floor()}%',
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(
              height: 40,
            ),
            RaisedButton(
                child: Text("Press Me"),
                onPressed: () {
                  if (controller.status == AnimationStatus.completed ||
                      controller.status == AnimationStatus.dismissed) {
                    print("in this status is ${controller.status}");
                    controller.forward(from: 0);
                  }
                })
          ],
        ),
      ),
    );
  }
}
