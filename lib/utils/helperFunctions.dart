import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'dart:convert';

import 'package:upgrade_to_do/models/taskModel.dart';
import 'package:upgrade_to_do/models/userModel.dart';
import 'package:upgrade_to_do/providers/adminTaskProvider.dart';
import 'package:upgrade_to_do/providers/taskProvider.dart';
import 'package:upgrade_to_do/utils/Config.dart';
import 'package:upgrade_to_do/utils/periods.dart';
import 'package:upgrade_to_do/utils/time.dart';

///this defines the order in which tasks have to be ordered
enum TaskOrder { PriorityUp, PriorityDown, Alphabetic, Date }

class HelperFunction {
  static Future<http.Response> postData(
      String url, Map<String, dynamic> dataMap) async {
    final client = http.Client();
    Uri parsedUri = Uri.parse(url);
    try {
      final res = client
          .post(parsedUri,
              headers: <String, String>{
                'Content-Type': 'application/json; charset=UTF-8',
              },
              body: jsonEncode(dataMap))
          .timeout(Duration(seconds: 30), onTimeout: () {
        return null;
      }).catchError((err) {
        if (err is SocketException) {
          print("Socket exception occured");
        } else
          print("An error occured here in trying to post data");
      });

      return res;
    } catch (err) {
      print("an error occured --->  $err");
      return null;
    } finally {
      client.close();
    }
  }

  static Future<http.Response> putData(
      String url, Map<String, dynamic> dataMap) async {
    final client = http.Client();
    Uri parsedUri = Uri.parse(url);
    try {
      final res = client
          .put(parsedUri,
              headers: <String, String>{
                'Content-Type': 'application/json; charset=UTF-8',
              },
              body: jsonEncode(dataMap))
          .timeout(Duration(seconds: 30), onTimeout: () {
        return null;
      }).catchError((err) {
        if (err is SocketException) {
          print("Socket exception occured");
        } else
          print("An error occured here in trying to post data");
      });

      return res;
    } catch (err) {
      print("an error occured --->  $err");
      return null;
    } finally {
      client.close();
    }
  }

  static Future<http.Response> getData(String url) async {
    final client = http.Client();
    Uri parsedUri = Uri.parse(url);
    try {
      final res = client
          .get(
        parsedUri,
        // headers: <String, String>{
        //   'Content-Type': 'application/json; charset=UTF-8',
        // },
      )
          .timeout(Duration(seconds: 30), onTimeout: () {
        return null;
      }).catchError((err) {
        if (err is SocketException) {
          print("Socket exception occured");
        } else
          print("An error occured here in trying to get data");

        return null;
      });

      return res;
    } catch (err) {
      print("an error occured --->  $err");
      return null;
    } finally {
      client.close();
    }
  }

  static Future<http.Response> deleteData(String url) async {
    final client = http.Client();
    Uri parsedUri = Uri.parse(url);
    try {
      final res = client
          .delete(
        parsedUri,
      )
          .timeout(Duration(minutes: 1), onTimeout: () {
        return null;
      }).catchError((err) {
        if (err is SocketException) {
          print("Socket exception occured");
        } else
          print("An error occured here in trying to delete data");
      });

      return res;
    } catch (err) {
      print("an error occured --->  $err");
      return null;
    } finally {
      client.close();
    }
  }

  static TaskPriority getTaskPriorityToAppFormat(String priority) {
    if (priority == "A faire")
      return TaskPriority.ToBeDone;
    else if (priority == "Important")
      return TaskPriority.Important;
    else if (priority == "Urgent")
      return TaskPriority.Urgent;
    else if (priority == "Urgent et Important")
      return TaskPriority.UrgentAndImportant;
    return TaskPriority.ToBeDone;
  }

  static String getTaskPriorityToServerFormat(TaskPriority priority) {
    if (priority == TaskPriority.ToBeDone)
      return "A faire";
    else if (priority == TaskPriority.Important)
      return "Important";
    else if (priority == TaskPriority.Urgent)
      return "Urgent";
    else if (priority == TaskPriority.UrgentAndImportant)
      return "Urgent et Important";
    return "A faire";
  }

  static int getPriorityValue(TaskPriority priority) {
    if (priority == TaskPriority.ToBeDone)
      return 0;
    else if (priority == TaskPriority.Important)
      return 1;
    else if (priority == TaskPriority.Urgent)
      return 2;
    else
      return 3;
  }

  static Map<String, List<Task>> filterTasksByPeriod(
      Periods period, List<Task> tasks) {
    MyTime myTime = new MyTime();
    Map<String, List<Task>> filteredTasks = {};
    switch (period) {
      case Periods.Day:
        tasks.forEach((element) {
          final day = myTime.compareDayToCurrent(dateString: element.addedOn);
          if (filteredTasks[day.toString()] == null) {
            filteredTasks[day.toString()] = [];
          }
          filteredTasks[day.toString()].add(element);
        });
        return filteredTasks;
        break;

      case Periods.Week:
        tasks.forEach((element) {
          final week = myTime.compareWeekToCurrent(dateString: element.addedOn);
          if (filteredTasks[week.toString()] == null) {
            filteredTasks[week.toString()] = [];
          }
          filteredTasks[week.toString()].add(element);
        });
        return filteredTasks;
        break;

      case Periods.Month:
        tasks.forEach((element) {
          final month =
              myTime.compareMonthToCurrent(dateString: element.addedOn);
          if (filteredTasks[month.toString()] == null) {
            filteredTasks[month.toString()] = [];
          }
          filteredTasks[month.toString()].add(element);
        });
        return filteredTasks;
        break;

      case Periods.Year:
        tasks.forEach((element) {
          final year = myTime.compareYearToCurrent(dateString: element.addedOn);
          if (filteredTasks[year.toString()] == null) {
            filteredTasks[year.toString()] = [];
          }
          filteredTasks[year.toString()].add(element);
        });
        return filteredTasks;
        break;
      default:
        return filteredTasks;
    }
  }

  static List<Task> thisWeeksMonthOrYearTasks(
      Map<String, List<Task>> filteredTasks) {
    return filteredTasks["0"];
  }

  static Map<String, int> getTasksStatistics(List<Task> tasks) {
    int completed = 0;

    if (tasks != null) {
      tasks.forEach((task) {
        if (task.completed) completed++;
      });

      return {
        "total": tasks.length,
        "pending": tasks.length - completed,
        "completed": completed
      };
    }
    return {"total": 0, "pending": 0, "completed": 0};
  }

  static getExecutionRate(Map<String, int> stats) {
    return (stats["completed"] / stats["total"]) * 100;
  }

  static Future<Map<String, int>> getTotalStats(context, adminId) async {
    final users =
        Provider.of<AdminTaskProvider>(context, listen: false).currentUsers;

    if (users.isNotEmpty) {
      print("we dey here");
      int completed = 0;
      int total = 0;

      for (int i = 0; i < users.length; i++) {
        await Provider.of<TaskProvider>(context, listen: false)
            .getAllMyTasks(users[i].userId, adminOrSupervisorMode: true);
        final stats = getTasksStatistics(
            Provider.of<TaskProvider>(context, listen: false).tasks);
        print(stats);
        completed += stats["completed"];
        total += stats["total"];
      }
      return {
        "total": total,
        "completed": completed,
        "pending": total - completed
      };
    } else {
      return {"total": 0, "completed": 0, "pending": 0};
    }
  }

  static String formatDateTimeToStandardString(DateTime dateTime) {
    return dateTime.toIso8601String().split("T")[0] +
        " " +
        dateTime.toIso8601String().split("T")[1].split(".")[0];
  }

  static List<User> filterUsers(String roleToShow, List<User> userList) {
    if (roleToShow == "user") {
      print("user role is user");
      return userList
          .where((user) => (user.role == "user" || user.role == null))
          .toList();
    } else if (roleToShow == "all")
      return userList;
    else if (roleToShow == Config.administrator) {
      return userList
          .where((user) => user.role == Config.administrator)
          .toList();
    } else if (roleToShow == Config.superAdministrator) {
      return userList
          .where((user) => user.role == Config.superAdministrator)
          .toList();
    } else {
      return userList.where((user) => user.role == Config.supervisor).toList();
    }
  }

  ///Moves the focus from the [from] focus node to the  [to] focusNode
  static void fieldFocusChange(
      BuildContext context, FocusNode from, FocusNode to) {
    from.unfocus();
    FocusScope.of(context).requestFocus(to);
  }

  static Future<void> refreshTasks(BuildContext context,
      RefreshController controller, dynamic widget) async {
    final res = await Provider.of<TaskProvider>(context, listen: false)
        .getAllMyTasks(Provider.of<TaskProvider>(context, listen: false).userId,
            adminOrSupervisorMode: widget.isAdminOrSupervisorMode,
            offlineMode: Provider.of<TaskProvider>(context)
                .settings
                .offlineMode
                .getValue());

    if (res == true)
      controller.refreshCompleted();
    else
      controller.refreshFailed();
  }
}
