class MyTime {
  //date format 2020-05-2

  MyTime({this.initialDateString}) {
    if (this.initialDateString != null) {
      convertedDate = getDateFromString(this.initialDateString);
    }
    now = DateTime.now();
  }

  String initialDateString;
  DateTime convertedDate;
  DateTime now;

  int getWeek(DateTime date) {
    final double weekRange = date.day / 7;
    if (weekRange <= 1) {
      return 1;
    } else if (weekRange <= 2) {
      return 2;
    } else if (weekRange <= 3) {
      return 3;
    } else if (weekRange <= 4) {
      return 4;
    } else
      return 5;
  }

  int compareDayToCurrent({String dateString}) {
    final date =
        dateString != null ? getDateFromString(dateString) : convertedDate;
    assert(date.month == now.month);
    return now.weekday - date.weekday;
  }

  ///returns the difference between the current week and the week from the date string
  ///if the method returns [0] then it is the same week
  ///if the returned [value] is [positive] then the current week is [value] weeks ahead of the date string week
  int compareWeekToCurrent({String dateString}) {
    final date =
        dateString != null ? getDateFromString(dateString) : convertedDate;
    // assert(date.month == now.month);
    return getWeek(now) - getWeek(date);
  }

  ///returns the difference between the current month and the month given by the dateString
  int compareMonthToCurrent({String dateString}) {
    final date =
        dateString != null ? getDateFromString(dateString) : convertedDate;
    assert(date.year == now.year);
    return now.month - date.month;
  }

  ///returns the difference between the current year and the year given by the dateString
  int compareYearToCurrent({String dateString}) {
    final date =
        dateString != null ? getDateFromString(dateString) : convertedDate;

    return now.year - date.year;
  }

  DateTime getDateFromString(String dateString) {
    //we separate the date string at the - character

    final dateArray = dateString
        .split(' ')[0]
        .split('-')
        .map((item) => int.parse(item))
        .toList();

    final date = new DateTime(dateArray[0], dateArray[1], dateArray[2]);
    return date;
  }

  static DateTime getDateTimeFromString(String dateString) {
    final dateArray = dateString
        .split(' ')[0]
        .split('-')
        .map((item) => int.parse(item))
        .toList();

    final timeArray = dateString
        .split(' ')[1]
        .split(":")
        .map((item) => int.parse(item))
        .toList();

    return new DateTime(dateArray[0], dateArray[1], dateArray[2], timeArray[0],
        timeArray[1], timeArray[2]);
  }
}
