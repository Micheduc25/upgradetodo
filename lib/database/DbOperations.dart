import 'package:sqflite/sqflite.dart';
import 'package:upgrade_to_do/database/DbConnect.dart';
import 'package:upgrade_to_do/models/taskModel.dart';
import 'package:upgrade_to_do/models/userModel.dart';
import 'package:upgrade_to_do/utils/Config.dart';
import 'package:upgrade_to_do/utils/helperFunctions.dart';

class DbOperations {
  ///returns [true] if task successfully added, [false] otherwise
  static Future<bool> addTask(int userId, Task task) async {
    final db = await DBConnect().initDB();

    try {
      if (task.id == null) {
        await db.insert(
            "tasks",
            {
              Config.title: task.title,
              Config.userId: userId,
              Config.createdAt: task.addedOn,
              Config.description: task.description,
              Config.endDate: task.completedOn,
              Config.createdOffline: task.locallyCreated ? 1 : 0,
              Config.taskPriority:
                  HelperFunction.getTaskPriorityToServerFormat(task.priority),
              Config.tstate: task.completed ? "true" : "false",
              Config.updatedAt: task.lastModifiedOn,
              Config.synchronized: task.synchronized ? "true" : "false"
            },
            conflictAlgorithm: ConflictAlgorithm.replace);
      } else {
        await db.insert(
            "tasks",
            {
              Config.id: task.id,
              Config.userId: userId,
              Config.title: task.title,
              Config.createdAt: task.addedOn,
              Config.description: task.description,
              Config.endDate: task.completedOn,
              Config.createdOffline: task.locallyCreated == true ? 1 : 0,
              Config.taskPriority:
                  HelperFunction.getTaskPriorityToServerFormat(task.priority),
              Config.tstate: task.completed ? "true" : "false",
              Config.updatedAt: task.lastModifiedOn,
              Config.synchronized: task.synchronized ? "true" : "false"
            },
            conflictAlgorithm: ConflictAlgorithm.replace);
      }
      return true;
    } catch (err) {
      print("an error occured during task insertion ---> $err");
      return false;
    } finally {
      await db.close();
    }
  }

  static Future<List<Task>> getAllTasks(int userId) async {
    final db = await DBConnect().initDB();
    List<Task> tasks = [];

    try {
      final tasksMap = await db.query("tasks",
          where: "tasks.userId=$userId", distinct: true);

      tasksMap.forEach((map) {
        tasks.add(new Task.fromMap(map));
      });

      return tasks;
    } catch (err) {
      print("An error occured during task retrieval from db --- > $err");
      return null;
    } finally {
      await db.close();
    }
  }

  static Future<bool> updateTask(
      int taskId, Map<String, dynamic> dataMap) async {
    final db = await DBConnect().initDB();

    try {
      await db.update("tasks", dataMap,
          where: "tasks.id=$taskId",
          conflictAlgorithm: ConflictAlgorithm.replace);
      return true;
    } catch (err) {
      print("An error occured during task update ---- > $err");
      return false;
    } finally {
      await db.close();
    }
  }

  static Future<bool> deleteTask(int taskId) async {
    final db = await DBConnect().initDB();

    try {
      await db.delete("tasks", where: "$taskId=tasks.id");
      return true;
    } catch (err) {
      print("An error occured during task deletion ---- > $err");
      return false;
    } finally {
      await db.close();
    }
  }

  static Future<bool> toggleTask(int taskId, bool value) async {
    final db = await DBConnect().initDB();

    try {
      await db.update("tasks", {Config.tstate: value ? "true" : "false"},
          where: "$taskId=tasks.id");
      return true;
    } catch (err) {
      print("An error occured during task toggling ---- > $err");
      return false;
    } finally {
      await db.close();
    }
  }

  static Future<User> loginUser(String username, String password) async {
    final db = await DBConnect().initDB();

    try {
      final users = await db.query("users",
          where: "users.username='$username' AND users.password='$password'");

      final user = users[0];
      if (user.isNotEmpty || user != null) {
        return new User(
            userId: user[Config.id],
            supervisor: user[Config.supervisor],
            userName: user[Config.username],
            role: user[Config.role]);
      } else
        return null;
    } catch (err) {
      print("An error occured during login ---- > $err");
      return null;
    } finally {
      await db.close();
    }
  }

  static Future<bool> addUser(
      int userId, String userName, String password, String role) async {
    final db = await DBConnect().initDB();

    try {
      await db.insert("users", {
        Config.id: userId,
        Config.username: userName,
        Config.password: password,
        Config.role: role,
        Config.createdAt:
            HelperFunction.formatDateTimeToStandardString(DateTime.now()),
        Config.updatedAt:
            HelperFunction.formatDateTimeToStandardString(DateTime.now())
      });
      return true;
    } catch (err) {
      print("An error occured during user addition ---> $err");
      return false;
    } finally {
      db.close();
    }
  }

  static Future<bool> deleteUser(userId) async {
    final db = await DBConnect().initDB();

    try {
      await db.delete("users", where: "users.id=$userId");
      return true;
    } catch (err) {
      print("An error occured during user addition ---> $err");
      return false;
    } finally {
      db.close();
    }
  }

  static Future<bool> isUserRegistered(int userId) async {
    final db = await DBConnect().initDB();

    try {
      final users = await db.query("users");
      print("users are $users");
      final res = await db.rawQuery("SELECT FROM users WHERE users.id=$userId");

      print(res);
      if (res.isNotEmpty)
        return true;
      else
        return false;
    } catch (err) {
      print("an error occured here");
      return false;
    }
  }

  static Future<bool> clearDbTables() async {
    final db = await DBConnect().initDB();

    try {
      await db.delete("users");
      await db.delete("tasks");
      await db.delete(Config.synchronization);
      return true;
    } catch (err) {
      print("could not clear tables $err");
      return false;
    } finally {
      await db.close();
    }
  }

  static Future<bool> isTaskOnline(taskId) async {
    print("taskId is  $taskId");
    final db = await DBConnect().initDB();

    try {
      final res = await db.query("tasks",
          columns: [Config.synchronized], where: "tasks.id=$taskId");

      if (res != null) {
        final synch = res[0][Config.synchronized];

        if (synch == "true")
          return true;
        else
          return false;
      } else {
        return false;
      }
    } catch (err) {
      print("could not show that task is synchronized $err");
      return false;
    } finally {
      await db.close();
    }
  }

  static Future<bool> isTaskSynchronized(taskId) async {
    print("taskId is  $taskId");
    final db = await DBConnect().initDB();

    try {
      final res = await db.query("tasks",
          columns: [Config.synchronized], where: "tasks.id=$taskId");

      if (res != null) {
        final synch = res[0][Config.synchronized];

        if (synch == "true")
          return true;
        else
          return false;
      } else {
        return false;
      }
    } catch (err) {
      print("could not show that task is synchronized $err");
      return false;
    } finally {
      await db.close();
    }
  }

  static Future<bool> addUnsyncTask(int taskId, String syncType) async {
    final db = await DBConnect().initDB();

    try {
      await db.insert(Config.synchronization,
          {"taskId": taskId, Config.syncType: syncType});

      print("task added to synchronization table");

      return true;
    } catch (err) {
      print("could not add unsync task $taskId");
      return false;
    } finally {
      await db.close();
    }
  }

  static Future<dynamic> getLastRowId(String table, String idColumn) async {
    final db = await DBConnect().initDB();

    try {
      final temp = await db.query('''
      SELECT TOP 1 * FROM $table  ORDER BY $idColumn DESC
      ''', columns: [idColumn]);

      var id = temp[0][idColumn];

      return id;
    } catch (err) {
      print("could not add unsync task ----->$err");
      return null;
    } finally {
      await db.close();
    }
  }

  static Future<List<Task>> getUnsyncTasksAddedOffline() async {
    final db = await DBConnect().initDB();
    List<Task> tasks = [];

    try {
      final tasksMaps = await db.query("tasks",
          where:
              "${Config.synchronized}='false' AND ${Config.createdOffline}=1");

      tasksMaps.forEach((map) {
        tasks.add(new Task.fromMap(map));
      });
      return tasks;
    } catch (err) {
      print("could not retrieve unsyncofflineadded tasks ");
      return tasks;
    } finally {
      await db.close();
    }
  }

  static Future<bool> removeUnsyncTasks(int taskId) async {
    final db = await DBConnect().initDB();

    try {
      await db.delete(Config.synchronization, where: "taskId=$taskId");

      return true;
    } catch (err) {
      print("could not remove taskId $taskId");
      return false;
    } finally {
      await db.close();
    }
  }

  static Future<List<dynamic>> getUnsynchronizedTasks(String syncType) async {
    List<dynamic> unsyncTasks = [];

    final db = await DBConnect().initDB();

    try {
      //we obtain all tasks in the synchronization table with the given syncType
      //if it is updates we return tasks
      //if it is deletes we just return task ids because the tasks themselves will no longer be in the database

      final taskMaps = await db.query(Config.synchronization,
          where: "${Config.syncType}='$syncType'", columns: ["taskId"]);

      for (int i = 0; i < taskMaps.length; i++) {
        if (syncType == Config.update) {
          final temp =
              await db.query("tasks", where: "id = ${taskMaps[i]['taskId']} ");

          unsyncTasks.add(new Task.fromMap(temp[0]));
        } else if (syncType == Config.delete) {
          unsyncTasks.add(taskMaps[i]['taskId']);
        }
      }

      return unsyncTasks;
    } catch (err) {
      print("could not retrieve unsynchronized tasks ");
      return unsyncTasks;
    } finally {
      await db.close();
    }
  }

  static Future<void> displayDbTables() async {
    final db = await DBConnect().initDB();

    // final users = await db.query("users");
    final tasks = await db.query("tasks");
    // final synchro = await db.query(Config.synchronization);

    // await db.close();

    print('''
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///                 DB Tables
    /// $tasks                                                                               //
    /// /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ''');
  }
}
