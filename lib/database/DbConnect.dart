import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:upgrade_to_do/utils/Config.dart';

class DBConnect {
  Future<Database> initDB() async {
    final Database database = await openDatabase(
        // Set the path to the database. Note: Using the `join` function from the
        // `path` package is best practice to ensure the path is correctly

        join(await getDatabasesPath(), 'upgrade_todo.db'),
        onCreate: (db, version) async {
      await db.execute(
          ''' CREATE TABLE tasks (id INTEGER NOT NULL  PRIMARY KEY AUTOINCREMENT ,
     created_at TEXT, description TEXT,end_date TEXT,task_priority TEXT,title TEXT,
     tstate VARCHAR(5),updated_at TEXT,synchronized VARCHAR(5), created_offline INTEGER,
      userId INTEGER NOT NULL
       );''');

      await db.execute('''
        CREATE TABLE users(id INTEGER NOT NULL PRIMARY KEY, username TEXT,
         password TEXT, role TEXT, updated_at TEXT,created_at TEXT, supervisor INTEGER)
        ''');

      await db.execute('''
        CREATE TABLE ${Config.synchronization}(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
         taskId INTEGER, sync_type TEXT)
        ''');
    }, onOpen: (db) async {
      print("database opened at ${DateTime.now()}");
    }, version: 1);

    return database;
  }
}
