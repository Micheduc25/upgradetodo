import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

import 'package:upgrade_to_do/providers/adminTaskProvider.dart';
import 'package:upgrade_to_do/providers/formProvider.dart';
import 'package:upgrade_to_do/providers/loginSignUpProvider.dart';
import 'package:upgrade_to_do/providers/supervisorTaskProvider.dart';
import 'package:upgrade_to_do/providers/taskProvider.dart';
import 'package:upgrade_to_do/screens/adminScreens/adminHome.dart';
import 'package:upgrade_to_do/screens/adminScreens/superAdminHome.dart';
import 'package:upgrade_to_do/screens/homeScreen.dart';
import 'package:upgrade_to_do/screens/loginOrSignup/loginOrSignupScreen.dart';
import 'package:upgrade_to_do/utils/Config.dart';

import 'package:upgrade_to_do/utils/settings.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: upgradeBrown,
      statusBarBrightness: Brightness.dark,
      statusBarIconBrightness: Brightness.light,
      systemNavigationBarColor: upgradeBrown));
  // WidgetsFlutterBinding.ensureInitialized();
  runApp(
      // TestApp()
      MyApp());
}

MaterialColor upgradeBrown =
    new MaterialColor(0xFF442503, {500: Color(0xFF442503)});

MaterialColor upgradeOrange = new MaterialColor(
    0xFFFFA200, {500: Color(0xFFFFA200), 600: Color(0xFFFFA202)});

class MyApp extends StatefulWidget {
  // This widget is the root of my application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    // ConnectivityUtils.instance.setServerToPing(ServerUrl);
    // print("ping server set to $serverUrl");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => TaskProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => AdminTaskProvider(),
        ),
        ChangeNotifierProvider(create: (context) => SupervisorTaskProvider())
      ],
      child: MaterialApp(
          title: 'upgrade_to_do',
          theme: ThemeData(
              scaffoldBackgroundColor: Colors.white,
              primaryColor: upgradeOrange,
              primaryColorDark: upgradeBrown,
              primarySwatch: Colors.orange,
              textTheme: Typography.blackMountainView,
              canvasColor: Colors.white,
              buttonTheme: ButtonThemeData(
                  buttonColor: upgradeBrown, textTheme: ButtonTextTheme.normal),
              dialogTheme: DialogTheme(
                  titleTextStyle: TextStyle(
                      color: upgradeBrown,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                  contentTextStyle:
                      TextStyle(color: upgradeBrown, fontSize: 17)),
              tooltipTheme: TooltipThemeData(
                  textStyle: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                  ),
                  decoration: BoxDecoration(
                      color: upgradeOrange,
                      border: Border.all(
                        color: upgradeBrown,
                      ))),
              appBarTheme: AppBarTheme(
                  elevation: 0,
                  iconTheme: IconThemeData(color: Colors.white, size: 30),
                  textTheme: TextTheme(
                      headline6: TextStyle(color: Colors.white, fontSize: 18))),
              floatingActionButtonTheme: FloatingActionButtonThemeData(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                foregroundColor: Colors.white,
                backgroundColor: upgradeOrange,
              )),
          home: MainHome()),
    );
  }
}

class MainHome extends StatefulWidget {
  @override
  _MainHomeState createState() => _MainHomeState();
}

class _MainHomeState extends State<MainHome> {
  StreamingSharedPreferences _prefs;
  bool autoLogin;

  @override
  void initState() {
    // ConnectivityUtils.instance.setServerToPing("google.com");

    //we try to login automatically if the user stored his information locally
    autoLogin = true;
    StreamingSharedPreferences.instance.then((prefs) async {
      setState(() {
        _prefs = prefs;
      });
      // await DbOperations.clearDbTables();
      // print("tables cleared");

      await Provider.of<TaskProvider>(context, listen: false)
          .getCurrentSettings();

      final userName =
          _prefs.getString("userName", defaultValue: "default").getValue();
      final password =
          _prefs.getString("password", defaultValue: "default").getValue();

      if (userName == "default" || password == "default") {
        setState(() {
          autoLogin = false;
        });
      } else {
        print("your data is stored locally we are login you in....");
        final userData = await LoginSignUpProvider.loginUser(context,
            name: userName,
            password: password,
            offlineMode: Provider.of<TaskProvider>(context, listen: false)
                .settings
                .offlineMode
                .getValue());

        if (userData == null) {
          setState(() {
            autoLogin = false;
          });
        } else {
          if (userData.role == Config.administrator ||
              userData.role == Config.superAdministrator) {
            Provider.of<AdminTaskProvider>(context, listen: false).adminId =
                userData.userId;
            await Provider.of<AdminTaskProvider>(context, listen: false)
                .getAllUsers(userData.userId);
          }
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => userData.role == Config.administrator
                  ? AdminHome(
                      settings: Settings(_prefs),
                      adminId: userData.userId,
                      adminName: userData.userName)
                  : userData.role == Config.superAdministrator
                      ? SuperAdminHome(
                          settings: Settings(_prefs),
                          adminId: userData.userId,
                          adminName: userData.userName)
                      : MyHomePage(
                          offlineMode: Provider.of<TaskProvider>(context)
                              .settings
                              .offlineMode
                              .getValue(),
                          hasSupervisor: userData.supervisor != null,
                          settings: Settings(_prefs),
                          adminId: null,
                          userRole: userData.role,
                          isSupervisor: userData.role == Config.supervisor,
                          userId: userData.userId,
                          title: "Mes Taches",
                          userName: userData.userName)));
        }
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _prefs != null && !autoLogin
        ? ChangeNotifierProvider(
            create: (context) => FormProvider(),
            child: LoginOrSignUp(
              settings: Settings(_prefs),
            ))
        : Scaffold(
            body: Container(
              alignment: Alignment.center,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    "Bienvenue ${_prefs != null ? _prefs.getString("userName", defaultValue: "User").getValue() : ""}",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: upgradeBrown,
                        fontSize: 30,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  CircularProgressIndicator(
                    backgroundColor: upgradeBrown,
                  ),
                ],
              ),
            ),
          );
  }
}
