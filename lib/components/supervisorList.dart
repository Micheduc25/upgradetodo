import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:upgrade_to_do/components/loadingDialog.dart';
import 'package:upgrade_to_do/main.dart';
import 'package:upgrade_to_do/providers/adminTaskProvider.dart';
import 'package:upgrade_to_do/utils/Config.dart';

class SuperVisorList extends StatefulWidget {
  SuperVisorList(
      {@required this.userId,
      @required this.userName,
      @required this.onWillPop,
      @required this.onSupervisorValidate});

  final int userId;
  final String userName;
  final Function onSupervisorValidate;
  final Function onWillPop;
  @override
  _SuperVisorListState createState() => _SuperVisorListState();
}

class _SuperVisorListState extends State<SuperVisorList> {
  @override
  Widget build(BuildContext context) {
    final supervisors = Provider.of<AdminTaskProvider>(context)
        .currentUsersAnnex
        .where((user) => user.role == Config.supervisor)
        .toList();
    return WillPopScope(
      onWillPop: widget.onWillPop,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        color: Colors.white,
        child: supervisors.isEmpty
            ? Center(
                child: Text(
                    "Il n'y a pas de superviseur disponible pour le moment"),
              )
            : Column(
                children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    "Choissisez un superviseur pour ${widget.userName}",
                    style: TextStyle(
                        color: upgradeOrange,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 22,
                  ),
                  Divider(
                    color: upgradeBrown,
                  ),
                  Expanded(
                    child: ListView.builder(
                        itemCount: supervisors.length,
                        itemBuilder: (context, index) {
                          return Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              ListTile(
                                title: Text(
                                  supervisors[index].userName,
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                ),
                                onTap: () async {
                                  showLoadingDialog(
                                      "Assignation du superviseur...", context);
                                  final res =
                                      await Provider.of<AdminTaskProvider>(
                                              context,
                                              listen: false)
                                          .addTeamMembers(
                                              supervisors[index].userId,
                                              [widget.userId],
                                              context,
                                              true);

                                  Navigator.of(context).pop();

                                  if (res) {
                                    widget.onSupervisorValidate();

                                    showMessage(context, "succès",
                                        "Désormais ${supervisors[index].userName} supervise ${widget.userName} ");
                                  } else {
                                    showMessage(context, "Erreur",
                                        "Désolé une érreur s'est produite lors de la connexion au serveur😓 ");
                                  }
                                },
                              ),
                              Divider(
                                color: upgradeBrown,
                              )
                            ],
                          );
                        }),
                  ),
                ],
              ),
      ),
    );
  }
}
