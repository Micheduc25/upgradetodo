import 'package:flutter/material.dart';
import 'package:upgrade_to_do/main.dart';

class ScrollWidget extends StatefulWidget {
  ScrollWidget(
      {this.scrollController,
      @required this.scrollingDown,
      @required this.referencePos});

  final ScrollController scrollController;
  final bool scrollingDown;
  final double referencePos;

  @override
  _ScrollWidgetState createState() => _ScrollWidgetState();
}

class _ScrollWidgetState extends State<ScrollWidget>
    with TickerProviderStateMixin {
  AnimationController arrow1Controller;
  AnimationController arrow2Controller;
  AnimationController containerController;

  Animation curvedAnimation1;
  Animation curvedAnimation2;
  Animation curvedAnimation3;

  @override
  void initState() {
    arrow1Controller = new AnimationController(
        vsync: this, duration: Duration(milliseconds: 500));

    arrow2Controller = new AnimationController(
        vsync: this, duration: Duration(milliseconds: 500));

    containerController = new AnimationController(
        vsync: this, duration: Duration(milliseconds: 300));

    curvedAnimation1 =
        new CurvedAnimation(parent: arrow1Controller, curve: Curves.easeOut);

    curvedAnimation2 =
        new CurvedAnimation(parent: arrow2Controller, curve: Curves.easeOut);

    curvedAnimation3 = new CurvedAnimation(
        parent: containerController, curve: Curves.bounceInOut);

    containerController.forward();

    arrow1Controller.forward();
    Future.delayed(Duration(milliseconds: 500), () {
      arrow2Controller.forward();
    });

//     containerController.forward();

    arrow1Controller.addListener(() {
      setState(() {});
    });

    arrow2Controller.addListener(() {
      setState(() {});
    });

    containerController.addListener(() {
      setState(() {});
    });

    arrow1Controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        arrow1Controller.reverse();
      } else if (status == AnimationStatus.dismissed) {
        arrow1Controller.forward();
      }
    });

    arrow2Controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        arrow2Controller.reverse();
      } else if (status == AnimationStatus.dismissed) {
        arrow2Controller.forward();
      }
    });

    containerController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        containerController.reverse();
      } else if (status == AnimationStatus.dismissed) {
        containerController.forward();
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    arrow1Controller.dispose();
    arrow2Controller.dispose();
    containerController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Transform.translate(
        offset: Offset(0, curvedAnimation3.value * 2.5),
        child: InkWell(
          onTap: () {
            if (widget.scrollingDown) {
              widget.scrollController.animateTo(
                  widget.scrollController.position.maxScrollExtent,
                  duration: Duration(milliseconds: 500),
                  curve: Curves.bounceInOut);
            } else {
              widget.scrollController.animateTo(
                  widget.scrollController.position.minScrollExtent,
                  duration: Duration(milliseconds: 500),
                  curve: Curves.bounceInOut);
            }
          },
          child: Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                color: upgradeOrange.withOpacity(.5),
                shape: BoxShape.circle,
                border: Border.all(color: upgradeBrown.withOpacity(.5)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.black.withOpacity(.3),
                      offset: Offset(1, 1),
                      blurRadius: 2,
                      spreadRadius: 2)
                ]),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(
                  widget.scrollingDown ? Icons.expand_more : Icons.expand_less,
                  size: 30,
                  color: upgradeBrown.withOpacity(curvedAnimation1.value),
                ),
//
                Icon(
                  widget.scrollingDown ? Icons.expand_more : Icons.expand_less,
                  size: 30,
                  color: upgradeBrown.withOpacity(curvedAnimation2.value),
                )
              ],
            ),
          ),
        ));
  }
}
