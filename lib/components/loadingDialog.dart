import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:upgrade_to_do/main.dart';

showLoadingDialog(loadMessage, BuildContext context) {
  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              CircularProgressIndicator(
                backgroundColor: upgradeBrown,
              ),
              SizedBox(height: 10),
              Text(loadMessage)
            ],
          ),
        );
      });
}

Future<bool> showMessage(BuildContext context, String title, String message,
    {bool dismissible = true, bool showActions = false}) async {
  final res = showDialog<bool>(
      context: context,
      barrierDismissible: dismissible,
      builder: (context) => AlertDialog(
            title: Text(
              title,
              textAlign: TextAlign.center,
              style: TextStyle(color: upgradeOrange, fontSize: 19),
            ),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  message,
                  style: TextStyle(color: upgradeBrown),
                ),
                !showActions
                    ? SizedBox(
                        height: 20,
                      )
                    : Container(),
                !showActions
                    ? RaisedButton(
                        child: Text(
                          "OK",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        })
                    : Container()
              ],
            ),
            actions: showActions
                ? <Widget>[
                    FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop(false);
                        },
                        child: Text(
                          "Non",
                          style: TextStyle(color: upgradeOrange),
                        )),
                    FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop(true);
                        },
                        child: Text(
                          "Oui",
                          style: TextStyle(color: upgradeOrange),
                        ))
                  ]
                : null,
          ));

  return res;
}
