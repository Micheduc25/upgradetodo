import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
//import 'package:upgrade_to_do/components/scrollWidget.dart';
import 'package:upgrade_to_do/components/taskItem.dart';
import 'package:upgrade_to_do/models/taskModel.dart';
import 'package:upgrade_to_do/providers/taskProvider.dart';
import 'package:upgrade_to_do/utils/helperFunctions.dart';
import 'package:upgrade_to_do/utils/periods.dart';
import 'package:upgrade_to_do/main.dart';

class AllTasks extends StatefulWidget {
  AllTasks(this.adminMode,
      {@required this.adminOrSupId,
      @required this.isAdminOrSupervisorMode,
      @required this.scrollController,
      this.period});
  final bool adminMode;

  final bool isAdminOrSupervisorMode;
  final int adminOrSupId;
  final Periods period;
  final ScrollController scrollController;

  @override
  _AllTasksState createState() => _AllTasksState();
}

class _AllTasksState extends State<AllTasks> {
  RefreshController controller;

  @override
  initState() {
    super.initState();
    controller = new RefreshController(initialRefresh: false);
  }

  @override
  dispose() {
    controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Task> currentTasks = Provider.of<TaskProvider>(context).getAllTasks;

    if (widget.period == Periods.Week) {
      currentTasks =
          HelperFunction.filterTasksByPeriod(Periods.Week, currentTasks)["0"];
    } else if (widget.period == Periods.Month) {
      currentTasks =
          HelperFunction.filterTasksByPeriod(Periods.Month, currentTasks)["0"];
    } else if (widget.period == Periods.Year) {
      currentTasks =
          HelperFunction.filterTasksByPeriod(Periods.Year, currentTasks)["0"];
    }
    // print(currentTasks);
    return Center(
        child: Stack(
      children: <Widget>[
        Container(
          child: currentTasks.isEmpty
              ? Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Image.asset("assets/logo.png"),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Vous n'avez pas encore de taches",
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 17,
                      ),
                      textAlign: TextAlign.center,
                    )
                  ],
                )
              : SmartRefresher(
                  controller: controller,
                  enablePullDown: true,
//                  enablePullUp: true,
                  header: WaterDropMaterialHeader(
                    backgroundColor: upgradeOrange,
                  ),
                  onRefresh: () async {
                    final res = await Provider.of<TaskProvider>(context,
                            listen: false)
                        .getAllMyTasks(
                            Provider.of<TaskProvider>(context, listen: false)
                                .userId,
                            adminOrSupervisorMode:
                                widget.isAdminOrSupervisorMode,
                            offlineMode: Provider.of<TaskProvider>(context,
                                    listen: false)
                                .settings
                                .offlineMode
                                .getValue());

                    if (res == true)
                      controller.refreshCompleted();
                    else
                      controller.refreshFailed();
                  },

                  child: ListView.builder(
                      itemCount: currentTasks.length,
                      controller: widget.scrollController,
                      itemBuilder: (context, index) {
                        return TaskItem(
                            isAdminOrSupervisorMode:
                                widget.isAdminOrSupervisorMode,
                            isAdminMode: widget.adminMode,
                            showDelete: !widget.adminMode,
                            adminOrSupId: widget.adminOrSupId,
                            task: currentTasks[index],
                            showCheckBox: !widget.adminMode);
                      }),
                ),
        ),
//        Positioned(
//          bottom: 10,
//          right: 5,
//          child: ScrollWidget(
//            scrollController: widget.scrollController,
//            scrollingDown: widget.scrollingDown,
//            referencePos:widget.referencePos
//          ),
//        )
      ],
    ));
  }
}
