import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:upgrade_to_do/components/loadingDialog.dart';

import 'package:upgrade_to_do/main.dart';
import 'package:upgrade_to_do/models/userModel.dart';
import 'package:upgrade_to_do/providers/adminTaskProvider.dart';
import 'package:upgrade_to_do/utils/Config.dart';
import './selectUserItem.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class UserList extends StatefulWidget {
  UserList(
      {@required this.userId,
      @required this.userName,
      @required this.onWillPop,
      @required this.externalMode,
      @required this.onUsersValidate,
      @required this.alreadySelectedUsers,
      this.onTeamMembersAdded});

  final int userId;
  final String userName;
  final Function onUsersValidate;
  final bool externalMode;
  final Function(List<int> selectedUsersIds, List<User> selectedUsers)
      onTeamMembersAdded;
  final Future<bool> Function() onWillPop;
  final List<User> alreadySelectedUsers;
  @override
  _UserListState createState() => _UserListState();
}

class _UserListState extends State<UserList> {
  List<int> userIdList;
  List<User> usersList;
  List<User> totalUserList;
  List<User> selectedUserList;
  RefreshController controller = new RefreshController(initialRefresh: false);

  bool validated;
  @override
  void initState() {
    userIdList = [];
    totalUserList = [];
    selectedUserList = [];
    usersList = [];
    validated = false;
    print("in initsate ohhh");
    if (widget.externalMode && widget.alreadySelectedUsers != []) {
      widget.alreadySelectedUsers.forEach((user) {
        userIdList.add(user.userId);
        selectedUserList.add(user);
      });
    }

    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("we are in");
    final users =
        Provider.of<AdminTaskProvider>(context).currentUsersAnnex.where((user) {
      // print("${user.userName} supervisor is ${user.supervisor}");
      return ((user.role == Config.user || user.role == null) &&
          (user.supervisor == null));
    }).toList();

    for (User user in users) print(user.toMap());

    //we create a new list to avoid modifications on the original list object
    if (totalUserList.isEmpty) {
      totalUserList = new List.from(users, growable: true);
    }
    if (usersList.isEmpty) {
      usersList = new List.from(users, growable: true);
    }
    print(usersList);
    return WillPopScope(
      onWillPop: widget.onWillPop,
      child: SmartRefresher(
        controller: controller,
        enablePullDown: true,
        enablePullUp: true,
        header: WaterDropMaterialHeader(
          backgroundColor: upgradeOrange,
        ),
        onRefresh: () async {
          await Provider.of<AdminTaskProvider>(context, listen: false)
              .getAllUsers(
                  Provider.of<AdminTaskProvider>(context, listen: false)
                      .adminId);

          controller.refreshCompleted();
        },
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          color: Colors.white,
          child: usersList.isEmpty
              ? Center(
                  child: Text(
                      "Il n'y a pas d'utilisateur disponible pour le moment"),
                )
              : Column(
                  children: <Widget>[
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Selectionez les utilisateurs à assigner à ${widget.userName}",
                      style: TextStyle(
                          color: upgradeOrange,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    Divider(
                      color: upgradeBrown,
                    ),
                    Expanded(
                      child: ListView.builder(
                          itemCount: totalUserList.length,
                          itemBuilder: (context, index) {
                            return SelectUserItem(
                              userName: totalUserList[index].userName,
                              alreadyChoosen:
                                  totalUserList[index].supervisor != null,
                              initialValue: widget.alreadySelectedUsers != null
                                  ? widget.alreadySelectedUsers
                                      .contains(totalUserList[index])
                                  : false,
                              onSelect: (value) {
                                setState(() {
                                  if (value) {
                                    userIdList.add(totalUserList[index].userId);

                                    if (widget.externalMode)
                                      selectedUserList
                                          .add(totalUserList[index]);
                                  } else {
                                    userIdList
                                        .remove(totalUserList[index].userId);

                                    if (widget.externalMode)
                                      selectedUserList
                                          .remove(totalUserList[index]);
                                  }
                                });
                                print(userIdList);
                              },
                            );
                          }),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    RaisedButton(
                        color: upgradeBrown,
                        padding:
                            EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                        child: Text(
                          "OK",
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                        onPressed: !validated
                            ? () async {
                                if (!widget.externalMode) {
                                  setState(() {
                                    validated = true;
                                  });

                                  if (userIdList.isNotEmpty) {
                                    showLoadingDialog(
                                        "veuillez patienter...", context);
                                    final res =
                                        await Provider.of<AdminTaskProvider>(
                                                context,
                                                listen: false)
                                            .addTeamMembers(widget.userId,
                                                userIdList, context, false);

                                    if (res) {
                                      Navigator.of(context).pop();

                                      widget.onUsersValidate();
                                    } else {
                                      showMessage(
                                          context,
                                          "Erreur",
                                          "Une érreur est survenu lors de l'affectation des utilisateurs, vérifiez votre connexion" +
                                              " internet et réessayez 😓");
                                    }
                                  } else {
                                    showMessage(
                                        context,
                                        "Aucun Utilisateur Sélectioné",
                                        "Veuillez sélectionner au moins un utilisateur");
                                  }

                                  setState(() {
                                    validated = false;
                                  });
                                } else {
                                  widget.onTeamMembersAdded(
                                      userIdList, selectedUserList);
                                }
                              }
                            : () {})
                  ],
                ),
        ),
      ),
    );
  }
}
