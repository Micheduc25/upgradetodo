import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:upgrade_to_do/components/taskItem.dart';
import 'package:upgrade_to_do/models/taskModel.dart';
import 'package:upgrade_to_do/providers/taskProvider.dart';
import 'package:upgrade_to_do/utils/helperFunctions.dart';
import 'package:upgrade_to_do/utils/periods.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:upgrade_to_do/main.dart';

class CompletedTasks extends StatefulWidget {
  CompletedTasks(this.adminMode,
      {@required this.adminOrSupId,
      @required this.isAdminOrSupervisorMode,
        @required this.scrollController,
      this.period});
  final bool adminMode;
  final bool isAdminOrSupervisorMode;
  final int adminOrSupId;
  final Periods period;
  final ScrollController scrollController;

  @override
  _CompletedTasksState createState() => _CompletedTasksState();
}

class _CompletedTasksState extends State<CompletedTasks> {
  RefreshController controller = new RefreshController(initialRefresh: false);

  @override
  Widget build(BuildContext context) {
    List<Task> completedTasks =
        Provider.of<TaskProvider>(context).getCompletedTasks();

    if (widget.period == Periods.Week) {
      completedTasks =
          HelperFunction.filterTasksByPeriod(Periods.Week, completedTasks)["0"];
    } else if (widget.period == Periods.Month) {
      completedTasks = HelperFunction.filterTasksByPeriod(
          Periods.Month, completedTasks)["0"];
    } else if (widget.period == Periods.Year) {
      completedTasks =
          HelperFunction.filterTasksByPeriod(Periods.Year, completedTasks)["0"];
    }
    return Center(
        child: Container(
            child: completedTasks.isEmpty
                ? Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Image.asset("assets/logo.png"),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Il n'y a pas de taches complétées",
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 17,
                        ),
                        textAlign: TextAlign.center,
                      )
                    ],
                  )
                : SmartRefresher(
                    controller: controller,
                    enablePullDown: true,

                    header: WaterDropMaterialHeader(
                      backgroundColor: upgradeOrange,
                    ),
                    onRefresh: () async {
                      final res = await Provider.of<TaskProvider>(context,
                              listen: false)
                          .getAllMyTasks(
                              Provider.of<TaskProvider>(context, listen: false)
                                  .userId,
                              adminOrSupervisorMode:
                                  widget.isAdminOrSupervisorMode);

                      if (res == true)
                        controller.refreshCompleted();
                      else
                        controller.refreshFailed();
                    },
                    child: ListView.builder(
                        itemCount: completedTasks.length,
                        controller:widget.scrollController,
                        itemBuilder: (context, index) {
                          return TaskItem(
                            isAdminOrSupervisorMode:
                                widget.isAdminOrSupervisorMode,
                            adminOrSupId: widget.adminOrSupId,
                            isAdminMode: widget.adminMode,
                            showDelete: !widget.adminMode,
                            task: completedTasks[index],
                            showCheckBox: false,
                          );
                        }),
                  )));
  }
}
