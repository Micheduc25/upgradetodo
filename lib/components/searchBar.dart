import 'package:flutter/material.dart';
import 'package:upgrade_to_do/main.dart';

class SearchBar extends StatelessWidget {
  SearchBar(
      {this.searchController, this.onChange, this.focusNode, this.searchFor});
  final String searchFor;
  final FocusNode focusNode;
  final TextEditingController searchController;
  final Function(String text) onChange;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextField(
        controller: searchController,
        cursorColor: upgradeOrange,
        focusNode: this.focusNode,
        onSubmitted: (value) {
          focusNode.unfocus();
        },
        decoration: InputDecoration(
            prefixIcon: Icon(
              Icons.search,
              color: upgradeOrange,
            ),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(width: 3, color: upgradeOrange)),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(width: 2, color: upgradeBrown)),
            hintText: "Cherchez des" + " " + searchFor,
            hintStyle: TextStyle(
              color: upgradeBrown.withOpacity(.6),
              fontSize: 18,
            )),
        onChanged: (text) {
          onChange(text);
        },
      ),
    );
  }
}
