import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:upgrade_to_do/main.dart';

class SelectUserItem extends StatefulWidget {
  SelectUserItem(
      {@required this.onSelect,
      @required this.alreadyChoosen,
      this.initialValue = false,
      @required this.userName});

  final void Function(bool) onSelect;
  final String userName;
  final bool alreadyChoosen;
  final bool initialValue;
  @override
  _SelectUserItemState createState() => _SelectUserItemState();
}

class _SelectUserItemState extends State<SelectUserItem>
    with SingleTickerProviderStateMixin {
  bool checked;

  @override
  void initState() {
    checked = widget.initialValue;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.red,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            leading: !widget.alreadyChoosen
                ? InkWell(
                    child: Container(
                        width: 28,
                        height: 28,
                        // padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            border: Border.all(color: upgradeBrown, width: 2),
                            borderRadius: BorderRadius.circular(5)),
                        alignment: Alignment.center,
                        child: checked
                            ? Icon(
                                Icons.check,
                                color: upgradeBrown,
                                size: 25,
                              )
                            : null),
                  )
                : Container(
                    width: 0,
                    height: 0,
                  ),
            title: Text("${widget.userName}",
                style: widget.alreadyChoosen
                    ? TextStyle(
                        color: Colors.grey,
                        fontSize: 18,
                        fontWeight: FontWeight.bold)
                    : TextStyle(color: upgradeBrown, fontSize: 18)),
            onTap: () {
              if (!widget.alreadyChoosen) {
                setState(() {
                  checked = !checked;
                  widget.onSelect(checked);
                });
              }
            },
          ),
          Divider(
            color: upgradeBrown,
          )
        ],
      ),
    );
  }
}
