import 'package:flutter/material.dart';
import 'package:loading_animations/loading_animations.dart';
import 'package:provider/provider.dart';
import 'package:upgrade_to_do/components/loadingDialog.dart';
import 'package:upgrade_to_do/main.dart';
import 'package:upgrade_to_do/providers/adminTaskProvider.dart';
import 'package:upgrade_to_do/providers/taskProvider.dart';
import 'package:upgrade_to_do/screens/homeScreen.dart';
import 'package:upgrade_to_do/utils/settings.dart';

class UserListTile extends StatefulWidget {
  UserListTile(
      {this.userId,
      this.isSuperAdmin = false,
      @required this.whenTapped,
      this.userName,
      @required this.userRole,
      this.isAdmin,
      @required this.hasSupervisor,
      this.isSupervisor = false,
      this.supervisorId,
      this.userIsSupervisor,
      this.settings});

  final String userName;
  final String userRole;
  final bool isSupervisor;
  final bool userIsSupervisor;
  final bool hasSupervisor;
  final int supervisorId;

  final bool isSuperAdmin;
  final int userId;
  final bool isAdmin;
  final Settings settings;
  final Function whenTapped;

  @override
  _UserListTileState createState() => _UserListTileState();
}

class _UserListTileState extends State<UserListTile> {
  bool deleteLoading = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        ListTile(
          leading: Text(
            "${widget.userId}",
            style: TextStyle(color: upgradeBrown),
          ),
          title: Text(
            "${widget.userName}",
            style: TextStyle(color: upgradeBrown, fontSize: 20),
          ),
          trailing: deleteLoading
              ? Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: LoadingFadingLine.circle(
                    backgroundColor: Colors.red,
                    size: 20,
                  ),
                )
              : InkWell(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: Icon(
                      Icons.delete,
                      color: Colors.red,
                      size: 25,
                    ),
                  ),
                  onTap: () async {
                    final focusNode = FocusScope.of(context);
                    if (focusNode.hasPrimaryFocus) {
                      focusNode.unfocus();
                    }
                    final result = await showDialog<bool>(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            title: Text(widget.isSupervisor
                                ? "Enlever Utilisateur"
                                : "Supprimer Utilisateur"),
                            content: Text(widget.isSupervisor
                                ? "Voulez vous vraiment enlever cet Utilisateur de l'équipe?"
                                : "Voulez vous vraiment suprimmer cet Utilisateur?"),
                            actions: <Widget>[
                              FlatButton(
                                  onPressed: () {
                                    Navigator.of(context).pop(false);
                                  },
                                  child: Text(
                                    "Non",
                                    style: TextStyle(color: upgradeOrange),
                                  )),
                              FlatButton(
                                  onPressed: () {
                                    Navigator.of(context).pop(true);
                                  },
                                  child: Text("Oui",
                                      style: TextStyle(color: upgradeOrange)))
                            ],
                          );
                        });

                    if (result != null && result == true) {
                      setState(() {
                        deleteLoading = true;
                      });

                      showLoadingDialog(
                          widget.isSupervisor
                              ? "Chargement"
                              : "Suppression en cours...",
                          context);
                      try {
                        if (widget.isSupervisor) {
                          print("removing team member");
                          await Provider.of<AdminTaskProvider>(context,
                                  listen: false)
                              .removeTeamMember(
                                  widget.supervisorId, widget.userId, context);

                          setState(() {
                            deleteLoading = false;
                          });
                        } else
                          await Provider.of<AdminTaskProvider>(context,
                                  listen: false)
                              .deleteUser(widget.userId);
                      } catch (e) {
                        print("could not delete user $e");
                      } finally {
                        Navigator.of(context).pop();
                      }

                      if (mounted) {
                        setState(() {
                          deleteLoading = false;
                        });
                      }
                    }
                  },
                ),
          onTap: () async {
            if (widget.whenTapped != null) widget.whenTapped();
            final focusNode = FocusScope.of(context);
            if (focusNode.hasPrimaryFocus) {
              focusNode.unfocus();
            }
            Provider.of<TaskProvider>(context, listen: false).setUserId =
                widget.userId;
            Provider.of<AdminTaskProvider>(context, listen: false)
                .setUserLoading = true;
            print("before getting tasks");
            final res = await Provider.of<TaskProvider>(context, listen: false)
                .getAllMyTasks(widget.userId, adminOrSupervisorMode: true);
            print("after getting taks");
            Provider.of<AdminTaskProvider>(context, listen: false)
                .setUserLoading = false;

            if (widget.isSupervisor) {
              Navigator.of(context).pop();
            }

            if (res == true ||
                (widget.isSuperAdmin && res != null ||
                    (widget.isAdmin == true && res != null)))
              await Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => MyHomePage(
                      adminMode: widget.isSuperAdmin == true ||
                              widget.isSupervisor == true
                          ? false
                          : true,
                      hasSupervisor: widget.hasSupervisor,
                      userRole: widget.userRole,
                      adminId: widget.isSuperAdmin
                          ? Provider.of<AdminTaskProvider>(context).adminId
                          : null,
                      offlineMode: Provider.of<TaskProvider>(context)
                          .settings
                          .offlineMode
                          .getValue(),
                      isSuperAdmin: widget.isSuperAdmin,
                      title: "Taches de ${widget.userName}",
                      showDrawer: false,
                      supId: widget.userIsSupervisor
                          ? widget.userId
                          : widget.supervisorId != null
                              ? widget.supervisorId
                              : null,
                      disableTaskCheck: widget.isSuperAdmin ? false : true,
                      userId: widget.userId,
                      userName: widget.userName,
                      isSupervisor: widget.isSupervisor,
                      settings: widget.settings)));
            else
              await showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: Text("Info"),
                      content: Text(res == null
                          ? "Il y a eu un problème de connexion au serveur"
                          : "Cet Utilisateur n'a pas encore de taches"),
                    );
                  });
          },
          subtitle: Text(
            widget.userRole,
            style: TextStyle(fontSize: 13),
          ),
        ),
        Divider(
          color: upgradeBrown,
        )
      ],
    );
  }
}
