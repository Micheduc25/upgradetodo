import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:upgrade_to_do/components/taskItem.dart';
import 'package:upgrade_to_do/models/taskModel.dart';
import 'package:upgrade_to_do/providers/taskProvider.dart';
import 'package:upgrade_to_do/utils/helperFunctions.dart';
import 'package:upgrade_to_do/utils/periods.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:upgrade_to_do/main.dart';

class ActiveTasks extends StatefulWidget {
  ActiveTasks(this.adminMode,
      {@required this.adminOrSupId,
      @required this.isAdminOrSupervisorMode,
        @required this.scrollController,
      this.period});
  final bool adminMode;
  final bool isAdminOrSupervisorMode;
  final ScrollController scrollController;

  final int adminOrSupId;
  final Periods period;

  @override
  _ActiveTasksState createState() => _ActiveTasksState();
}

class _ActiveTasksState extends State<ActiveTasks> {
  RefreshController controller = new RefreshController(initialRefresh: false);
  @override
  Widget build(BuildContext context) {
    List<Task> activeTasks =
        Provider.of<TaskProvider>(context).getActiveTasks();

    if (widget.period == Periods.Week) {
      activeTasks =
          HelperFunction.filterTasksByPeriod(Periods.Week, activeTasks)["0"];
    } else if (widget.period == Periods.Month) {
      activeTasks =
          HelperFunction.filterTasksByPeriod(Periods.Month, activeTasks)["0"];
    } else if (widget.period == Periods.Year) {
      activeTasks =
          HelperFunction.filterTasksByPeriod(Periods.Year, activeTasks)["0"];
    }
    return Center(
        child: Container(
            child: activeTasks.isEmpty
                ? Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Image.asset("assets/logo.png"),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Il n'y a pas de taches en attente",
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 17,
                        ),
                        textAlign: TextAlign.center,
                      )
                    ],
                  )
                : SmartRefresher(
                    controller: controller,
                    enablePullDown: true,

                    header: WaterDropMaterialHeader(
                      backgroundColor: upgradeOrange,
                    ),
                    onRefresh: () async {
                      final res = await Provider.of<TaskProvider>(context,
                              listen: false)
                          .getAllMyTasks(
                              Provider.of<TaskProvider>(context, listen: false)
                                  .userId,
                              adminOrSupervisorMode:
                                  widget.isAdminOrSupervisorMode);

                      if (res == true)
                        controller.refreshCompleted();
                      else
                        controller.refreshFailed();
                    },
                    child: ListView.builder(
                        itemCount: activeTasks.length,
                        controller:widget.scrollController,
                        itemBuilder: (context, index) {
                          return TaskItem(
                            isAdminOrSupervisorMode:
                                widget.isAdminOrSupervisorMode,
                            isAdminMode: widget.adminMode,
                            showDelete: !widget.adminMode,
                            adminOrSupId: widget.adminOrSupId,
                            task: activeTasks[index],
                            showCheckBox: !widget.adminMode,
                          );
                        }),
                  )));
  }
}
