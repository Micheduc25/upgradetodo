import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:loading_animations/loading_animations.dart';
import 'package:provider/provider.dart';
import 'package:upgrade_to_do/main.dart';
import 'package:upgrade_to_do/models/taskModel.dart';
import 'package:upgrade_to_do/providers/taskProvider.dart';
import 'package:upgrade_to_do/screens/taskScreen.dart';

class TaskItem extends StatefulWidget {
  TaskItem(
      {@required this.task,
      this.showCheckBox = true,
      this.isAdminMode = false,
      @required this.adminOrSupId,
      @required this.isAdminOrSupervisorMode,
      @required this.showDelete});

  final Task task;
  final bool showCheckBox;
  final bool showDelete;
  final int adminOrSupId;
  final bool isAdminMode;
  final bool isAdminOrSupervisorMode;
  @override
  _TaskItemState createState() => _TaskItemState();
}

class _TaskItemState extends State<TaskItem>
    with SingleTickerProviderStateMixin {
  bool checkValue;
  bool loading;
  bool deleteLoading;

  AnimationController controller;
  Animation<TextStyle> textAnimation;

  @override
  void initState() {
    checkValue = widget.task.completed;
    loading = false;
    deleteLoading = false;

    controller = new AnimationController(
        value: widget.task.completed ? 1 : 0,
        vsync: this,
        duration: Duration(milliseconds: 300));
    textAnimation = TextStyleTween(
            begin: TextStyle(
                color: upgradeBrown, fontSize: 20, fontWeight: FontWeight.bold),
            end: TextStyle(
                color: Colors.grey,
                fontSize: 20,
                fontWeight: FontWeight.bold,
                decorationColor: Colors.grey,
                decoration: TextDecoration.lineThrough))
        .animate(controller)
          ..addListener(() {
            if (mounted) setState(() {});
          });

    super.initState();
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.task.completed) {
      controller.value = 1;
      checkValue = true;
    } else {
      controller.value = 0;
      checkValue = false;
    }
    return ListTile(
        leading: widget.showCheckBox
            ? loading
                ? Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      height: 20,
                      width: 20,
                      child: LoadingFilling.square(
                        size: 16,
                        fillingColor: upgradeBrown,
                      ),
                    ),
                  )
                : Transform.scale(
                    scale: 0.87,
                    child: Checkbox(
                      value: checkValue,
                      checkColor: upgradeOrange,
                      activeColor: upgradeBrown,
                      onChanged: (value) async {
                        setState(() {
                          loading = true;
                        });

                        final response = await Provider.of<TaskProvider>(
                                context,
                                listen: false)
                            .toggleTask(widget.task.id,
                                adminOrSupervisorMode:
                                    widget.isAdminOrSupervisorMode,
                                adminOrSupId: widget.adminOrSupId,
                                offlineMode: Provider.of<TaskProvider>(context,
                                        listen: false)
                                    .settings
                                    .offlineMode
                                    .getValue());

                        if (response)
                          setState(() {
                            checkValue = value;
                          });

                        if (value) {
                          final now = DateTime.now();
                          controller.forward();
                          widget.task.completedOn = " Complétée le " +
                              now.toIso8601String().split('T')[0] +
                              " à " +
                              now.toIso8601String().split('T')[1].split(".")[0];
                        }
                        Provider.of<TaskProvider>(context, listen: false)
                            .notifyListeners();

                        setState(() {
                          loading = false;
                        });
                        controller.reverse();
                      },
                    ),
                  )
            : widget.isAdminMode ? null : Container(width: 0, height: 0),
        title: Text(
            widget.task.title.length < 25
                ? widget.task.title
                : widget.task.title.substring(0, 25) + "...",
            style: textAnimation.value),
        trailing: deleteLoading
            ? Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: LoadingFadingLine.circle(
                  backgroundColor: Colors.red,
                  size: 16,
                ),
              )
            : !widget.isAdminMode
                ? InkWell(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Icon(
                        Icons.delete,
                        color: Colors.red,
                        size: 22,
                      ),
                    ),
                    onTap: () async {
                      final result = await showDialog<bool>(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: Text("Supprimer Tache"),
                              content: Text(
                                  "Voulez vous vraiment supprimer cette tache?"),
                              actions: <Widget>[
                                FlatButton(
                                    onPressed: () {
                                      Navigator.of(context).pop(false);
                                    },
                                    child: Text(
                                      "Non",
                                      style: TextStyle(color: upgradeOrange),
                                    )),
                                FlatButton(
                                    onPressed: () {
                                      Navigator.of(context).pop(true);
                                    },
                                    child: Text("Oui",
                                        style: TextStyle(color: upgradeOrange)))
                              ],
                            );
                          });

                      if (result != null && result == true) {
                        setState(() {
                          deleteLoading = true;
                        });
                        try {
                          await Provider.of<TaskProvider>(context,
                                  listen: false)
                              .deleteTask(widget.task.id,
                                  adminOrSupervisorMode:
                                      widget.isAdminOrSupervisorMode,
                                  adminOrSupId: widget.adminOrSupId,
                                  offlineMode: Provider.of<TaskProvider>(
                                          context,
                                          listen: false)
                                      .settings
                                      .offlineMode
                                      .getValue());
                        } catch (e) {
                          print("could not delete task $e");
                        }

                        setState(() {
                          deleteLoading = false;
                        });
                      }
                    },
                  )
                : null,
        // isThreeLine: true,
        subtitle: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Icon(
                  Icons.date_range,
                  color: upgradeOrange,
                  size: 20,
                ),
                SizedBox(
                  width: 7,
                ),
                Text(
                  widget.task.addedOn,
                  style: TextStyle(
                      color: upgradeBrown,
                      fontSize: 11,
                      fontWeight: FontWeight.bold),
                )
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Provider.of<TaskProvider>(context, listen: false)
                .generatePriorityColor(widget.task.priority),
            SizedBox(
              height: 10,
            )
          ],
        ),
        onTap: () {
          // print(widget.task);
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => TaskScreen(
                  isAdminOrSupervisorMode: widget.isAdminOrSupervisorMode,
                  adminOrSupId: widget.adminOrSupId,
                  isAdminMode: widget.isAdminMode,
                  task: widget.task)));
        });
  }
}
