import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:upgrade_to_do/main.dart';
import 'package:upgrade_to_do/screens/homeScreen.dart';
import 'package:upgrade_to_do/utils/homeScreenSettings.dart';
import 'package:upgrade_to_do/utils/periods.dart';

class ExecutionIndicator extends StatefulWidget {
  ExecutionIndicator(
      {@required this.rate,
      @required this.color,
      @required this.title,
      @required this.period,
      @required this.homeSettings,
      @required this.stats});
  final double rate;
  final Color color;
  final String title;
  final Periods period;
  final Map<String, int> stats;
  final HomeScreenSettings homeSettings;
  @override
  _ExecutionIndicatorState createState() => _ExecutionIndicatorState();
}

class _ExecutionIndicatorState extends State<ExecutionIndicator>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  @override
  void initState() {
    super.initState();

    if (!widget.rate.isNaN) {
      controller = new AnimationController(
          vsync: this,
          duration: Duration(milliseconds: 1000),
          upperBound: widget.rate);

      controller.addListener(() {
        if (mounted) setState(() {});
      });

      if (widget.rate != 0) {
        controller.forward();
      }
    }
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  title: Text("Détails"),
                  content: Column(
                    // mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                    builder: (context) =>
                                        MyHomePage.fromSettings(
                                            widget.homeSettings,
                                            0,
                                            widget.period)));
                          },
                          child: Text("Total : ${widget.stats['total']}",
                              style: TextStyle(
                                  fontSize: 19, color: upgradeBrown))),
                      SizedBox(
                        height: 10,
                      ),
                      InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                    builder: (context) =>
                                        MyHomePage.fromSettings(
                                            widget.homeSettings,
                                            1,
                                            widget.period)));
                          },
                          child: Text("En Attente : ${widget.stats['pending']}",
                              style: TextStyle(
                                  fontSize: 19, color: upgradeBrown))),
                      SizedBox(
                        height: 10,
                      ),
                      InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                    builder: (context) =>
                                        MyHomePage.fromSettings(
                                            widget.homeSettings,
                                            2,
                                            widget.period)));
                          },
                          child: Text("Fait : ${widget.stats['completed']}",
                              style: TextStyle(
                                  fontSize: 19, color: upgradeBrown))),
                    ],
                  ),
                ));
      },
      child: Container(
        child: CircularPercentIndicator(
          header: Text(
            widget.title,
            textAlign: TextAlign.center,
            style: TextStyle(color: upgradeBrown, fontSize: 20),
          ),
          backgroundColor: Colors.grey[100],
          progressColor: widget.color,
          circularStrokeCap: CircularStrokeCap.round,
          lineWidth: 10,
          radius: MediaQuery.of(context).size.width * 0.4,
          percent: controller != null ? controller.value / 100 : 0,
          center: Text(
            controller != null
                ? '${controller.value.toStringAsFixed(2)}%'
                : "0%",
            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}
