import 'package:flutter/material.dart';
import 'package:upgrade_to_do/main.dart';
import 'package:upgrade_to_do/screens/loginOrSignup/inputEntry.dart';

class PasswordInput extends StatefulWidget {
  PasswordInput({
    this.controller,
    this.hintText,
    this.label,
    @required this.focusNode,
    this.finalField,
    @required this.onSubmitted,
    @required this.inputAction,
    this.validate,
  });

  final TextEditingController controller;
  final String hintText;
  final bool finalField;
  final FocusNode focusNode;
  final String Function(String) validate;
  final TextInputAction inputAction;
  final Function(String) onSubmitted;

  final String label;

  @override
  _PasswordInputState createState() => _PasswordInputState();
}

class _PasswordInputState extends State<PasswordInput> {
  bool showPassword = false;
  @override
  Widget build(BuildContext context) {
    return Input(
        controller: widget.controller,
        hintText: widget.hintText,
        validate: widget.validate,
        focusNode: widget.focusNode,
        finalField: widget.finalField,
        onSubmitted: (val) => widget.onSubmitted(val),
        inputAction: widget.inputAction,
        obScureText: !showPassword,
        suffixIcon: InkWell(
          child: Icon(
            showPassword ? Icons.visibility_off : Icons.visibility,
            color: upgradeOrange,
          ),
          onTap: () {
            setState(() {
              showPassword = !showPassword;
            });
          },
        ),
        label: widget.label);
  }
}
