import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:upgrade_to_do/components/loadingDialog.dart';
import 'package:upgrade_to_do/providers/adminTaskProvider.dart';
import 'package:upgrade_to_do/providers/supervisorTaskProvider.dart';
import 'package:upgrade_to_do/providers/taskProvider.dart';
import 'package:upgrade_to_do/screens/loginOrSignup/inputEntry.dart';

class ServerUrlInput extends StatelessWidget {
  ServerUrlInput({this.urlController});
  final TextEditingController urlController;


  @override
  Widget build(BuildContext context) {
    final settings = Provider.of<TaskProvider>(context).settings;
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Input(
              controller: urlController,
              hintText: "url du serveur",
              validate: null,
              onSubmitted: null,
              focusNode: null,
              inputAction: null,
              label: "url"),
          SizedBox(
            height: 20,
          ),
          RaisedButton(
              child: Text(
                "OK",
                style: TextStyle(color: Colors.white, fontSize: 18),
              ),
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              onPressed: () async{
                if (urlController.text != "") {
                  Provider.of<AdminTaskProvider>(context, listen: false)
                      .serverUrl = urlController.text;

                  Provider.of<TaskProvider>(context, listen: false).serverUrl =
                        urlController.text;

                  Provider.of<SupervisorTaskProvider>(context, listen: false)
                      .serverUrl = urlController.text;

                  await settings.serverUrl.setValue(urlController.text);
                  Navigator.of(context).pop();
                  showMessage(context, "Succès",
                      "l'URL du serveur est désormais ${urlController.text}");
                } else {
                  showMessage(context, "Info",
                      "l'URL du serveur ne doit pas etre null");
                }
              })
        ],
      ),
    );
  }
}
