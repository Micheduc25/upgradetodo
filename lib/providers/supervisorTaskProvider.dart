import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:upgrade_to_do/models/userModel.dart';
import 'package:upgrade_to_do/providers/adminTaskProvider.dart';
import 'package:upgrade_to_do/utils/globalConst.dart';
import 'package:upgrade_to_do/utils/helperFunctions.dart';

class SupervisorTaskProvider with ChangeNotifier {
  String serverUrl = "http://192.168.0.100:8000";
  List<User> teamMembers = [];
  int supervisorId;

  Future<void> getMyTeamMembers(int supervisorId) async {
    try {
      final res = await HelperFunction.getData(
          "$serverUrl/api/overseelist/$supervisorId");
      print(res.statusCode);
      print(teamMembers);

      if (res.statusCode == 200) {
        final memsMaps = json.decode(res.body)["oversee list"];

        this.teamMembers = [];

        memsMaps.forEach((map) {
          final mem = new User.fromMap(map);
          if (!this.teamMembers.contains(mem)) {
            this.teamMembers.add(mem);
          }
        });
        print("members are $memsMaps");
        notifyListeners();
      }
    } catch (err) {
      print("an error occured while getting team members $err");
    }
  }

  void setTeamMembersFromState(int supervisorId, BuildContext context) {
    this.teamMembers = Provider.of<AdminTaskProvider>(context, listen: false)
        .currentUsersAnnex
        .where((user) => user.supervisor == supervisorId)
        .toList();
    notifyListeners();
  }

  void removeTeamMemberFromList(int id) {
    this.teamMembers.removeWhere((element) => element.userId == id);
    notifyListeners();
  }
}
