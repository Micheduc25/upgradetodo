import 'package:flutter/material.dart';

class FormProvider with ChangeNotifier {
  bool autovalidateSignUp = false;
  bool autovalidateLogin = false;

  bool _signUpLoading = false;
  bool _loginLoading = false;

  bool rememberMeFlag = false;
  set setRememberMe(bool newValue) {
    this.rememberMeFlag = newValue;
    notifyListeners();
  }

  set setLoginAutoValidate(bool value) {
    this.autovalidateLogin = value;
    notifyListeners();
  }

  set setSignUpAutoValidate(bool value) {
    this.autovalidateSignUp = value;
    notifyListeners();
  }

  set loginLoading(bool value) {
    this._loginLoading = value;
    notifyListeners();
  }

  set signUpLoading(bool value) {
    this._signUpLoading = value;
    notifyListeners();
  }

  get loginLoading => this._loginLoading;
  get signUpLoading => this._signUpLoading;
}
