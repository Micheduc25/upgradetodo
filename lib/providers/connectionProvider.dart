import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:upgrade_to_do/providers/taskProvider.dart';

class ConnectionProvider with ChangeNotifier {
  ConnectionProvider(BuildContext context) {
    checkInternetConnection().listen((val) {
      connectionAvailable = val;
      Provider.of<TaskProvider>(context, listen: false)
          .updateOfflineModeSettings(val);
      notifyListeners();
    });
  }

  bool connectionAvailable;

  Stream<bool> checkInternetConnection() async* {
    while (true) {
      try {
        final result = await InternetAddress.lookup('google.com');
        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
          yield true;
          print("connected");
        }
      } on SocketException catch (_) {
        yield false;
        print('not connected');
      }

      await Future.delayed(Duration(seconds: 3));
    }
  }
}
