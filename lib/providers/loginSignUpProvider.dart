import 'dart:async';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:upgrade_to_do/database/DbOperations.dart';
import 'package:upgrade_to_do/models/userModel.dart';
import 'package:upgrade_to_do/providers/adminTaskProvider.dart';

import 'package:upgrade_to_do/providers/taskProvider.dart';
import 'package:upgrade_to_do/utils/Config.dart';

class LoginSignUpProvider with ChangeNotifier {
  static Future<dynamic> loginUser(BuildContext context,
      {@required String name,
      role = "user",
      @required String password,
      bool offlineMode = false}) async {
    String serverUrl =
        Provider.of<TaskProvider>(context, listen: false).serverUrl;

    print("login user with $serverUrl");
    print("username: $name  password : $password, offlineMode is $offlineMode");

    if (!offlineMode) {
      var client = http.Client();

      try {
        final res = await client
            .post(Uri.parse("$serverUrl/api/login/checklogin"),
                headers: <String, String>{
                  'Content-Type': 'application/json; charset=UTF-8',
                },
                body:
                    jsonEncode({"username": "$name", "password": "$password"}))
            .timeout(Duration(seconds: 60), onTimeout: () {
          throw new SocketException("too much time taken to login");
        }).catchError((err) {
          if (err is SocketException) {
            print("a socket exception occured ohh");
          }
          print("an error occured during post ----> $err");
        });

        print("response status is ${res.statusCode}");
        print("response body is :" + res.body);

        if (res.statusCode > 299) return null;
        print("response body is :" + res.body);

        final Map<String, dynamic> responseMap = json.decode(res.body);
        Provider.of<TaskProvider>(context, listen: false).setUserId =
            responseMap["id"];
        Provider.of<TaskProvider>(context, listen: false).setUserName =
            responseMap["username"];
        print("good till here");

        //we get updated and deleted tasks online
        print("role is ${responseMap[Config.role]}");
        if (responseMap[Config.role] == Config.user ||
            responseMap[Config.role] == Config.supervisor) {
          final List<String> updateStrings =
              responseMap[Config.updatedTasks]?.split(",") ?? [];

          List<int> tasksToUpdateLocally;

          try {
            tasksToUpdateLocally =
                updateStrings.map((str) => int.parse(str)).toList();
          } catch (e) {
            tasksToUpdateLocally = [];
          }

          final List<String> deleteStrings =
              responseMap[Config.deletedTasks]?.split(",") ?? [];

          print("deleteStrings are : $deleteStrings");

          List<int> tasksToDeleteLocally;

          try {
            tasksToDeleteLocally =
                deleteStrings.map((str) => int.parse(str)).toList();
          } catch (e) {
            tasksToDeleteLocally = [];
          }

          Provider.of<TaskProvider>(context, listen: false)
              .setTasksToUpdateOrDeleteIds(
                  tasksToUpdate: tasksToUpdateLocally,
                  tasksToDelete: tasksToDeleteLocally);

          print("We're here oh");

          //we fetch only the tasks of supervisors and normal users
          await Provider.of<TaskProvider>(context, listen: false).getAllMyTasks(
            responseMap["id"],
            adminOrSupervisorMode: false,
          );

          //after local updates and delete we set back the updatedTasks and
          //deletedTasks of the user to empty string

          if (tasksToUpdateLocally != [] || tasksToDeleteLocally != []) {
            await Provider.of<AdminTaskProvider>(context, listen: false)
                .updateUser(responseMap["id"],
                    {Config.updatedTasks: "", Config.deletedTasks: ""});
          }
        }

        return User(
            userId: responseMap[Config.id],
            supervisor: responseMap[Config.supervisor],
            role: responseMap[Config.role],
            userName: responseMap[Config.username]);
      } catch (err) {
        if (err is FormatException) return "infoError";
        if (err is SocketException) print("socket exception occured");

        print("An error occured during post here in login : $err");
        return null;
      } finally {
        client.close();
      }
    } else {
      final res = await DbOperations.loginUser(name, password);

      if (res != null) {
        Provider.of<TaskProvider>(context, listen: false).setUserId =
            res.userId;
        Provider.of<TaskProvider>(context, listen: false).setUserName =
            res.userName;
        print("user login successful...now getting tasks...");
        await Provider.of<TaskProvider>(context, listen: false).getAllMyTasks(
          res.userId,
          offlineMode: true,
          adminOrSupervisorMode: false,
        );
        return res;
      } else
        return "notLocal";
    }
  }

  static Future<Map<String, dynamic>> signUpUser(BuildContext context,
      {@required String name,
      fromAdmin = false,
      @required int supervisorId,
      String role = "user",
      @required String password}) async {
    String serverUrl =
        Provider.of<TaskProvider>(context, listen: false).serverUrl;
    print("username is $name and password is $password");

    var client = http.Client();

    try {
      final res = await client
          .post(Uri.parse("$serverUrl/api/login/register"),
              headers: <String, String>{
                'Content-Type': 'application/json; charset=UTF-8',
              },
              body: jsonEncode({
                "username": "$name",
                "role": "$role",
                "password": "$password",
              }))
          .timeout(Duration(seconds: 30), onTimeout: () {
        throw new SocketException("too much time taken to login");
      }).catchError((err) {
        print("an error occured during post ----> $err");
      });

      print(
          "Status code is ${res.statusCode} and response body is ${res.body}");
      if (res.statusCode > 299) return null;

      print("response status is ${res.statusCode}");
      final responseMap = json.decode(res.body);

      if (!fromAdmin) {
        Provider.of<TaskProvider>(context, listen: false).setUserId =
            responseMap["id"];
        Provider.of<TaskProvider>(context, listen: false).setUserName =
            responseMap["username"];
      }
      return {
        "id": responseMap["id"],
        Config.role: responseMap["Config.role"],
        Config.supervisor: responseMap[Config.supervisor],
        "username": responseMap["username"]
      };
    } catch (err) {
      print("An error occured during post : $err");
      return null;
    } finally {
      client.close();
    }
  }
}
