import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:upgrade_to_do/models/userModel.dart';
import 'package:upgrade_to_do/providers/loginSignUpProvider.dart';
import 'package:upgrade_to_do/providers/supervisorTaskProvider.dart';
import 'package:upgrade_to_do/utils/Config.dart';
import 'package:upgrade_to_do/utils/helperFunctions.dart';

class AdminTaskProvider with ChangeNotifier {
  String serverUrl = "http://192.168.0.100:8000";
  List<User> currentUsers = [];
  List<User> currentUsersAnnex = [];
  List<int> selectedUsersId = [];

  int adminId;
  bool userLoading = false;

  Future<void> getAllUsers(int adminId) async {
    final response =
        await HelperFunction.getData("$serverUrl/api/getallusers/$adminId");

    if (response != null) print("response status is ${response.statusCode}");

    if (response != null && response.statusCode == 200) {
      this.currentUsers = [];
      this.currentUsersAnnex = [];

      final decodedResponse = json.decode(response.body);

      // print("users are:  $decodedResponse");

      decodedResponse.forEach((map) {
        currentUsers.add(new User(
            userId: map[Config.id],
            supervisor: map[Config.supervisor],
            userName: map[Config.username],
            role: map[Config.role]));

        currentUsersAnnex.add(new User(
            userId: map[Config.id],
            supervisor: map[Config.supervisor],
            userName: map[Config.username],
            role: map[Config.role]));
      });

      notifyListeners();
    }
  }

  void setSelectUserList(List<int> userIdList) {
    this.selectedUsersId = userIdList;
    notifyListeners();
  }

  Future<User> addNewUser(String userName, String password, String role,
      int supervisor, BuildContext context) async {
    final data = await LoginSignUpProvider.signUpUser(context,
        name: userName,
        fromAdmin: true,
        role: role,
        supervisorId: supervisor,
        password: password);

    if (data != null) {
      this.currentUsersAnnex.add(new User.fromMap(data));
      notifyListeners();
      return new User(
          userId: data[Config.id],
          supervisor: data[Config.supervisor],
          userName: data[Config.username],
          role: data[Config.role]);
    } else
      return null;
  }

  Future<User> updateUser(int userId, Map<String, dynamic> dataMap) async {
    final res = await HelperFunction.putData(
        "$serverUrl/api/updateuser/$userId", dataMap);

    if (res != null) {
      print(res.body);

      if (res.statusCode == 200) {
        final Map<String, dynamic> userMap = jsonDecode(res.body);

        print("user successfully updated");

        return new User.fromMap(userMap);
      } else
        return null;
    }
    return null;
  }

  void modifyUser(int userId, Function(User, User) modif) {
    final user =
        this.currentUsersAnnex.singleWhere((user) => user.userId == userId);

    try {
      final user2 =
          this.currentUsers.singleWhere((user) => user.userId == userId);

      modif(user, user2);
    } catch (err) {
      modif(user, null);
    }

    notifyListeners();
  }

  ///Deletes a user, returns [true] if user is succesfully deleted else [false]
  /// `userId` is the id of the user which has to be removed from the database
  Future<bool> deleteUser(int userId) async {
    final res =
        await HelperFunction.deleteData("$serverUrl/api/destroyUser/$userId");
    await this.getAllUsers(adminId);

    notifyListeners();

    if (res != null)
      return true;
    else
      return false;
  }

  void searchUser(String searchKey, String searchBy, String roleToShow) {
    final role = roleToShow == "all" ? null : roleToShow;

    if (searchKey != "") {
      // print("Search key is $searchKey");
      if (searchBy == "id") {
        this.currentUsers = this.currentUsersAnnex.where((user) {
          if (user.role == null) user.role = "user";
          print(user.role);
          if ((user.userId == int.parse(searchKey)) &&
              (user.role == role || role == null))
            return true;
          else
            return false;
        }).toList();
      } else if (searchBy == "name") {
        this.currentUsers = this.currentUsersAnnex.where((user) {
          if (user.role == null) user.role = "user";
          if (user.userName.toLowerCase().startsWith(searchKey.toLowerCase()) &&
              (user.role == role || role == null))
            return true;
          else
            return false;
        }).toList();
      }
    } else
      this.currentUsers = this.currentUsersAnnex;

    notifyListeners();
  }

  set setUserLoading(bool newValue) {
    this.userLoading = newValue;
    notifyListeners();
  }

  Future<void> createSupervisor(int userId) async {
    final res =
        await HelperFunction.postData("$serverUrl/api/makesprvsr/$userId", {});

    print(res.statusCode);

    print(res.body);
  }

  Future<void> removeSupervisor(int supervisorId) async {
    final res = await HelperFunction.postData(
        "$serverUrl/api/removesprvsr/$supervisorId", {});

    print("supervisor removed response is ${res.statusCode}: ${res.body}");
  }

  Future<List<User>> getAllSupervisors(int adminId) async {
    List<User> sups = [];

    final res =
        await HelperFunction.getData("$serverUrl/api/getSupervisors/$adminId");

    if (res.statusCode == 200) {
      final supervisors = json.decode(res.body);
      print("supervisors: $supervisors");
      supervisors.forEach((map) {
        sups.add(new User.fromMap(map));
      });

      return sups;
    } else
      return null;
  }

  Future<bool> addTeamMembers(int supervisorId, List<int> membersId,
      BuildContext context, bool addingOneUser) async {
    print("adding team members....$membersId");

    final res = await HelperFunction.postData(
        "$serverUrl/api/oversee/$supervisorId", {"membersId": membersId});

    print(" response is ${res.body}");

    if (res != null) {
      if (res.statusCode == 200) {
        final List<dynamic> userMaps =
            jsonDecode(res.body)["oversee list"]; //list of added team members

        for (int i = 0; i < userMaps.length; i++) {
          final user = new User.fromMap(userMaps[i]);
          Provider.of<SupervisorTaskProvider>(context, listen: false)
              .teamMembers
              .add(user);

          // //we set the supervisor id for the user
          try {
            final myUser =
                Provider.of<AdminTaskProvider>(context, listen: false)
                    .currentUsersAnnex
                    .singleWhere((u) => u.userId == user.userId);
            myUser.supervisor = supervisorId;
          } catch (err) {
            print("element not found in currentUsersAnnex");
          }

          try {
            final thisUser =
                Provider.of<AdminTaskProvider>(context, listen: false)
                    .currentUsers
                    .singleWhere((u) => u.userId == user.userId);
            thisUser.supervisor = supervisorId;
          } catch (err) {
            print("User not found in currentUsers list $err");
          }
          // print("supervisor id set for ${myUser.userName}");
          // print(myUser.toMap());
        }
        notifyListeners();
        Provider.of<SupervisorTaskProvider>(context, listen: false)
            .notifyListeners();

        return true;
      } else
        return false;
    } else
      return false;
  }

  Future<bool> removeTeamMember(
      int supervisorId, memberId, BuildContext context) async {
    final res = await HelperFunction.deleteData(
        "$serverUrl/api/removeovrsmbr/$supervisorId/$memberId");

    if (res != null) {
      print(res.statusCode);
      if (res.statusCode == 200) {
        print("remove response is :" + res.body);

        Provider.of<AdminTaskProvider>(context, listen: false)
            .currentUsers
            .singleWhere((user) => user.userId == memberId)
            .supervisor = null;

        Provider.of<AdminTaskProvider>(context, listen: false)
            .currentUsersAnnex
            .singleWhere((user) => user.userId == memberId)
            .supervisor = null;
        Provider.of<AdminTaskProvider>(context, listen: false)
            .notifyListeners();

        Provider.of<SupervisorTaskProvider>(context, listen: false)
            .removeTeamMemberFromList(memberId);

        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}
