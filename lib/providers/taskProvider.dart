import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';
import 'package:upgrade_to_do/database/DbOperations.dart';
import 'package:upgrade_to_do/models/taskModel.dart';
import 'package:http/http.dart' as http;
import 'package:upgrade_to_do/providers/adminTaskProvider.dart';
import 'package:upgrade_to_do/utils/Config.dart';
import 'package:upgrade_to_do/utils/globalConst.dart';
import 'package:upgrade_to_do/utils/helperFunctions.dart';
import 'package:upgrade_to_do/utils/settings.dart';
import 'package:upgrade_to_do/utils/time.dart';

class TaskProvider with ChangeNotifier {
  String serverUrl = "http://192.168.0.100:8000";
  List<Task> tasks = [];
  bool deleteFlag = false;
  int completedTasks = 0;
  int activeTasks = 0;
  int totalTasks = 0;
  int lastindex = 1;
  String userName = "";
  int userId;
  List<int> tasksToUpdateLocally = [];
  List<int> tasksToDeleteLocally = [];
  Settings settings;

  TaskOrder taskOrder = TaskOrder.Date;

  Future<bool> getAllMyTasks(
    int userId, {
    bool offlineMode = false,
    @required bool adminOrSupervisorMode,
  }) async {
    //create tasks from the http response
    print("In (getAllMyTasks)");
    if (!offlineMode) {
      final client = http.Client();

      try {
        final res = await client
            .get(Uri.parse("$serverUrl/api/tasks/$userId"))
            .catchError((err) {
          print("an error occured during get $err");
          return null;
        }).timeout(Duration(seconds: 60), onTimeout: () {
          print("task getting timed out");
          return null;
        });
        if (res.body != "" && res.statusCode == 200) {
          this.tasks = [];
          final response = json.decode(res.body);

          for (int i = 0; i < response.length; i++) {
            response[i]["createPosition"] = i;
            print("createPosition: $i");

            // print("synchronized Set");
            this.tasks.add(Task.fromMap(response[i]));
          }

          orderTaskBy(this.taskOrder);

          redistributeTasks();
          print(
              "tasks to update locally: $tasksToUpdateLocally \n task to delete locally: $tasksToDeleteLocally");
          //Here we try to store the tasks that are found online into the local db
          if (!adminOrSupervisorMode) {
            if (tasks != null) {
              for (int i = 0; i < tasks.length; i++) {
                if (!tasks[i].synchronized) {
                  tasks[i].synchronized = true;
                  final res = await DbOperations.addTask(userId, tasks[i]);

                  if (res) {
                    await this.editOnlineTask(
                        tasks[i].id, {Config.synchronized: true});
                  }
                }
              }

              //we now locally delete tasks that have been deleted online by admins or supervisors
              for (int i = 0; i < tasksToDeleteLocally.length; i++) {
                await DbOperations.deleteTask(tasksToDeleteLocally[i]);
                print("deleted local task ${tasksToDeleteLocally[i]}");
              }

              //we update local tasks that have been updated online by admins or supervisors
              for (int i = 0; i < tasksToUpdateLocally.length; i++) {
                final updatedTask = this
                    .tasks
                    .singleWhere((task) => task.id == tasksToUpdateLocally[i]);

                if (updatedTask.synchronized == true) {
                  await DbOperations.updateTask(
                      tasksToUpdateLocally[i], updatedTask.toMap());
                }
                print("Updated local task ${tasksToUpdateLocally[i]}");
              }

              await DbOperations.displayDbTables();
            }
          }
          if (response.length > 0)
            return true;
          else
            return false;
        }

        return null;
      } catch (err) {
        print("Error occured while gettings tasks -------> $err");
        return null;
      } finally {
        client.close();
      }
    } else {
      if (this.tasks.isNotEmpty) this.tasks.clear();

      this.tasks = await DbOperations.getAllTasks(userId);
      await DbOperations.displayDbTables();
      notifyListeners();
      if (tasks != null)
        return true;
      else
        return false;
    }
  }

  Future<bool> addTask(int userId,
      {@required String title,
      @required String createdAt,
      @required String description,
      @required String date,
      @required bool tstate,
      @required bool adminOrSupervisorMode,
      @required TaskPriority taskPriority,
      @required bool forSynchronization,
      int localId,
      bool offlineMode = false}) async {
    print("In (addTask) offlineMode is $offlineMode");
    if (!offlineMode) {
      final client = http.Client();

      try {
        final res = await client
            .post(Uri.parse("$serverUrl/api/addtask/$userId"),
                headers: <String, String>{
                  'Content-Type': 'application/json; charset=UTF-8',
                },
                body: jsonEncode({
                  Config.title: "$title",
                  Config.description: description,
                  Config.tstate: tstate ?? false,
                  Config.synchronized: adminOrSupervisorMode ? false : true,
                  Config.taskPriority:
                      getTaskPriorityToServerFormat(taskPriority),
                  Config.endDate: date
                }))
            .catchError((err) {
          print("an error occured during get $err");
        });

        final response = json.decode(res.body);
        print("after adding task, response is  " + response.toString());

        //we assign the id generated by the api to the task in the  local db
        final taskId = response[Config.id];

        //if the task is successfully added online we also add it in the local database
        if (res.statusCode == 201 && !forSynchronization) {
          if (!adminOrSupervisorMode) {
            final res = await DbOperations.addTask(
                userId,
                new Task(
                    id: taskId,
                    addedOn: createdAt,
                    title: title,
                    date: date,
                    description: description,
                    lastModifiedOn: createdAt,
                    locallyCreated: false,
                    synchronized: true,
                    completed: false,
                    priority: taskPriority,
                    completedOn: date));

            if (res)
              print("Task Synchronized");
            else
              print("Task no Synchronized");
          }

          this.tasks.add(new Task(
              id: taskId,
              addedOn: createdAt,
              title: title,
              date: date,
              description: description,
              lastModifiedOn: createdAt,
              locallyCreated: false,
              synchronized: true,
              completed: false,
              priority: taskPriority,
              completedOn: date));

          await DbOperations.displayDbTables();

          redistributeTasks();
          orderTaskBy(this.taskOrder);
          notifyListeners();
        } else if (res.statusCode == 201 && forSynchronization == true) {
          //if the task was successfully added online, we now modify local id to the obtained id

          final res = await DbOperations.updateTask(
              localId, {Config.id: taskId, Config.synchronized: 'true'});

          print("current Tasks are $tasks and id is ${tasks[0].id}");

          final theTask =
              this.tasks.singleWhere((element) => element.id == localId);

          print("The task id is ${theTask.id}");
          theTask.id = taskId;

          if (res)
            print("task id updated");
          else {
            throw Exception("task id was not updated locally");
          }
        }

        redistributeTasks();
        orderTaskBy(this.taskOrder);
        notifyListeners();
        return true;
      } catch (err) {
        print("an error occured during task addition $err");
        return false;
      } finally {
        client.close();
      }
    } else {
      // print("taskProvider line 169, saving task locally...");
      final res = await DbOperations.addTask(
          userId,
          new Task(
              addedOn: createdAt,
              title: title,
              date: date,
              description: description,
              lastModifiedOn: createdAt,
              locallyCreated: true,
              synchronized: false,
              completed: false,
              priority: taskPriority,
              completedOn: date));

      if (res) {
        this.getAllMyTasks(
          userId,
          offlineMode: true,
          adminOrSupervisorMode: adminOrSupervisorMode,
        );
        redistributeTasks();
        orderTaskBy(this.taskOrder);
        notifyListeners();
      }

      return res;
    }
  }

  Future<bool> editOnlineTask(int taskId, Map<String, dynamic> newData) async {
    final client = new http.Client();

    try {
      final res = await client.put(
        Uri.parse("$ServerUrl/api/updatetask/$taskId"),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(newData),
      );
      print("update response is ${res.statusCode}");

      //if online update is successful we synchronize the task update locally
      if (res != null && res.statusCode == 200) {
        print("successfully updated task");
        return true;
      } else {
        print("task not updated, status code ${res.statusCode}");
        return false;
      }
    } catch (err) {
      print("An error occured during task update ----->$err");
      return false;
    } finally {
      client.close();
    }
  }

  editTask(int taskId, String tasknewTitle, String newDesc, String newDate,
      TaskPriority taskPriority,
      {bool offlineMode = false,
      @required bool adminOrSupervisorMode,
      @required int adminOrSupId,
      bool tstate,
      @required bool forSynchronization}) async {
    if (!offlineMode) {
      final theTask = tasks.singleWhere((task) {
        if (task.id == taskId)
          return true;
        else
          return false;
      });

      //we perform an http  post request to update the given task on the server through our api
      //and locally if the task is successfully updated on the server

      final client = new http.Client();

      try {
        print("adminId is $adminOrSupId userId is $userId");
        final res = await client.put(
          Uri.parse(adminOrSupervisorMode
              ? "$serverUrl/api/updatetask_ad/$adminOrSupId/$userId/$taskId"
              : "$serverUrl/api/updatetask/$taskId"),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode({
            Config.title: tasknewTitle,
            Config.description: newDesc,
            Config.tstate: theTask.completed ?? tstate,
            Config.taskPriority: getTaskPriorityToServerFormat(taskPriority),
            Config.endDate: newDate,
          }),
        );
        print("update response is ${res.statusCode} : \n ${res.body}");

        //if online update is successful we synchronize the task update locally if not in adminMode
        if (res.statusCode == 200) {
          if (!forSynchronization && !adminOrSupervisorMode) {
            final ans = await DbOperations.updateTask(taskId, {
              Config.id: taskId,
              Config.title: tasknewTitle,
              Config.description: newDesc,
              Config.tstate: theTask.completed ? "true" : "false",
              Config.taskPriority: getTaskPriorityToServerFormat(taskPriority),
              Config.endDate: newDate
            });
            if (ans) {
              print("task synchronized");
              await DbOperations.displayDbTables();
            } else
              print("task not synchronized after update");
          }

          await getAllMyTasks(userId,
              adminOrSupervisorMode: adminOrSupervisorMode);
        }

        notifyListeners();
        return true;
      } catch (err) {
        print("an error occured while updating the task $tasknewTitle  $err");
        return false;
      } finally {
        client.close();
      }
    } else {
      //and we add its id to the synchronization table with dync type update
      //since its now different from the online version
      final res = await DbOperations.updateTask(taskId, {
        Config.id: taskId,
        Config.title: tasknewTitle,
        Config.taskPriority: getTaskPriorityToServerFormat(taskPriority),
        Config.description: newDesc,
        Config.endDate: newDate,
      });

      if (res) {
        final bool isTaskOnline = await DbOperations.isTaskOnline(taskId);

        if (isTaskOnline)
          await DbOperations.addUnsyncTask(taskId, Config.update);
        // await getAllMyTasks(userId, offlineMode: true);

        final updatedTask = this.tasks.singleWhere((task) {
          if (task.id == taskId)
            return true;
          else
            return false;
        });

        if (updatedTask != null) {
          updatedTask.title = tasknewTitle;
          updatedTask.priority = taskPriority;
          updatedTask.description = newDesc;
          updatedTask.date = newDate;
        } else {
          print("updated task is null");
        }
      }

      await DbOperations.displayDbTables();

      notifyListeners();
      return res;
    }
  }

  List<Task> get getAllTasks => tasks;

  getTask(int taskId) {
    try {
      final task = tasks.singleWhere(
        (task) {
          if (task.id == taskId)
            return true;
          else
            return false;
        },
      );

      return task;
    } catch (err) {
      print("task not found");
    }
  }

  List<Task> getCompletedTasks() {
    return tasks.where((task) {
      if (task.completed)
        return true;
      else
        return false;
    }).toList();
  }

  List<Task> getActiveTasks() {
    return tasks.where((task) {
      if (!task.completed)
        return true;
      else
        return false;
    }).toList();
  }

  Future<bool> deleteTask(
    int taskId, {
    bool offlineMode = false,
    bool forSynchronization = false,
    @required int adminOrSupId,
    @required adminOrSupervisorMode,
  }) async {
    if (!offlineMode) {
      final res = await HelperFunction.deleteData(adminOrSupervisorMode
          ? "$serverUrl/api/destroytask_ad/$adminOrSupId/$userId/$taskId"
          : "$serverUrl/api/destroytask/$taskId/$userId");

      if (res != null) print("response is ${res.statusCode}: \n ${res.body}");

      if (res.statusCode == 200) {
        if (!forSynchronization && !adminOrSupervisorMode) {
          final ans = await DbOperations.deleteTask(taskId);
          if (ans)
            print("tasks synchronized after deletion");
          else
            print("tasks not synchronized after deletion");
        }

        // await this.getAllMyTasks(userId);
        this.tasks.removeWhere((task) {
          if (task.id == taskId)
            return true;
          else
            return false;
        });

        await DbOperations.displayDbTables();

        redistributeTasks();
        notifyListeners();

        return true;
      } else
        return false;
    } else {
      //when next user goes online we fetch  ids of deleted tasks and delete the tasks online
      //then we remove the ids of tasks which have been deleted online already from sync table
      //only tasks which are found online are added to the synchronization table
      //tasks not found online(with synchronization:false) are not added

      final bool isTaskOnline = await DbOperations.isTaskOnline(taskId);
      final res = await DbOperations.deleteTask(taskId);
      if (res) {
        //if task is online, we add it to the synchronization table
        if (isTaskOnline) {
          await DbOperations.addUnsyncTask(taskId, Config.delete);
          print("task found online, added to sync table");
        }
        this.tasks.removeWhere((task) {
          if (task.id == taskId)
            return true;
          else
            return false;
        });

        await DbOperations.displayDbTables();
        redistributeTasks();
        notifyListeners();
      }
      return res;
    }
  }

  set setDeleteFlag(bool value) {
    this.deleteFlag = value;
    notifyListeners();
  }

  redistributeTasks() {
    completedTasks = this.tasks.where((task) {
      if (task.completed)
        return true;
      else
        return false;
    }).length;

    this.activeTasks = this.tasks.length - completedTasks;

    this.totalTasks = this.tasks.length;
  }

  Future<bool> toggleTask(int taskId,
      {@required adminOrSupervisorMode,
      @required int adminOrSupId,
      bool offlineMode = false}) async {
    print("connection mode is $offlineMode");
    print(
        "print adminId is $adminOrSupId  adminOrSupMode is $adminOrSupervisorMode");

    final task = this.tasks.singleWhere((task) {
      if (task.id == taskId)
        return true;
      else
        return false;
    });
    if (!offlineMode) {
      final res = await HelperFunction.putData(
          adminOrSupervisorMode
              ? "$serverUrl/api/updatetask_ad/$adminOrSupId/$userId/$taskId"
              : "$serverUrl/api/updatetask/$taskId",
          {Config.tstate: !task.completed});

      print(" toggle response is : " + res?.body);

      if (res.statusCode == 200) {
        task.completed = !task.completed;

        if (!adminOrSupervisorMode) {
          final ans = await DbOperations.toggleTask(taskId, task.completed);

          if (ans) {
            print("Tasks synchronized");
            await DbOperations.displayDbTables();
          } else
            print("tasks not synchronized ");
        }

        redistributeTasks();
        notifyListeners();
        return true;
      } else {
        return false;
      }
    } else {
      final res = await DbOperations.toggleTask(taskId, !task.completed);
      //after toggling the task offline its no more synchronized
      //with its online version so we add it to the synchronization table
      //we add to sync table only if the task is found online

      final bool isTaskOnline = await DbOperations.isTaskOnline(taskId);
      print("isTaskOnline is  $isTaskOnline");

      if (isTaskOnline) await DbOperations.addUnsyncTask(taskId, Config.update);

      if (res) {
        task.completed = !task.completed;
        print("task toggled locally success!");
        await DbOperations.displayDbTables();

        redistributeTasks();
        notifyListeners();
      }
      return res;
    }
  }

  set setUserId(int userId) {
    this.userId = userId;
    notifyListeners();
  }

  set setUserName(String userName) {
    this.userName = userName;
    notifyListeners();
  }

  String getTaskPriorityToServerFormat(TaskPriority priority) {
    if (priority == TaskPriority.ToBeDone)
      return "A faire";
    else if (priority == TaskPriority.Important)
      return "Important";
    else if (priority == TaskPriority.Urgent)
      return "Urgent";
    else if (priority == TaskPriority.UrgentAndImportant)
      return "Urgent et Important";
    return "A faire";
  }

  List<Task> filterTaskByPriority(TaskPriority priority) {
    return this.tasks.where((task) {
      if (task.priority == priority)
        return true;
      else
        return false;
    }).toList();
  }

  Widget generatePriorityColor(TaskPriority priority) {
    return Container(
      width: double.infinity,
      height: 5,
      decoration: BoxDecoration(
          // borderRadius: BorderRadius.circular(10),
          color: priority == TaskPriority.ToBeDone
              ? Colors.green
              : priority == TaskPriority.Important
                  ? Colors.yellow[200]
                  : priority == TaskPriority.Urgent
                      ? Colors.orangeAccent
                      : Colors.red),
    );
  }

  void orderTaskBy(TaskOrder orderCriteria) {
    if (orderCriteria == TaskOrder.PriorityDown) {
      this.tasks.sort((task1, task2) {
        return HelperFunction.getPriorityValue(task1.priority)
            .compareTo(HelperFunction.getPriorityValue(task2.priority));
      });

      this.tasks = this.tasks.reversed.toList();
    } else if (orderCriteria == TaskOrder.PriorityUp) {
      this.tasks.sort((task1, task2) {
        return HelperFunction.getPriorityValue(task1.priority)
            .compareTo(HelperFunction.getPriorityValue(task2.priority));
      });

      // this.tasks = this.tasks.reversed.toList();
    } else if (orderCriteria == TaskOrder.Date) {
      this.tasks.sort((task1, task2) =>
          MyTime.getDateTimeFromString(task1.addedOn)
              .compareTo(MyTime.getDateTimeFromString(task2.addedOn)));
      this.tasks = this.tasks.reversed.toList();
    } else if (orderCriteria == TaskOrder.Alphabetic) {
      this.tasks.sort((task1, task2) =>
          task1.title.toLowerCase().compareTo(task2.title.toLowerCase()));
    }

    notifyListeners();
  }

  set setTaskOrderCriteria(TaskOrder criteria) {
    this.taskOrder = criteria;
    orderTaskBy(this.taskOrder);
    notifyListeners();
  }

  Future<Settings> getCurrentSettings() async {
    final prefs = await StreamingSharedPreferences.instance;
    this.settings = new Settings(prefs);
    notifyListeners();

    return this.settings;
  }

  Future<void> updateOfflineModeSettings(bool newValue) async {
    await this.settings.offlineMode.setValue(newValue);

    this.settings = await this.getCurrentSettings();
    notifyListeners();
  }

//Here we get all tasks with attribute synchronized = false and send them to online db;
//We get all tasks in sync table with sync type update and we update the on the online db;
//We get all taskIds in sync table with sync type delete and delete these tasks in online db;
  Future<bool> synchronizeTasks({saveOffline = false}) async {
    bool result = true;
    final locallyAddedUnsyncTasks =
        await DbOperations.getUnsyncTasksAddedOffline();

    for (int i = 0; i < locallyAddedUnsyncTasks.length; i++) {
      final tempRes = await this.addTask(userId,
          adminOrSupervisorMode: false,
          title: locallyAddedUnsyncTasks[i].title,
          createdAt: locallyAddedUnsyncTasks[i].addedOn,
          description: locallyAddedUnsyncTasks[i].description,
          date: locallyAddedUnsyncTasks[i].date,
          tstate: locallyAddedUnsyncTasks[i].completed,
          taskPriority: locallyAddedUnsyncTasks[i].priority,
          localId: locallyAddedUnsyncTasks[i].id,
          offlineMode: false,
          forSynchronization: true);

      if (tempRes == false) {
        result = false;
        break;
      }
    }

    if (!result) return false;

    final tasksToUpdate =
        await DbOperations.getUnsynchronizedTasks(Config.update);

    print(" tasks to update are $tasksToUpdate");

    for (int i = 0; i < tasksToUpdate.length; i++) {
      final tempRes = await this.editTask(
          tasksToUpdate[i].id,
          tasksToUpdate[i].title,
          tasksToUpdate[i].description,
          tasksToUpdate[i].date,
          tasksToUpdate[i].priority,
          adminOrSupervisorMode: false,
          adminOrSupId: null,
          tstate: tasksToUpdate[i].completed,
          forSynchronization: true,
          offlineMode: false);

      if (tempRes == false) {
        result = false;
        break;
      }
      await DbOperations.removeUnsyncTasks(tasksToUpdate[i].id);
    }

    if (!result) return false;

    final tasksToDelete =
        await DbOperations.getUnsynchronizedTasks(Config.delete);

    for (int i = 0; i < tasksToDelete.length; i++) {
      final tempRes = await this.deleteTask(tasksToDelete[i],
          adminOrSupId: null, adminOrSupervisorMode: false, offlineMode: false);

      if (tempRes == false) {
        result = false;
        break;
      }

      await DbOperations.removeUnsyncTasks(tasksToDelete[i]);
    }

    if (!result) return false;

    if (result) print("Successfully synchronized your tasks dude");

    return result;
  }

  void setTasksToUpdateOrDeleteIds(
      {List<int> tasksToUpdate, List<int> tasksToDelete}) {
    if (tasksToUpdate != null) this.tasksToUpdateLocally = tasksToUpdate;
    if (tasksToDelete != null) tasksToDeleteLocally = tasksToDelete;
    notifyListeners();
  }
}
