import 'package:upgrade_to_do/utils/Config.dart';

class User {
  int userId;
  String userName;
  String role;
  int supervisor;

  User({this.userId, this.userName, this.supervisor, this.role});

  factory User.fromMap(Map<String, dynamic> map) {
    return new User(
        userId: map[Config.id],
        role: map[Config.role] ?? "user",
        supervisor: map[Config.supervisor],
        userName: map[Config.username]);
  }

  Map<String, dynamic> toMap() {
    return {
      Config.id: this.userId,
      Config.role: this.role,
      Config.supervisor: this.supervisor,
      Config.username: this.userName
    };
  }
}
