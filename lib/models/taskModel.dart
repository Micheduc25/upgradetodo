import 'package:flutter/cupertino.dart';
import 'package:upgrade_to_do/utils/Config.dart';
import 'package:upgrade_to_do/utils/helperFunctions.dart';

///This describes the priority of a given task
enum TaskPriority { Urgent, Important, UrgentAndImportant, ToBeDone }

class Task {
  Task(
      {this.id,
      this.title,
      this.description,
      this.priority = TaskPriority.ToBeDone,
      this.date,
      this.addedOn,
      this.completedOn = "en attente",
      this.lastModifiedOn = "jamais modifié",
      this.createPosition,
      this.synchronized,
      @required this.locallyCreated,
      this.completed = false});
  int id;
  int createPosition;
  String title;
  String description;
  String date;
  String addedOn;
  String completedOn;
  String lastModifiedOn;
  TaskPriority priority;
  bool locallyCreated;
  bool completed;
  bool synchronized;

  factory Task.fromMap(Map<String, dynamic> map) {
    bool tstate;
    var tempTstate = map["tstate"];
    if (tempTstate == "false")
      tstate = false;
    else if (tempTstate == "true")
      tstate = true;
    else
      tstate = map["tstate"];

    return new Task(
        id: map["id"],
        createPosition: map["createPosition"],
        title: map["title"] ?? "Titre de la tache",
        description: map["description"] ?? "Description de la tache",
        locallyCreated:
            (map[Config.createdOffline] == 1 ? true : false) ?? false,
        priority: HelperFunction.getTaskPriorityToAppFormat(
                map[Config.taskPriority]) ??
            TaskPriority
                .ToBeDone, //change this later when api will be modifiedS
        completed: tstate,
        date: map[Config.endDate] ??
            "date not set", // since the date is not yet obtained from the api we just put a temporary for now
        addedOn: map["created_at"],
        synchronized: (map[Config.synchronized] == "true" ||
                map[Config.synchronized] == true)
            ? true
            : false ?? false,
        lastModifiedOn: map["updated_at"]);
  }

  Map<String, dynamic> toMap() {
    return {
      Config.id: this.id,
      Config.title: this.title,
      Config.description: this.description,
      Config.createdOffline: this.locallyCreated ? 1 : 0,
      Config.taskPriority:
          HelperFunction.getTaskPriorityToServerFormat(this.priority),
      Config.tstate: this.completed ? "true" : "false",
      Config.endDate: this.date,
      Config.createdAt: this.addedOn,
      Config.updatedAt: this.lastModifiedOn,
      Config.synchronized: this.synchronized ? "true" : "false"
    };
  }
}
