import 'package:flutter/material.dart';
import 'package:upgrade_to_do/main.dart';

class HelpScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Aide'),
      ),
      body: Center(
        child: Container(
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Image.asset(
                  "assets/logo.png",
                  width: 100,
                ),
                SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Text.rich(
                    TextSpan(
                        text: "Bienvenue dans la page d'aide de l'application ",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 17),
                        children: [
                          TextSpan(
                              text: "GOHZE ToDo.\n",
                              style: TextStyle(
                                color: upgradeOrange,
                              )),
                          TextSpan(
                              text: "Ceci est une application qui permet aux utilisateurs de créer, modifier, supprimer et" +
                                  " marquer les tâches comme terminées lorsqu'elles sont terminées\n\n"),
                          TextSpan(
                              text: "Comment créer mon compte?\n",
                              style: TextStyle(
                                  fontWeight: FontWeight.w900,
                                  fontSize: 19,
                                  color: upgradeOrange)),
                          TextSpan(
                            text: "Pour créer un nouveau compte il vous suffit d'aller su l'onglet \"SignUp\". Entrez" +
                                "  toutes les informations requises et puis appuyez sur le bouton \"Créer\". Dès que le" +
                                " nouveau compte sera créé vous serez redirigé vers la page d'acceuil dans laquelle vous " +
                                "pourrez ajouter des nouvelles tâches\n\n",
                          ),
                          TextSpan(
                              text: "Comment créer une nouvelle tâche?\n",
                              style: TextStyle(
                                  fontWeight: FontWeight.w900,
                                  fontSize: 19,
                                  color: upgradeOrange)),
                          TextSpan(
                              text: "Appuyez sur le bouton \"Ajouter Tâche\", ceci ouvrira une page dans laquelle vous" +
                                  " pourrez entrer toutes les informations concernant la tâche dont le titre, la description," +
                                  " la date de fin, et la prioritée de la tâche. Appuyez sur le bouton \"Créer\"; la tâche" +
                                  "sera aujouté dans votre liste de tâches\n\n "),
                          TextSpan(
                              text:
                                  "Prioritées des tâches et leurs couleurs:\n",
                              style: TextStyle(
                                  fontWeight: FontWeight.w900,
                                  fontSize: 17,
                                  color: upgradeOrange)),
                          TextSpan(
                              text: "A faire --->",
                              style: TextStyle(
                                fontWeight: FontWeight.w900,
                                fontSize: 17,
                              ),
                              children: [
                                TextSpan(
                                    text: " vert\n",
                                    style: TextStyle(color: Colors.green))
                              ]),
                          TextSpan(
                              text: "Important --->",
                              style: TextStyle(
                                fontWeight: FontWeight.w900,
                                fontSize: 17,
                              ),
                              children: [
                                TextSpan(
                                    text: " Jaune pâle\n",
                                    style: TextStyle(color: Colors.yellow[200]))
                              ]),
                          TextSpan(
                              text: "Urgent --->",
                              style: TextStyle(
                                fontWeight: FontWeight.w900,
                                fontSize: 17,
                              ),
                              children: [
                                TextSpan(
                                    text: " Orange\n",
                                    style:
                                        TextStyle(color: Colors.orangeAccent))
                              ]),
                          TextSpan(
                              text: "Urgent et Important --->",
                              style: TextStyle(
                                fontWeight: FontWeight.w900,
                                fontSize: 17,
                              ),
                              children: [
                                TextSpan(
                                    text: " Rouge\n\n",
                                    style: TextStyle(color: Colors.red))
                              ]),
                          TextSpan(
                            text: "Comment modifier une tâche?\n",
                            style: TextStyle(
                                fontWeight: FontWeight.w900,
                                fontSize: 19,
                                color: upgradeOrange),
                          ),
                          TextSpan(
                              text: "A partir de votre liste de tâches, appuyez sur la tâche que vous voulez modifier. Ceci ouvrira la page" +
                                  " de la tâche. Appuyez sur l'icône d'un crayon en blanc sur la bar en haut de l'écran, ceci ouvrira la page" +
                                  " de modification de la tâche. Dans cette page vous pourrez modifier les informations à propos de cette tâche" +
                                  ". Une fois que les modifications ont été faites appuyez sur le bouton modifier, et la tâche sera modifier avec succès\n\n "),
                          TextSpan(
                            text: "Comment supprimer une tâche?\n",
                            style: TextStyle(
                                fontWeight: FontWeight.w900,
                                fontSize: 19,
                                color: upgradeOrange),
                          ),
                          TextSpan(
                              text: "Vous pouvez supprimer les tâches de deux façons. Premièrement, à partir de votre liste de tâches, appuyez l'icon de la corbeille." +
                                  " Une boîte de dialogue apparaîtra et on vous démandera de confirmer la suppression. Appuyez sur \"Oui\" et la tâche sera supprimé. \n" +
                                  "Deuxièmement, A partir de l'écran de la tâche que vous voulez supprimer, appuyez sur l'icone de la corbeille sur la bar en haut de " +
                                  "l'écran et puis confirmez la suppression\n\n"),
                          TextSpan(
                            text: "Comment compléter une tâche?\n",
                            style: TextStyle(
                                fontWeight: FontWeight.w900,
                                fontSize: 19,
                                color: upgradeOrange),
                          ),
                          TextSpan(
                              text: "Pour marquer une tâche comme complète il faut juste appuyer sur le checkbox qui" +
                                  " est du coté gauche de la liste de tâches \n\n"),
                        ]),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
