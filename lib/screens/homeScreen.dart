import 'dart:async';

import 'package:flutter/material.dart';
import 'package:upgrade_to_do/components/userList.dart';
import 'package:upgrade_to_do/components/scrollWidget.dart';

import 'package:provider/provider.dart';
import 'package:upgrade_to_do/components/activeTasks.dart';
import 'package:upgrade_to_do/components/allTasks.dart';
import 'package:upgrade_to_do/components/completedTasks.dart';
import 'package:upgrade_to_do/components/loadingDialog.dart';
import 'package:upgrade_to_do/components/supervisorList.dart';
import 'package:upgrade_to_do/main.dart';
import 'package:upgrade_to_do/providers/adminTaskProvider.dart';
import 'package:upgrade_to_do/providers/formProvider.dart';
import 'package:upgrade_to_do/providers/supervisorTaskProvider.dart';
import 'package:upgrade_to_do/providers/taskProvider.dart';
import 'package:upgrade_to_do/screens/aboutScreen.dart';
import 'package:upgrade_to_do/screens/helpScreen.dart';
import 'package:upgrade_to_do/screens/loginOrSignup/loginOrSignupScreen.dart';

import 'package:upgrade_to_do/screens/newTaskScreen.dart';
import 'package:upgrade_to_do/screens/summaryScreen.dart';
import 'package:upgrade_to_do/screens/supervisorScreen/supervisorScreen.dart';
import 'package:upgrade_to_do/utils/Config.dart';
import 'package:upgrade_to_do/utils/helperFunctions.dart';
import 'package:upgrade_to_do/utils/homeScreenSettings.dart';
import 'package:upgrade_to_do/utils/periods.dart';
import 'package:upgrade_to_do/utils/settings.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage(
      {Key key,
      this.title,
      @required this.userId,
      @required this.settings,
      this.showDrawer = true,
      this.adminMode = false,
      this.disableTaskCheck = false,
      this.isSupervisor = false,
      this.isSuperAdmin = false,
      this.supId,
      this.isSupervisorMode,
      @required this.userRole,
      @required this.offlineMode,
      @required this.hasSupervisor,
      @required this.adminId,
      this.period,
      this.initialTab = 0,
      this.userName})
      : super(key: key);

  final String title;
  final bool isSupervisorMode;

  final String userName;
  final bool isSuperAdmin;
  final int supId;
  final int userId;
  final Settings settings;
  final bool showDrawer;
  final bool adminMode;
  final bool disableTaskCheck;
  final bool isSupervisor;
  final String userRole;
  final bool offlineMode;
  final bool hasSupervisor;
  final int adminId;
  final Periods period;
  final int initialTab;

  factory MyHomePage.fromSettings(
      HomeScreenSettings settings, int initTab, Periods period) {
    return MyHomePage(
      userId: settings.userId,
      settings: settings.settings,
      userRole: settings.userRole,
      offlineMode: settings.offlineMode,
      hasSupervisor: settings.hasSupervisor,
      adminId: settings.adminId,
      period: period,
      supId: settings.supId,
      isSupervisor: settings.isSupervisor,
      isSupervisorMode: settings.isSupervisorMode,
      userName: settings.userName,
      title: settings.title,
      showDrawer: settings.showDrawer,
      adminMode: settings.adminMode,
      disableTaskCheck: settings.disableTaskCheck,
      initialTab: initTab ?? settings.initialTab ?? 0,
      isSuperAdmin: settings.isSuperAdmin,
    );
  }

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  TextEditingController urlController;
  TabController tabController;
  bool offlineMode;
  String userRole;
  bool showUserList;
  bool showSupList;
  bool superVisorAssigned;
  StreamSubscription connectivitySubscription;
  ScrollController scrollController;
  bool scrollingDown;
  double referencePos;
  bool scrollable;


  @override
  void initState() {
     tabController = new TabController(
        initialIndex: widget.initialTab, length: 3, vsync: this);

     tabController.addListener(() {
      setState((){
        scrollingDown = true;
        scrollable = false;
        referencePos = 0;
      });

      scrollController.animateTo(0, duration: Duration(milliseconds:400), curve: Curves.easeIn);

    });
    urlController = new TextEditingController();
    scrollController = new ScrollController();

    userRole = widget.userRole;
    scrollable = false;
    showSupList = false;
    showUserList = false;


    scrollingDown = true;
    referencePos = 0;


    scrollController.addListener(() {
      final newPos = scrollController.offset;

      if (newPos > referencePos) {
        scrollingDown = true;
      } else if (newPos < referencePos) {
        scrollingDown = false;
      }

      setState(() {
        referencePos = newPos;
        if(scrollable==false&&scrollController.offset>0){
          scrollable = true;
        }

        if(scrollController.offset>=scrollController.position.maxScrollExtent && !scrollController.position.outOfRange){

            scrollingDown = false;
        }

        else if(scrollController.offset<=scrollController.position.minScrollExtent && !scrollController.position.outOfRange){

          scrollingDown = true;
        }
      });
    });

    super.initState();
  }

  @override
  void dispose() {
    connectivitySubscription?.cancel();
    scrollController?.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final data = Provider.of<TaskProvider>(context);
    offlineMode = data.settings.offlineMode
        .getValue(); //change this later to get stream value

    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      appBar: AppBar(
        // automaticallyImplyLeading: false,
        bottom: PreferredSize(
            child: Container(
              // color: Colors.red,
              child: Text(
                "Taux d'exécution: ${data.totalTasks != 0 ? ((data.completedTasks / data.totalTasks) * 100).toStringAsFixed(2) : 0}% ",
                style: TextStyle(color: Colors.white, fontSize: 17),
              ),
            ),
            preferredSize: Size(50, 0)),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(widget.title),
          ],
        ),
        actions: <Widget>[
          (widget.isSuperAdmin || widget.adminMode)
              ? Container()
              : InkWell(
                  child: Tooltip(
                    message: offlineMode ? "Hors ligne" : "En ligne",
                    child: Icon(
                        offlineMode
                            ? Icons.signal_wifi_off
                            : Icons.signal_wifi_4_bar,
                        size: 30),
                  ),
                  onTap: () async {
                    //if we were offline and the button is pressed we store the unsynchronized tasks online too
                    if (offlineMode) {
                      showLoadingDialog(
                          "Synchronization des tâches...", context);

                      final result = await Provider.of<TaskProvider>(context,
                              listen: false)
                          .synchronizeTasks();

                      final result2 = await Provider.of<TaskProvider>(context,
                              listen: false)
                          .getAllMyTasks(widget.userId,
                              adminOrSupervisorMode:
                                  widget.isSuperAdmin || widget.isSupervisor);

                      Navigator.of(context).pop();

                      showMessage(
                          context,
                          result && result2 ? "Succès" : "Echec",
                          result
                              ? "Les tâches ont été synchronisées avec succès😉"
                              : "Désolé les tâches n'ont pas été synchronisées, vérifiez votre connexion internet et réesayez😓");

                      if (result) {
                        await Provider.of<TaskProvider>(context, listen: false)
                            .updateOfflineModeSettings(!offlineMode);
                        setState(() {
                          offlineMode = !offlineMode;
                        });
                      }
                    } else {
                      await Provider.of<TaskProvider>(context, listen: false)
                          .updateOfflineModeSettings(!offlineMode);
                      setState(() {
                        offlineMode = !offlineMode;
                      });
                    }
                  },
                ),
          (showUserList || showSupList)
              ? Container()
              : PopupMenuButton<String>(
                  icon: Icon(
                    Icons.more_vert,
                    size: 30,
                  ),
                  onSelected: (value) async {
                    if (value == "refresh") {
                      showLoadingDialog(
                          "Rafraîchissement des tâches...", context);
                      await Provider.of<TaskProvider>(context, listen: false)
                          .getAllMyTasks(widget.userId,
                              offlineMode: Provider.of<TaskProvider>(context,
                                      listen: false)
                                  .settings
                                  .offlineMode
                                  .getValue(),
                              adminOrSupervisorMode:
                                  widget.isSupervisor || widget.isSuperAdmin);
                      Navigator.of(context).pop();
                    } else if (value == "assignSup") {
                      if (offlineMode) {
                        showMessage(
                            context,
                            "Info!",
                            "Il faut être en ligne pour assigner un superviseur " +
                                "appuyez sur l'icon du réseau et réessayez");
                      } else {
                        // Navigator.of(context).pop();
                        setState(() {
                          showSupList = true;
                        });
                      }
                    } else if (value == "assignUsers") {
                      setState(() {
                        showUserList = true;
                      });
                    }
                  },
                  itemBuilder: (context) => [
                        PopupMenuItem(
                            value: "refresh",
                            child: ListTile(
                              leading: Icon(
                                Icons.refresh,
                                color: upgradeBrown,
                                // size: 30,
                              ),
                              title: Text(
                                "Rafrachir",
                                style: TextStyle(
                                    color: upgradeBrown, fontSize: 17),
                              ),
                            )),
                        PopupMenuDivider(),
                        PopupMenuItem(
                          value: "sort",
                          child: SafeArea(
                            child: PopupMenuButton<String>(
                              offset: Offset(100, 0),
                              initialValue: Provider.of<TaskProvider>(context,
                                              listen: false)
                                          .taskOrder ==
                                      TaskOrder.Date
                                  ? "date"
                                  : Provider.of<TaskProvider>(context,
                                                  listen: false)
                                              .taskOrder ==
                                          TaskOrder.PriorityUp
                                      ? "priorityUp"
                                      : Provider.of<TaskProvider>(context,
                                                      listen: false)
                                                  .taskOrder ==
                                              TaskOrder.PriorityDown
                                          ? "priorityDown"
                                          : "alphabetic",
                              child: ListTile(
                                leading: Icon(
                                  Icons.sort,
                                  color: upgradeBrown,
                                  // size: 30,
                                ),
                                title: Text(
                                  "Trier par",
                                  style: TextStyle(
                                      color: upgradeBrown, fontSize: 17),
                                ),
                              ),
                              itemBuilder: (context) => [
                                PopupMenuItem(
                                    value: "date",
                                    child: Text(
                                      "Date de création",
                                      style: TextStyle(
                                          fontSize: 18, color: upgradeBrown),
                                    )),
                                PopupMenuDivider(),
                                PopupMenuItem(
                                    value: "priorityUp",
                                    child: Text(
                                      "Prioritée👆",
                                      style: TextStyle(
                                          fontSize: 18, color: upgradeBrown),
                                    )),
                                PopupMenuDivider(),
                                PopupMenuItem(
                                    value: "priorityDown",
                                    child: Text(
                                      "Prioritée👇",
                                      style: TextStyle(
                                          fontSize: 18, color: upgradeBrown),
                                    )),
                                PopupMenuDivider(),
                                PopupMenuItem(
                                    value: "alphabetic",
                                    child: Text(
                                      "Ordre alphabetique",
                                      style: TextStyle(
                                          fontSize: 18, color: upgradeBrown),
                                    ))
                              ],
                              onSelected: (value) {
                                if (value == "date") {
                                  Provider.of<TaskProvider>(context,
                                          listen: false)
                                      .setTaskOrderCriteria = TaskOrder.Date;
                                } else if (value == "priorityUp") {
                                  Provider.of<TaskProvider>(context,
                                              listen: false)
                                          .setTaskOrderCriteria =
                                      TaskOrder.PriorityUp;
                                } else if (value == "priorityDown") {
                                  Provider.of<TaskProvider>(context,
                                              listen: false)
                                          .setTaskOrderCriteria =
                                      TaskOrder.PriorityDown;
                                } else {
                                  Provider.of<TaskProvider>(context,
                                              listen: false)
                                          .setTaskOrderCriteria =
                                      TaskOrder.Alphabetic;
                                }

                                Navigator.of(context).pop();
                              },
                              onCanceled: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ),
                        widget.isSuperAdmin &&
                                !widget.isSupervisor &&
                                !widget.hasSupervisor
                            ? PopupMenuDivider()
                            : null,
                        widget.isSuperAdmin &&
                                !widget.isSupervisor &&
                                !widget.hasSupervisor
                            ? PopupMenuItem(
                                value: "toggleSupervisor",
                                child: ListTile(
                                  leading: Icon(
                                    Icons.people,
                                    color: upgradeBrown,
                                  ),
                                  title: Text(userRole == Config.supervisor
                                      ? "Destituer Superviseur"
                                      : "Rendre Superviseur"),
                                  onTap: () async {
                                    Navigator.of(context).pop();
                                    showLoadingDialog(
                                        userRole != Config.supervisor
                                            ? "Ajout de Superviseur..."
                                            : "destitution du superviseur",
                                        context);

                                    if (userRole != Config.supervisor) {
                                      await Provider.of<AdminTaskProvider>(
                                              context,
                                              listen: false)
                                          .createSupervisor(widget.userId);

                                      setState(() {
                                        userRole = Config.supervisor;
                                      });
                                    } else {
                                      await Provider.of<AdminTaskProvider>(
                                              context,
                                              listen: false)
                                          .removeSupervisor(widget.userId);

                                      setState(() {
                                        userRole = Config.user;
                                      });
                                    }

                                    Provider.of<AdminTaskProvider>(context,
                                            listen: false)
                                        .modifyUser(widget.userId,
                                            (user, user2) {
                                      user.role = userRole;
                                      if (user2 != null) user2.role = userRole;
                                    });

                                    Navigator.of(context).pop();
                                  },
                                ))
                            : null,
                        widget.isSuperAdmin && !widget.hasSupervisor
                            ? PopupMenuDivider()
                            : null,
                        widget.isSuperAdmin &&
                                !widget.isSupervisor &&
                                userRole != Config.supervisor &&
                                !widget.hasSupervisor
                            ? PopupMenuItem(
                                value: "assignSup",
                                child: ListTile(
                                  leading: Icon(
                                    Icons.group_add,
                                    color: upgradeBrown,
                                  ),
                                  title: Text(
                                    "affecter superviseur",
                                    style: TextStyle(color: upgradeBrown),
                                  ),
                                ))
                            : null,
                        widget.isSuperAdmin && userRole == Config.supervisor
                            ? PopupMenuItem(
                                value: "assignUsers",
                                child: ListTile(
                                  leading: Icon(
                                    Icons.person_add,
                                    color: upgradeBrown,
                                  ),
                                  title: Text(
                                    "affecter des membres d'équipe",
                                    style: TextStyle(color: upgradeBrown),
                                  ),
                                ))
                            : null
                      ]),
        ],
      ),
      drawer: (widget.isSuperAdmin ||
                  widget.isSupervisor ||
                  widget.showDrawer) &&
              (!showSupList && !showUserList)
          ? Drawer(
              child: SafeArea(
                child: Container(
                  color: upgradeOrange,
                  child: Column(
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          Container(
                            height: 150,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                image: DecorationImage(
                                    fit: BoxFit.contain,
                                    image: AssetImage("assets/logo.png"))),
                          ),
                          Container(
                            height: 150,
                            alignment: Alignment.center,
                            color: Colors.black.withOpacity(.75),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Icon(
                                  Icons.account_circle,
                                  color: Colors.white,
                                  size: 80,
                                ),
                                Text(
                                  "${widget.userName}",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 30,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      Expanded(
                        child: ListView(
                          children: <Widget>[
                            (userRole == Config.supervisor ||
                                        widget.isSuperAdmin ||
                                        widget.adminMode) &&
                                    userRole != "user"
                                ? ListTile(
                                    leading: Icon(
                                      Icons.people,
                                      color: Colors.white,
                                      size: 30,
                                    ),
                                    title: Text(
                                      (widget.isSuperAdmin || widget.adminMode)
                                          ? "Equipe de ${widget.userName}"
                                          : "Mon Equipe",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 20),
                                    ),
                                    onTap: () async {
                                      //go to help page
                                      Navigator.of(context).pop();
                                      showLoadingDialog(
                                          "Chargement...", context);

                                      if (Provider.of<AdminTaskProvider>(
                                                  context,
                                                  listen: false)
                                              .currentUsersAnnex ==
                                          []) {
                                        await Provider.of<
                                                    SupervisorTaskProvider>(
                                                context,
                                                listen: false)
                                            .getMyTeamMembers(
                                                widget.supId != null
                                                    ? widget.supId
                                                    : widget.userId);
                                      } else {
                                        Provider.of<SupervisorTaskProvider>(
                                                context,
                                                listen: false)
                                            .setTeamMembersFromState(
                                                widget.supId != null
                                                    ? widget.supId
                                                    : widget.userId,
                                                context);
                                      }

                                      Navigator.of(context).pop();

                                      await Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  SupervisorScreen(
                                                    adminMode:
                                                        widget.isSuperAdmin,
                                                    supId: widget.userId,
                                                    settings: widget.settings,
                                                    userName: widget.userName,
                                                  )));
                                    },
                                  )
                                : Container(
                                    width: 0,
                                    height: 0,
                                  ),
                            (userRole == Config.supervisor ||
                                        widget.isSuperAdmin ||
                                        widget.adminMode) &&
                                    userRole != Config.user
                                ? Divider(
                                    color: Colors.white,
                                    thickness: 2,
                                  )
                                : Container(
                                    width: 0,
                                    height: 0,
                                  ),
                            ListTile(
                              leading: Icon(
                                Icons.assessment,
                                color: Colors.white,
                                size: 30,
                              ),
                              title: Text(
                                "Bilan",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ),
                              onTap: () async {
                                //go to help page
                                Navigator.of(context).pop();

                                double rate = 0;
                                if (data.totalTasks != 0 &&
                                    data.completedTasks != 0) {
                                  rate = double.parse(
                                      ((data.completedTasks / data.totalTasks) *
                                              100)
                                          .toStringAsFixed(2));
                                }

                                print("rate is $rate");

                                await Navigator.of(context)
                                    .push(MaterialPageRoute(
                                        builder: (context) => SummaryScreen(
                                              executionRate: rate,
                                              userId: widget.userId,
                                              settings: widget.settings,
                                              userRole: userRole,
                                              offlineMode: widget.offlineMode,
                                              hasSupervisor:
                                                  widget.hasSupervisor,
                                              adminId: widget.adminId,
                                              period: widget.period,
                                              supId: widget.supId,
                                              isSupervisor: widget.isSupervisor,
                                              isSupervisorMode:
                                                  widget.isSupervisorMode,
                                              userName: widget.userName,
                                              title: widget.title,
                                              showDrawer: widget.showDrawer,
                                              adminMode: widget.adminMode,
                                              disableTaskCheck:
                                                  widget.disableTaskCheck,
                                              initialTab:
                                                  widget.initialTab ?? 0,
                                              isSuperAdmin: widget.isSuperAdmin,
                                            )));
                              },
                            ),
                            Divider(
                              color: Colors.white,
                              thickness: 2,
                            ),
                            ListTile(
                              leading: Icon(
                                Icons.help_outline,
                                color: Colors.white,
                                size: 30,
                              ),
                              title: Text(
                                "Aide",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ),
                              onTap: () async {
                                //go to help page
                                Navigator.of(context).pop();

                                await Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (context) => HelpScreen()));
                              },
                            ),
                            Divider(
                              color: Colors.white,
                              thickness: 2,
                            ),
                            ListTile(
                              leading: Icon(
                                Icons.info_outline,
                                color: Colors.white,
                                size: 30,
                              ),
                              title: Text(
                                "A Propos",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ),
                              onTap: () async {
                                //go to about page
                                Navigator.of(context).pop();
                                await Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (context) => AboutScreen()));
                              },
                            ),
                            Divider(
                              color: Colors.white,
                              thickness: 2,
                            ),
                            widget.showDrawer
                                ? ListTile(
                                    leading: Icon(
                                      Icons.exit_to_app,
                                      color: Colors.red,
                                      size: 30,
                                    ),
                                    title: Text(
                                      "Se déconnecter",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 20),
                                    ),
                                    onTap: () async {
                                      //logout
                                      await widget.settings.userName
                                          .setValue("default");
                                      await widget.settings.password
                                          .setValue("default");
                                      await Navigator.of(context)
                                          .pushReplacement(MaterialPageRoute(
                                              builder: (context) =>
                                                  ChangeNotifierProvider(
                                                    create: (context) =>
                                                        FormProvider(),
                                                    child: LoginOrSignUp(
                                                        settings:
                                                            widget.settings),
                                                  )));
                                    },
                                  )
                                : ListTile(
                                    leading: Icon(
                                      widget.isSuperAdmin
                                          ? Icons.home
                                          : Icons.group,
                                      color: Colors.white,
                                      size: 30,
                                    ),
                                    title: Text(
                                      widget.isSuperAdmin
                                          ? "List d'Utilisateurs"
                                          : "Membres de mon équipe",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 20),
                                    ),
                                    onTap: () {
                                      Navigator.of(context).pop();
                                      Navigator.of(context).pop();
                                    },
                                  )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          : null,
      body: Stack(
        fit: StackFit.expand,
        alignment: Alignment.center,
        children: <Widget>[
          Column(
            children: <Widget>[
              // SizedBox(
              //   height: 5,
              // ),
              widget.period != null
                  ? Text("Tâches de " +
                      (widget.period == Periods.Week
                          ? "cette semaine"
                          : widget.period == Periods.Month
                              ? "ce mois"
                              : "cet année"))
                  : Container(),
              Expanded(
                child: TabBarView(controller: tabController, children: [
                  AllTasks(

                    widget.adminMode,
                    scrollController:scrollController,

                    isAdminOrSupervisorMode:
                        widget.isSuperAdmin || widget.isSupervisor,
                    adminOrSupId: widget.isSupervisor
                        ? widget.supId
                        : widget.isSuperAdmin ? widget.adminId : null,
                    period: widget.period,
                  ),
                  ActiveTasks(
                    widget.adminMode,
                    scrollController: scrollController,
                    isAdminOrSupervisorMode:
                        widget.isSuperAdmin || widget.isSupervisor,
                    adminOrSupId: widget.isSupervisor
                        ? widget.supId
                        : widget.isSuperAdmin ? widget.adminId : null,
                    period: widget.period,
                  ),
                  CompletedTasks(
                    widget.adminMode,
                    scrollController: scrollController,
                    isAdminOrSupervisorMode:
                        widget.isSuperAdmin || widget.isSupervisor,
                    adminOrSupId: widget.isSupervisor
                        ? widget.supId
                        : widget.isSuperAdmin ? widget.adminId : null,
                    period: widget.period,
                  )
                ]),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5.0),
                child: TabBar(
                    indicatorColor: upgradeBrown,
                    controller: tabController,
                    indicatorWeight: 4,
                    labelPadding: EdgeInsets.symmetric(horizontal: 3),
                    tabs: [
                      Padding(
                          padding: EdgeInsets.only(bottom: 4),
                          child: Text(
                            "Tous (${data.totalTasks})",
                            textAlign: TextAlign.center,
                          )),
                      Text("En attente (${data.activeTasks})",
                          textAlign: TextAlign.center),
                      Text("Fait (${data.completedTasks})",
                          textAlign: TextAlign.center)
                    ]),
              ),
              SizedBox(
                height: 20,
              ),
              !widget.adminMode || widget.isSupervisor
                  ? FloatingActionButton.extended(
                      label: Text("Ajouter Tache"),
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => NewTaskScreen(
                                  adminOrSupervisorMode: widget.isSupervisor ||
                                      widget.isSuperAdmin,
                                  userId: widget.userId,
                                  adminOrSupId: widget.isSupervisor
                                      ? widget.supId
                                      : widget.isSuperAdmin
                                          ? widget.adminId
                                          : null,
                                  settings: widget.settings,
                                  editMode: false,
                                )));
                      },
                      tooltip: 'Increment',
                    )
                  : Container(
                      width: 0,
                      height: 0,
                    ),
              SizedBox(
                height: 10,
              )
            ],
          ),
          showSupList && userRole == Config.user
              ? SuperVisorList(
                  userId: widget.userId,
                  userName: widget.userName,
                  onWillPop: () async {
                    setState(() {
                      showSupList = false;
                    });
                    return false;
                  },
                  onSupervisorValidate: () {
                    setState(() {
                      showSupList = false;
                    });
                  })
              : Container(),
          (showUserList && userRole == Config.supervisor)
              ? UserList(
                  userName: widget.userName,
                  alreadySelectedUsers: [],
                  userId: widget.userId,
                  externalMode: false,
                  onWillPop: () async {
                    setState(() {
                      showUserList = false;
                    });
                    return false;
                  },
                  onUsersValidate: () async {
                    setState(() {
                      showUserList = false;
                    });

                    showLoadingDialog("Chargement...", context);

                    if (Provider.of<AdminTaskProvider>(context, listen: false)
                            .currentUsersAnnex ==
                        []) {
                      await Provider.of<SupervisorTaskProvider>(context,
                              listen: false)
                          .getMyTeamMembers(widget.supId != null
                              ? widget.supId
                              : widget.userId);
                    } else {
                      Provider.of<SupervisorTaskProvider>(context,
                              listen: false)
                          .setTeamMembersFromState(
                              widget.supId != null
                                  ? widget.supId
                                  : widget.userId,
                              context);
                    }

                    Navigator.of(context).pop();

                    await Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => SupervisorScreen(
                              adminMode: widget.isSuperAdmin,
                              supId: widget.userId,
                              settings: widget.settings,
                              userName: widget.userName,
                            )));
                  },
                )
              : Container(),

      scrollable==true?    Positioned(
            bottom: 0,
            right: 5,
            child: ScrollWidget(
                scrollController: scrollController,
                scrollingDown: scrollingDown,
                referencePos:referencePos
            ),
          ):Container()
        ],
      ),
    );
  }
}
