import 'package:flutter/material.dart';
import 'package:upgrade_to_do/main.dart';

class AboutScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("A Propos"),
      ),
      body: Center(
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text.rich(
                TextSpan(
                    text: "This application was created by ",
                    style: TextStyle(fontSize: 20),
                    children: [
                      TextSpan(
                          text: "Ndjock Michel Junior",
                          style: TextStyle(fontWeight: FontWeight.w700)),
                      TextSpan(
                        text: ", a young student of the",
                      ),
                      TextSpan(
                          text:
                              " National Higher Polytechnic Institute of the University of Bamenda",
                          style: TextStyle(
                              fontWeight: FontWeight.w700,
                              color: Colors.orange))
                    ]),
                textAlign: TextAlign.center,
                style: TextStyle(color: upgradeBrown, fontSize: 30),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
