import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:upgrade_to_do/components/loadingDialog.dart';
import 'package:upgrade_to_do/components/passwordInput.dart';
import 'package:upgrade_to_do/main.dart';
import 'package:upgrade_to_do/providers/formProvider.dart';
import 'package:upgrade_to_do/providers/loginSignUpProvider.dart';
import 'package:upgrade_to_do/providers/taskProvider.dart';
import 'package:upgrade_to_do/screens/loginOrSignup/inputEntry.dart';
import 'package:upgrade_to_do/utils/helperFunctions.dart';
import 'package:upgrade_to_do/utils/validators.dart';

class SignUpScreen extends StatelessWidget {
  SignUpScreen(
      {this.nameController,
      this.passwordController,
      this.emailController,
      @required this.tabController,
      @required this.confirmPasswordController,
      this.autovalidate = false,
      @required this.formKey,
      @required this.nameFocus,
      @required this.emailFocus,
      @required this.passwordFocus,
      @required this.passwordConfirmFocus});
  final TextEditingController nameController;
  final TextEditingController emailController;
  final TextEditingController passwordController;
  final TextEditingController confirmPasswordController;
  final TabController tabController;
  final FocusNode nameFocus;
  final FocusNode passwordFocus;
  final FocusNode passwordConfirmFocus;
  final FocusNode emailFocus;
  final bool autovalidate;

  final GlobalKey<FormState> formKey;
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: this.formKey,
        autovalidate: Provider.of<FormProvider>(context).autovalidateSignUp,
        child: Container(
          height: 600,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Image.asset(
                "assets/logo.png",
                height: 100,
              ),
              SizedBox(
                height: 30,
              ),
              Input(
                suffixIcon: null,
                controller: nameController,
                focusNode: nameFocus,
                inputAction: TextInputAction.next,
                onSubmitted: (val) {
                  HelperFunction.fieldFocusChange(
                      context, nameFocus, emailFocus);
                },
                label: "Nom",
                hintText: "Entrez votre nom",
                validate: (name) => Validator.textValidator(name),
              ),
              SizedBox(
                height: 20,
              ),
              Input(
                suffixIcon: null,
                focusNode: emailFocus,
                controller: emailController,
                inputAction: TextInputAction.next,
                onSubmitted: (val) {
                  HelperFunction.fieldFocusChange(
                      context, emailFocus, passwordFocus);
                },
                label: "Email",
                hintText: "Entrez votre addresse email",
                validate: (name) => Validator.emailValidator(name),
              ),
              SizedBox(
                height: 20,
              ),
              PasswordInput(
                controller: passwordController,
                focusNode: passwordFocus,
                inputAction: TextInputAction.next,
                onSubmitted: (val) {
                  HelperFunction.fieldFocusChange(
                      context, passwordFocus, passwordConfirmFocus);
                },
                label: "mot de passe",
                hintText: "Entrez votre mot de passe",
                validate: (name) => Validator.passwordValidator(name),
              ),
              SizedBox(
                height: 30,
              ),
              PasswordInput(
                controller: confirmPasswordController,
                focusNode: passwordConfirmFocus,
                inputAction: TextInputAction.done,
                onSubmitted: (val) async {
                  _signUpUser(context);
                },
                label: "confirmer mot de passe",
                hintText: "Entrez de nouveau le mot de passe",
                validate: (name) => Validator.passwordValidator(name),
              ),
              SizedBox(
                height: 30,
              ),
              !Provider.of<FormProvider>(context, listen: false).signUpLoading
                  ? RaisedButton(
                      onPressed: () => _signUpUser(context),
                      child: Text(
                        "SignUp",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                      color: upgradeBrown,
                      padding:
                          EdgeInsets.symmetric(vertical: 20, horizontal: 40),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                    )
                  : CircularProgressIndicator(
                      backgroundColor: upgradeBrown,
                    )
            ],
          ),
        ),
      ),
    );
  }

  void _signUpUser(BuildContext context) async {
    if (!Provider.of<TaskProvider>(context, listen: false)
        .settings
        .offlineMode
        .getValue()) {
      if (passwordController.text == confirmPasswordController.text) {
        if (formKey.currentState.validate()) {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
          Provider.of<FormProvider>(context, listen: false).signUpLoading =
              true;

          final userData = await LoginSignUpProvider.signUpUser(context,
              name: nameController.text,
              supervisorId: null,
              password: passwordController.text);
          // print("user id is ${userData["id"]}");

          if (userData != null) {
            nameController.clear();
            emailController.clear();
            passwordController.clear();
            confirmPasswordController.clear();

            Future.delayed(Duration(milliseconds: 3000), () {
              Provider.of<FormProvider>(context, listen: false).signUpLoading =
                  false;
              tabController.animateTo(0);
            });
            Scaffold.of(context).showSnackBar(SnackBar(
              content: Text(
                "Compte créé avec succès",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white),
              ),
              backgroundColor: upgradeBrown,
              duration: Duration(seconds: 3),
            ));
          } else {
            showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text("Erreur"),
                    content: Text(
                        "Une erreur s'est produite lors de la creation du compte"),
                  );
                });

            Provider.of<FormProvider>(context, listen: false).signUpLoading =
                false;
          }
        } else {
          Provider.of<FormProvider>(context, listen: false)
              .setSignUpAutoValidate = true;
          showDialog(
              context: context,
              builder: (context) {
                return AlertDialog(
                  title: Text("Erreur"),
                  content: Text("Vérifiez vos informations s'il vous plait"),
                );
              });
        }
      } else {
        showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                title: Text("Erreur"),
                content: Text("Les deux mots de passes ne correspondent pas"),
              );
            });
      }
    } else {
      showMessage(context, "Mode Hors Ligne",
          "Vous êtes hors ligne, connectez vous pour créer un compte");
    }
  }
}
