import 'dart:async';

import 'package:flutter/material.dart';

import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:upgrade_to_do/components/loadingDialog.dart';

import 'package:upgrade_to_do/components/serverUrlInput.dart';

import 'package:upgrade_to_do/main.dart';

import 'package:upgrade_to_do/screens/loginOrSignup/LoginScreen.dart';

import 'package:upgrade_to_do/screens/loginOrSignup/signUpScreen.dart';
import 'package:upgrade_to_do/utils/settings.dart';
import 'package:cross_connectivity/cross_connectivity.dart' as cros;

class LoginOrSignUp extends StatefulWidget {
  LoginOrSignUp({@required this.settings});

  final Settings settings;
  @override
  _LoginOrSignUpState createState() => _LoginOrSignUpState();
}

class _LoginOrSignUpState extends State<LoginOrSignUp>
    with SingleTickerProviderStateMixin {
  TextEditingController nameController;
  TextEditingController nameController2;
  TextEditingController emailController;
  GlobalKey<FormState> formKey;
  GlobalKey<FormState> sformKey;

  TextEditingController passwordController;
  TextEditingController passwordController2;
  TextEditingController confirmPasswordController2;
  TabController tabController;
  TextEditingController urlController;
  GlobalKey<ScaffoldState> scaffoldKey;

  FocusNode nameFocus1;
  FocusNode nameFocus2;
  FocusNode passwordFocus1;
  FocusNode passwordFocus2;
  FocusNode emailFocus;
  FocusNode comfirmPassFocus;

  StreamSubscription connectivitySubscription;

  @override
  void initState() {
    nameFocus1 = FocusNode();
    nameFocus2 = FocusNode();
    passwordFocus1 = FocusNode();
    passwordFocus2 = FocusNode();
    emailFocus = FocusNode();
    comfirmPassFocus = FocusNode();
    nameController = new TextEditingController();
    emailController = new TextEditingController();
    passwordController = new TextEditingController();
    urlController =
        new TextEditingController(text: widget.settings.serverUrl.getValue());
    scaffoldKey = new GlobalKey<ScaffoldState>();

    tabController = new TabController(length: 2, vsync: this);

    nameController2 = new TextEditingController();

    passwordController2 = new TextEditingController();
    confirmPasswordController2 = new TextEditingController();
    formKey = new GlobalKey<FormState>();
    sformKey = new GlobalKey<FormState>();

    KeyboardVisibilityNotification().addNewListener(onChange: (bool visible) {
      if (visible)
        print("Now visible");
      else
        print('no more visible');
    }, onHide: () {
      if (nameFocus1.hasFocus)
        nameFocus1.unfocus();
      else if (nameFocus2.hasFocus)
        nameFocus2.unfocus();
      else if (passwordFocus1.hasFocus)
        passwordFocus1.unfocus();
      else if (passwordFocus2.hasFocus)
        passwordFocus2.unfocus();
      else if (emailFocus.hasFocus)
        emailFocus.unfocus();
      else if (comfirmPassFocus.hasFocus) comfirmPassFocus.unfocus();
    });

    cros.Connectivity().isConnected.listen((connected) {
      if (connected) {
        print("cross says: you are connected");
        try {
          showMessage(context, "En Ligne", "Vous etes en ligne",
              dismissible: false, showActions: false);
          Future.delayed(Duration(milliseconds: 1500), () {
            Navigator.of(context).pop();
          });
        } catch (err) {
          print("could not show scaffold");
        }
      } else {
        print("cross says: you are not connected any longer");
        Future.delayed(Duration.zero, () {
          showMessage(context, "Hors Ligne", "Vous etes hors ligne",
              dismissible: false, showActions: false);
          Future.delayed(Duration(milliseconds: 1500), () {
            Navigator.of(context).pop();
          });
        });
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    connectivitySubscription?.cancel();
    nameController?.dispose();
    passwordController?.dispose();
    emailController?.dispose();
    nameController2?.dispose();
    passwordController2.dispose();
    urlController?.dispose();
    nameFocus1?.dispose();
    nameFocus2?.dispose();
    passwordFocus1?.dispose();
    passwordFocus2?.dispose();
    emailFocus?.dispose();
    comfirmPassFocus?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      //this floating action button is to be removed when server will be hosted
      floatingActionButton: FloatingActionButton(
          child: Icon(
            Icons.vpn_lock,
            color: Colors.white,
          ),
          onPressed: () async {
            showDialog(
              context: context,
              builder: (context) {
                return AlertDialog(
                  title: Text("Modifier URL Serveur"),
                  content: ServerUrlInput(
                    urlController: urlController,
                  ),
                );
              },
            );
          }),
      body: SafeArea(
        child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);

            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: Center(
            child: Column(
              children: <Widget>[
                TabBar(
                  controller: tabController,
                  tabs: [
                    Tab(
                      child: Text(
                        "Login",
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                    Tab(child: Text("SignUp", style: TextStyle(fontSize: 20)))
                  ],
                  indicatorColor: upgradeBrown,
                  indicatorSize: TabBarIndicatorSize.tab,
                  labelColor: Colors.white,
                  unselectedLabelColor: upgradeBrown,
                  indicator: BoxDecoration(color: upgradeBrown),
                ),
                Container(
                  height: 2,
                  color: upgradeBrown,
                ),
                Expanded(
                    child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: TabBarView(controller: tabController, children: [
                    LoginScreen(
                      settings: widget.settings,
                      formKey: formKey,
                      nameController: nameController,
                      passwordController: passwordController,
                      nameFocus: nameFocus1,
                      passwordFocus: passwordFocus1,
                    ),
                    SignUpScreen(
                      tabController: tabController,
                      formKey: sformKey,
                      nameController: nameController2,
                      passwordController: passwordController2,
                      confirmPasswordController: confirmPasswordController2,
                      emailController: emailController,
                      nameFocus: nameFocus2,
                      passwordFocus: passwordFocus2,
                      emailFocus: emailFocus,
                      passwordConfirmFocus: comfirmPassFocus,
                    )
                  ]),
                ))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
