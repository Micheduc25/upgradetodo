import 'package:flutter/material.dart';
import 'package:upgrade_to_do/main.dart';

class InputItem extends StatelessWidget {
  InputItem({
    this.controller,
  });

  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(
        controller: this.controller,
      ),
    );
  }
}

class Input extends TextFormField {
  Input(
      {Key key,
      @required this.controller,
      @required this.hintText,
      @required this.validate,
      this.finalField = false,
      @required this.onSubmitted,
      this.obScureText = false,
      this.autoFocus = false,
      @required this.inputAction,
      @required this.focusNode,
      this.suffixIcon,
      @required this.label})
      : super(
            key: key,
            controller: controller,
            decoration: InputDecoration(
              suffixIcon: suffixIcon,
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: upgradeBrown)),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: upgradeBrown)),
              hintText: hintText,
              labelText: label,
            ),
            keyboardType: label == 'Email'
                ? TextInputType.emailAddress
                : TextInputType.name,
            onFieldSubmitted: (value) => onSubmitted(value),
            autocorrect: true,
            focusNode: focusNode,
            autofocus: autoFocus,
            cursorColor: upgradeOrange,
            textInputAction: inputAction,
            obscureText: obScureText,
            validator: validate);

  final TextEditingController controller;
  final String label;
  final String hintText;
  final bool autoFocus;
  final Function(String) validate;
  final bool obScureText;
  final Widget suffixIcon;
  final bool finalField;
  final FocusNode focusNode;
  final Function(String) onSubmitted;
  final TextInputAction inputAction;
}
