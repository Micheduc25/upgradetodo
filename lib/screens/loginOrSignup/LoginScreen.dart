import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:upgrade_to_do/components/loadingDialog.dart';
import 'package:upgrade_to_do/components/passwordInput.dart';
import 'package:upgrade_to_do/database/DbOperations.dart';
import 'package:upgrade_to_do/main.dart';
import 'package:upgrade_to_do/providers/adminTaskProvider.dart';
import 'package:upgrade_to_do/providers/formProvider.dart';
import 'package:upgrade_to_do/providers/loginSignUpProvider.dart';
import 'package:upgrade_to_do/providers/taskProvider.dart';
import 'package:upgrade_to_do/screens/adminScreens/adminHome.dart';

import 'package:upgrade_to_do/screens/adminScreens/superAdminHome.dart';
import 'package:upgrade_to_do/screens/homeScreen.dart';
import 'package:upgrade_to_do/screens/loginOrSignup/inputEntry.dart';
import 'package:upgrade_to_do/utils/Config.dart';
import 'package:upgrade_to_do/utils/helperFunctions.dart';
import 'package:upgrade_to_do/utils/settings.dart';
import 'package:upgrade_to_do/utils/validators.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen(
      {this.nameController,
      this.passwordController,
      @required this.nameFocus,
      @required this.passwordFocus,
      @required this.formKey,
      @required this.settings});
  final Settings settings;
  final TextEditingController nameController;
  final TextEditingController passwordController;
  final FocusNode nameFocus;
  final FocusNode passwordFocus;

  final GlobalKey<FormState> formKey;
  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      autovalidateMode:
          Provider.of<FormProvider>(context).autovalidateLogin == true
              ? AutovalidateMode.always
              : AutovalidateMode.disabled,
      // autovalidate: Provider.of<FormProvider>(context).autovalidateLogin,
      child: SingleChildScrollView(
        child: Container(
          height: 600,
          // color: Colors.red,
          alignment: Alignment.center,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Image.asset(
                "assets/logo.png",
                height: 100,
              ),
              SizedBox(
                height: 30,
              ),
              Input(
                controller: nameController,
                suffixIcon: null,
                autoFocus: false,
                focusNode: nameFocus,
                inputAction: TextInputAction.next,
                onSubmitted: (val) {
                  print("changing focus from $nameFocus to $passwordFocus");
                  HelperFunction.fieldFocusChange(
                      context, nameFocus, passwordFocus);
                },
                label: "Nom",
                hintText: "Entrez votre nom",
                validate: (name) => Validator.textValidator(name),
              ),
              SizedBox(
                height: 20,
              ),
              PasswordInput(
                controller: passwordController,
                focusNode: passwordFocus,
                inputAction: TextInputAction.done,
                onSubmitted: (val) async {
                  _loginUser(context);
                },
                label: "mot de passe",
                hintText: "Entrez votre mot de passe",
                validate: (name) => Validator.passwordValidator(name),
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Checkbox(
                      checkColor: upgradeOrange,
                      activeColor: upgradeBrown,
                      value: Provider.of<FormProvider>(context, listen: false)
                          .rememberMeFlag,
                      onChanged: (newValue) {
                        Provider.of<FormProvider>(context, listen: false)
                            .setRememberMe = newValue;
                      }),
                  Text(
                    "Se souvenir de moi",
                    style: TextStyle(color: upgradeBrown),
                  )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              !Provider.of<FormProvider>(context, listen: false).loginLoading
                  ? RaisedButton(
                      onPressed: () => _loginUser(context),
                      child: Text(
                        "Login",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                      color: upgradeBrown,
                      padding:
                          EdgeInsets.symmetric(vertical: 20, horizontal: 40),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                    )
                  : CircularProgressIndicator(
                      backgroundColor: upgradeBrown,
                    ),
            ],
          ),
        ),
      ),
    );
  }

  void _loginUser(BuildContext context) async {
    if (nameController.text != "" &&
        passwordController.text != "" &&
        formKey.currentState.validate()) {
      FocusScopeNode currentFocus = FocusScope.of(context);

      if (!currentFocus.hasPrimaryFocus) {
        currentFocus.unfocus();
      }
      Provider.of<FormProvider>(context, listen: false).loginLoading = true;
      final userData = await LoginSignUpProvider.loginUser(context,
          name: nameController.text,
          password: passwordController.text,
          offlineMode: Provider.of<TaskProvider>(context, listen: false)
              .settings
              .offlineMode
              .getValue());

      if (Provider.of<FormProvider>(context, listen: false).rememberMeFlag ==
          true) {
        print("setting remember me...");

        Provider.of<TaskProvider>(context, listen: false)
            .settings
            .userName
            .setValue(nameController.text);
        Provider.of<TaskProvider>(context, listen: false)
            .settings
            .password
            .setValue(passwordController.text);
      }

      if (userData == "notLocal") {
        // await Provider.of<TaskProvider>(context,
        //         listen: false)
        //     .updateOfflineModeSettings(false);

        showMessage(context, "Erreur de Connexion",
            "Veuillez vérifier votre nom ou mot de passe et réesayez");
      }

      if (userData != null &&
          userData != "infoError" &&
          userData != "notLocal") {
        if (Provider.of<FormProvider>(context, listen: false).rememberMeFlag) {
          await this.settings.userName.setValue(nameController.text);

          await this.settings.password.setValue(passwordController.text);
        }

        final isUserLocal =
            await DbOperations.isUserRegistered(userData.userId);

        if (!isUserLocal) {
          final res = await DbOperations.addUser(userData.userId,
              nameController.text, passwordController.text, userData.role);

          print("User was added in local db with id: ${userData.userId}");

          await DbOperations.displayDbTables();
        }
        nameController.clear();
        passwordController.clear();
        print("user id is ${userData.userId}");

        if (userData.role == Config.administrator ||
            userData.role == Config.superAdministrator) {
          Provider.of<AdminTaskProvider>(context, listen: false).adminId =
              userData.userId;
          await Provider.of<AdminTaskProvider>(context, listen: false)
              .getAllUsers(userData.userId);
        }

        Provider.of<FormProvider>(context, listen: false).loginLoading = false;

        await Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => userData.role == Config.administrator
                ? AdminHome(
                    adminId: userData.userId,
                    adminName: userData.userName,
                    settings: this.settings,
                  )
                : userData.role == Config.superAdministrator
                    ? SuperAdminHome(
                        settings: this.settings,
                        adminId: userData.userId,
                        adminName: userData.userName)
                    : MyHomePage(
                        userId: userData.userId,
                        userRole: userData.role,
                        adminId: null,
                        hasSupervisor: userData.supervisor != null,
                        offlineMode: Provider.of<TaskProvider>(context)
                            .settings
                            .offlineMode
                            .getValue(),
                        isSupervisor: false,
                        title: "Mes Taches",
                        userName: userData.userName,
                        settings: this.settings,
                      )));
      } else {
        Provider.of<FormProvider>(context, listen: false).loginLoading = false;
        showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                title: Text("Erreur"),
                content: Text(userData == "infoError"
                    ? "Vérifiez vos informations s'il vous plait"
                    : "Une erreur s'est produite lors de la connexion au serveur"),
              );
            });
      }
    } else {
      Provider.of<FormProvider>(context, listen: false).setLoginAutoValidate =
          true;
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text("Erreur"),
              content: Text("Vérifiez vos informations s'il vous plait"),
            );
          });
    }
  }
}
