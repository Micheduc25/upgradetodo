import 'dart:async';

import 'package:flutter/material.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';

import 'package:provider/provider.dart';
import 'package:upgrade_to_do/components/loadingDialog.dart';
import 'package:upgrade_to_do/main.dart';
import 'package:upgrade_to_do/models/taskModel.dart';
import 'package:upgrade_to_do/providers/taskProvider.dart';
import 'package:upgrade_to_do/utils/settings.dart';

class NewTaskScreen extends StatelessWidget {
  NewTaskScreen(
      {this.editMode,
      this.initialDate,
      this.initialDescription,
      this.initialPriority = TaskPriority.ToBeDone,
      this.taskId,
      this.settings,
      @required this.adminOrSupervisorMode,
      @required this.userId,
      @required this.adminOrSupId,
      this.initialName});
  final String initialName;
  final bool editMode;
  final int userId;
  final TaskPriority initialPriority;
  final int taskId;
  final Settings settings;
  final bool adminOrSupervisorMode;
  final int adminOrSupId;

  final String initialDescription;
  final String initialDate;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(editMode ? "Modifier Tache" : 'Nouvelle Tache'),
          centerTitle: true,
        ),
        body: NewTaskScreenHome(
            editMode: editMode,
            initialName: initialName,
            adminOrSupId: adminOrSupId,
            settings: settings,
            userId: userId,
            initialPriority: initialPriority,
            initialDate: initialDate,
            adminOrSupervisorMode: adminOrSupervisorMode,
            initialDescription: initialDescription,
            taskId: taskId));
  }
}

class NewTaskScreenHome extends StatefulWidget {
  NewTaskScreenHome(
      {this.editMode,
      this.initialDate,
      this.initialDescription,
      this.taskId,
      this.settings,
      @required this.adminOrSupervisorMode,
      @required this.initialPriority,
      @required this.userId,
      this.adminOrSupId,
      this.initialName});
  final String initialName;
  final bool editMode;
  final int taskId;
  final int userId;
  final TaskPriority initialPriority;
  final Settings settings;
  final bool adminOrSupervisorMode;
  final int adminOrSupId;

  final String initialDescription;
  final String initialDate;
  @override
  _NewTaskScreenHomeState createState() => _NewTaskScreenHomeState();
}

class _NewTaskScreenHomeState extends State<NewTaskScreenHome> {
  TextEditingController nameController;
  TextEditingController descriptionController;
  String date;
  String addedOn;
  TaskPriority taskPriority;
  bool loading;
  bool isPageReady;
  StreamSubscription offlineModeSubscription;
  bool offlineMode;
  FocusNode nameFocus;
  FocusNode descriptionFocus;
  StreamSubscription keyboardVisibilitySubs;

  @override
  void initState() {
    loading = false;

    taskPriority = widget.initialPriority;
    nameController = new TextEditingController(
        text: widget.editMode ? widget.initialName : null);
    descriptionController = new TextEditingController(
        text: widget.editMode ? widget.initialDescription : null);
    date = widget.editMode
        ? widget.initialDate
        : DateTime.now().toIso8601String().split('T')[0];

    KeyboardVisibilityNotification().addNewListener(onChange: (bool visible) {
      if (visible)
        print("Now visible");
      else
        print('no more visible');
    }, onHide: () {
      if (nameFocus.hasFocus)
        nameFocus.unfocus();
      else if (descriptionFocus.hasFocus) descriptionFocus.unfocus();
    });

    nameFocus = new FocusNode();
    descriptionFocus = new FocusNode();

    offlineModeSubscription = widget.settings.offlineMode.listen((value) {
      setState(() {
        offlineMode = value;
        print("offlineMode set to $offlineMode");
      });
    });

    super.initState();
  }

  @override
  void dispose() {
    nameController?.dispose();
    descriptionController?.dispose();
    offlineModeSubscription?.cancel();
    nameFocus?.dispose();
    descriptionFocus?.dispose();
    keyboardVisibilitySubs?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if ((!widget.editMode &&
                (nameController.text != "" ||
                    descriptionController.text != "" ||
                    date != DateTime.now().toIso8601String().split('T')[0] ||
                    taskPriority != TaskPriority.ToBeDone)) ||
            (widget.editMode &&
                (nameController.text != widget.initialName ||
                    descriptionController.text != widget.initialDescription ||
                    date != widget.initialDate ||
                    taskPriority != widget.initialPriority))) {
          //if we are creating a new task if all fields are empty we can leave
          //but if a field has started to be filled we show a popup
          //same for editing mode if atleast one field is different from the initials we show the popup

          final res = await showMessage(context, "Quitter cette page",
              "voulez vous annuler les modifications que vous avez faites?",
              showActions: true, dismissible: false);

          if (res)
            return true;
          else
            return false;
        }

        //else we pop normally

        return true;
      },
      child: GestureDetector(
        onTap: () {
          if (nameFocus.hasFocus)
            nameFocus.unfocus();
          else if (descriptionFocus.hasFocus) descriptionFocus.unfocus();
        },
        child: SingleChildScrollView(
          child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                  TextField(
                    controller: nameController,
                    focusNode: nameFocus,
                    autofocus: false,
                    decoration: InputDecoration(
                      labelText: "Titre",
                      hintText: "Titre de la tache",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(color: upgradeBrown, width: 2),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(color: upgradeBrown, width: 2),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  TextField(
                    controller: descriptionController,
                    maxLines: 5,
                    focusNode: descriptionFocus,
                    decoration: InputDecoration(
                      labelText: "Description",
                      hintText: "Description de la tache",
                      alignLabelWithHint: true,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(color: upgradeBrown, width: 2),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(color: upgradeBrown, width: 2),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "A faire le ",
                        style: TextStyle(fontSize: 27),
                      ),
                      SizedBox(height: 10),
                      FlatButton(
                          onPressed: () async {
                            FocusScopeNode currentFocus =
                                FocusScope.of(context);

                            if (!currentFocus.hasPrimaryFocus) {
                              currentFocus.unfocus();
                            }
                            final tempTime = DateTime.now();
                            final dateTime = await showDatePicker(
                                context: context,
                                initialDatePickerMode: DatePickerMode.day,
                                initialDate: widget.editMode
                                    ? DateTime.parse(date)
                                    : DateTime.now(),
                                firstDate: tempTime,
                                lastDate: DateTime(2050, 12, 31));

                            setState(() {
                              if (dateTime != null)
                                date = dateTime.toIso8601String().split('T')[0];
                            });
                          },
                          color: upgradeBrown,
                          padding: EdgeInsets.symmetric(
                              horizontal: 15, vertical: 15),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          child: Text(
                            "$date ",
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          )),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  DropdownButtonFormField<TaskPriority>(
                      // focusNode: FocusScope.of(context),
                      value: taskPriority,
                      hint: Container(
                          padding: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 20),
                          color: upgradeBrown,
                          child: Text(
                            "Prioritée de la tache",
                            style: TextStyle(color: Colors.white),
                          )),
                      iconSize: 30,
                      icon: Icon(
                        Icons.arrow_drop_down,
                        color: upgradeBrown,
                      ),
                      decoration: InputDecoration(
                        labelText: "prioritée",
                        labelStyle: TextStyle(color: upgradeBrown),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide:
                              BorderSide(color: upgradeOrange, width: 2),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(color: upgradeBrown),
                        ),
                      ),
                      items: [
                        DropdownMenuItem(
                            value: TaskPriority.ToBeDone,
                            child: GestureDetector(
                              onTap: () {
                                FocusScopeNode currentFocus =
                                    FocusScope.of(context);

                                if (!currentFocus.hasPrimaryFocus) {
                                  currentFocus.unfocus();
                                }
                              },
                              child: Text(
                                "A faire",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 20, color: upgradeBrown),
                              ),
                            )),
                        DropdownMenuItem(
                            value: TaskPriority.Important,
                            child: GestureDetector(
                              onTap: () {
                                FocusScopeNode currentFocus =
                                    FocusScope.of(context);

                                if (!currentFocus.hasPrimaryFocus) {
                                  currentFocus.unfocus();
                                }
                              },
                              child: Text(
                                "Important",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 20, color: upgradeBrown),
                              ),
                            )),
                        DropdownMenuItem(
                            value: TaskPriority.Urgent,
                            child: GestureDetector(
                              onTap: () {
                                FocusScopeNode currentFocus =
                                    FocusScope.of(context);

                                if (!currentFocus.hasPrimaryFocus) {
                                  currentFocus.unfocus();
                                }
                              },
                              child: Text(
                                "Urgent",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 20, color: upgradeBrown),
                              ),
                            )),
                        DropdownMenuItem(
                            value: TaskPriority.UrgentAndImportant,
                            child: GestureDetector(
                              onTap: () {
                                FocusScopeNode currentFocus =
                                    FocusScope.of(context);

                                if (!currentFocus.hasPrimaryFocus) {
                                  currentFocus.unfocus();
                                }
                              },
                              child: Text(
                                "Urgent et Important",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 20, color: upgradeBrown),
                              ),
                            ))
                      ],
                      onChanged: (priority) {
                        setState(() {
                          this.taskPriority = priority;
                          print("priority was set to  $taskPriority");
                        });
                      }),
                  SizedBox(
                    height: 20,
                  ),
                  loading
                      ? Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: CircularProgressIndicator(
                            backgroundColor: upgradeBrown,
                          ),
                        )
                      : RaisedButton(
                          onPressed: () async {
                            final now = DateTime.now();
                            if (nameController.text != "") {
                              FocusScopeNode currentFocus =
                                  FocusScope.of(context);

                              if (!currentFocus.hasPrimaryFocus) {
                                currentFocus.unfocus();
                              }
                              setState(() {
                                loading = true;
                              });
                              if (widget.editMode) {
                                final res = await Provider.of<TaskProvider>(
                                        context,
                                        listen: false)
                                    .editTask(
                                  widget.taskId,
                                  nameController.text,
                                  descriptionController.text,
                                  date,
                                  taskPriority,
                                  forSynchronization: false,
                                  adminOrSupId: widget.adminOrSupId,
                                  adminOrSupervisorMode:
                                      widget.adminOrSupervisorMode,
                                  offlineMode: Provider.of<TaskProvider>(
                                          context,
                                          listen: false)
                                      .settings
                                      .offlineMode
                                      .getValue(),
                                );
                                setState(() {
                                  loading = false;
                                });

                                if (res == true)
                                  Navigator.of(context).pop();
                                else {
                                  //show a dialog to indicate that an error occured
                                  showMessage(context, "Erreure",
                                      "Une erreure est survenu lors de la mise à jour de la tâche");
                                }
                              } else {
                                final res = await Provider.of<TaskProvider>(
                                        context,
                                        listen: false)
                                    .addTask(widget.userId,
                                        tstate: null,
                                        adminOrSupervisorMode:
                                            widget.adminOrSupervisorMode,
                                        title: nameController.text,
                                        createdAt: now
                                                .toIso8601String()
                                                .split("T")[0] +
                                            " " +
                                            now
                                                .toIso8601String()
                                                .split("T")[1]
                                                .split(".")[0],
                                        description: descriptionController.text,
                                        date: date +
                                            " " +
                                            now
                                                .toIso8601String()
                                                .split("T")[1]
                                                .split(".")[0],
                                        forSynchronization: false,
                                        offlineMode: offlineMode,
                                        taskPriority: taskPriority);
                                setState(() {
                                  loading = false;
                                });

                                if (res == true)
                                  Navigator.of(context).pop();
                                else {
                                  //show a dialog to indicate that an error occured
                                  showMessage(context, "Erreur d'ajout",
                                      "La tache n'a pas pu etre ajouté à cause d'une erreur de connection au serveur ");
                                }
                              }
                            }
                          },
                          child: Text(
                            widget.editMode ? "Modifier" : "Ajouter",
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                          color: upgradeBrown,
                          padding: EdgeInsets.symmetric(
                              horizontal: 30, vertical: 20),
                        )
                ],
              )),
        ),
      ),
    );
  }
}
