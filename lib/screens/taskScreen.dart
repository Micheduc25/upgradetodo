import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:upgrade_to_do/main.dart';
import 'package:upgrade_to_do/models/taskModel.dart';
import 'package:upgrade_to_do/providers/taskProvider.dart';
import 'package:upgrade_to_do/screens/newTaskScreen.dart';

class TaskScreen extends StatelessWidget {
  TaskScreen(
      {this.task,
      this.userId,
      @required this.adminOrSupId,
      @required this.isAdminOrSupervisorMode,
      this.isAdminMode});
  final Task task;
  final int userId;
  final bool isAdminMode;
  final bool isAdminOrSupervisorMode;
  final int adminOrSupId;
  @override
  Widget build(BuildContext context) {
    final Task newTask = Provider.of<TaskProvider>(context).getTask(task.id);

    print(newTask != null ? newTask : task);
    return Scaffold(
      appBar: AppBar(
        actions: !isAdminMode
            ? <Widget>[
                !task.completed
                    ? InkWell(
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Icon(
                            Icons.edit,
                            size: 30,
                          ),
                        ),
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => NewTaskScreen(
                                  userId: userId,
                                  taskId:
                                      newTask != null ? newTask.id : task.id,
                                  adminOrSupId: adminOrSupId,
                                  adminOrSupervisorMode:
                                      isAdminOrSupervisorMode,
                                  initialPriority: newTask != null
                                      ? newTask.priority
                                      : task.priority ?? null,
                                  editMode: true,
                                  initialName: newTask != null
                                      ? newTask.title
                                      : task.title ?? "",
                                  initialDescription: newTask != null
                                      ? newTask.description
                                      : task.description ?? "",
                                  settings: Provider.of<TaskProvider>(context)
                                      .settings,
                                  initialDate: newTask != null
                                      ? newTask.date
                                      : task.date ?? "")));
                        },
                      )
                    : Container(),
                InkWell(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Icon(
                      Icons.delete,
                      color: Colors.red,
                      size: 30,
                    ),
                  ),
                  onTap: () async {
                    final result = await showDialog<bool>(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            title: Text("Supprimer Tache"),
                            content: Text(
                                "Voulez vous vraiment supprimer cette tache?"),
                            actions: <Widget>[
                              FlatButton(
                                  onPressed: () {
                                    Navigator.of(context).pop(false);
                                  },
                                  child: Text("Non")),
                              FlatButton(
                                  onPressed: () {
                                    Navigator.of(context).pop(true);
                                  },
                                  child: Text("Oui"))
                            ],
                          );
                        });

                    if (result) {
                      await Provider.of<TaskProvider>(context, listen: false)
                          .deleteTask(newTask != null ? newTask.id : task.id,
                              adminOrSupId: adminOrSupId,
                              adminOrSupervisorMode: isAdminOrSupervisorMode);

                      if (result) Navigator.of(context).pop();
                    }
                  },
                ),
              ]
            : null,
        title: Text("Ma Tache"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Column(
            children: <Widget>[
              Column(
                children: [
                  DesciptionTile(
                    leadingIcon: Icon(
                      task.completed ? Icons.check : Icons.close,
                      color: task.completed ? Colors.green : Colors.red,
                    ),
                    content: newTask != null ? newTask.title : task.title ?? "",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    wordLimit: 40,
                  ),
                  Text(
                    task.completed || newTask.completed
                        ? "(task completed)"
                        : "(${getTimeLeft(newTask ?? task)})",
                    style: TextStyle(
                        color: task.completed || newTask.completed
                            ? Colors.green
                            : Colors.red,
                        fontWeight: FontWeight.bold),
                  )
                ],
              ),
              Divider(
                color: upgradeBrown,
                thickness: 1,
              ),
              DesciptionTile(
                leadingIcon: Icon(
                  Icons.library_books,
                  color: upgradeBrown,
                ),
                content:
                    newTask != null ? newTask.description : task.description,
                wordLimit: 80,
              ),
              Divider(
                color: upgradeBrown,
                thickness: 1,
              ),
              ListTile(
                leading: Icon(
                  Icons.date_range,
                  color: upgradeBrown,
                ),
                title: Text(
                  newTask != null ? newTask.date : task.date,
                  style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                ),
              ),
              Divider(
                color: upgradeBrown,
                thickness: 1,
              ),
              ListTile(
                title: Text(
                  "Prioritée : ${getTaskPriority(newTask != null ? newTask.priority : task.priority)}",
                  style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                ),
              ),
              ListTile(
                title: Text(
                  "Crée le :" +
                      (newTask != null ? newTask.addedOn : task.addedOn),
                  style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                ),
                subtitle: Text(
                  "Dernier modification : " +
                      (newTask != null
                          ? newTask.lastModifiedOn
                          : task.lastModifiedOn),
                  style: TextStyle(fontSize: 15),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  String getTaskPriority(TaskPriority priority) {
    if (priority == TaskPriority.ToBeDone)
      return "A faire";
    else if (priority == TaskPriority.Important)
      return "Important";
    else if (priority == TaskPriority.Urgent)
      return "Urgent";
    else if (priority == TaskPriority.UrgentAndImportant)
      return "Urgent et Important";
    return "A faire";
  }

  String getTimeLeft(Task theTask) {
    final DateTime startDate = DateTime.parse(theTask.addedOn);

    final DateTime endDate = DateTime.parse(theTask.date);

    final Duration timeLeft = endDate.difference(startDate);

    if (timeLeft.inDays > 0) {
      return "${timeLeft.inDays} day${timeLeft.inDays > 1 ? 's' : ''} left";
    } else if (timeLeft.inHours > 0) {
      return "${timeLeft.inHours} hour${timeLeft.inHours > 1 ? 's' : ''} left";
    } else if (timeLeft.inMinutes > 0) {
      return "${timeLeft.inMinutes} minute${timeLeft.inMinutes > 1 ? 's' : ''} left";
    } else {
      return "${timeLeft.inSeconds} second${timeLeft.inSeconds > 1 ? 's' : ''} left";
    }
  }
}

class DesciptionTile extends StatefulWidget {
  const DesciptionTile({
    Key key,
    @required this.content,
    @required this.leadingIcon,
    this.style,
    this.wordLimit = 70,
  }) : super(key: key);

  final String content;
  final int wordLimit;
  final Icon leadingIcon;
  final TextStyle style;

  @override
  _DesciptionTileState createState() => _DesciptionTileState();
}

class _DesciptionTileState extends State<DesciptionTile> {
  bool showReadMore;

  bool descriptionTooLong;
  @override
  void initState() {
    showReadMore = false;
    descriptionTooLong =
        widget.content.length > widget.wordLimit ? true : false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
        leading: widget.leadingIcon,
        title: Text(
          showReadMore || !descriptionTooLong
              ? widget.content
              : widget.content.substring(0, widget.wordLimit) + "..." ??
                  "Lorem Ipsum",
          style: widget.style ??
              TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
        ),
        trailing: descriptionTooLong
            ? InkWell(
                child: Icon(
                  !showReadMore ? Icons.expand_more : Icons.expand_less,
                  color: upgradeBrown,
                  size: 30,
                ),
                onTap: () {
                  setState(() {
                    showReadMore = !showReadMore;
                    // descriptionTooLong = !descriptionTooLong;
                  });
                },
              )
            : null);
  }
}
