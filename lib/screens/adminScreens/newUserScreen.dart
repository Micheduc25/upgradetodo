import 'package:flutter/material.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';

import 'package:provider/provider.dart';
import 'package:upgrade_to_do/components/loadingDialog.dart';
import 'package:upgrade_to_do/components/passwordInput.dart';
import 'package:upgrade_to_do/components/userList.dart';
import 'package:upgrade_to_do/main.dart';
import 'package:upgrade_to_do/models/userModel.dart';
import 'package:upgrade_to_do/providers/adminTaskProvider.dart';

import 'package:upgrade_to_do/providers/loginSignUpProvider.dart';
import 'package:upgrade_to_do/providers/taskProvider.dart';
import 'package:upgrade_to_do/screens/loginOrSignup/inputEntry.dart';
import 'package:upgrade_to_do/utils/Config.dart';
import 'package:upgrade_to_do/utils/helperFunctions.dart';
import 'package:upgrade_to_do/utils/validators.dart';

class CreateUserScreen extends StatefulWidget {
  CreateUserScreen(this.adminId);
  final int adminId;
  @override
  _CreateUserScreenState createState() => _CreateUserScreenState();
}

class _CreateUserScreenState extends State<CreateUserScreen> {
  TextEditingController nameController;
  TextEditingController emailController;
  TextEditingController passwordController;
  TextEditingController confirmPasswordController;
  String newUserRole;
  List<int> supervisorTeamMembersIds;
  List<User> supervisorTeamMembers;

  bool autovalidate;
  GlobalKey<FormState> formKey;

  bool signUpLoading;
  int supervisor;
  FocusNode nameFocus;
  FocusNode passwordFocus;
  FocusNode emailFocus;
  FocusNode confirmPassFocus;
  bool showUserList;

  @override
  void initState() {
    nameController = new TextEditingController();
    emailController = new TextEditingController();
    passwordController = new TextEditingController();
    confirmPasswordController = new TextEditingController();
    supervisorTeamMembers = [];
    supervisorTeamMembersIds = [];
    newUserRole = "user";
    formKey = GlobalKey<FormState>();
    signUpLoading = false;
    autovalidate = false;
    showUserList = false;

    nameFocus = FocusNode();
    passwordFocus = FocusNode();
    emailFocus = FocusNode();
    confirmPassFocus = FocusNode();

    KeyboardVisibilityNotification().addNewListener(onHide: () {
      if (nameFocus.hasFocus)
        nameFocus.unfocus();
      else if (passwordFocus.hasFocus)
        passwordFocus.unfocus();
      else if (emailFocus.hasFocus)
        emailFocus.unfocus();
      else if (confirmPassFocus.hasFocus) confirmPassFocus.unfocus();
    });

    super.initState();
  }

  @override
  void dispose() {
    nameController?.dispose();
    emailController?.dispose();
    passwordController?.dispose();
    confirmPasswordController?.dispose();
    nameFocus?.dispose();
    passwordFocus?.dispose();
    emailFocus?.dispose();
    confirmPassFocus?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Créer Utilisateur"),
        centerTitle: true,
      ),
      body: WillPopScope(
        onWillPop: () async {
          if (nameController.text != "" ||
              emailController.text != "" ||
              passwordController.text != "" ||
              confirmPasswordController.text != "" ||
              newUserRole != "user" ||
              supervisor != null) {
            final res = await showMessage(context, "Quitter cette page",
                "voulez vous annuler les modifications que vous avez faites?",
                showActions: true, dismissible: false);

            if (res)
              return true;
            else
              return false;
          }

          return true;
        },
        child: GestureDetector(
          onTap: () {
            if (nameFocus.hasFocus)
              nameFocus.unfocus();
            else if (emailFocus.hasFocus)
              emailFocus.unfocus();
            else if (passwordFocus.hasFocus)
              passwordFocus.unfocus();
            else if (confirmPassFocus.hasFocus) confirmPassFocus.unfocus();
          },
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Form(
                key: formKey,
                autovalidate: autovalidate,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  height: 600,
                  child: ListView(
                    children: <Widget>[
                      Image.asset(
                        "assets/logo.png",
                        height: 85,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Créer un nouvelle utilisateur",
                        textAlign: TextAlign.center,
                        style: TextStyle(color: upgradeBrown, fontSize: 24),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Input(
                        suffixIcon: null,
                        controller: nameController,
                        focusNode: nameFocus,
                        inputAction: TextInputAction.next,
                        onSubmitted: (val) {
                          HelperFunction.fieldFocusChange(
                              context, nameFocus, emailFocus);
                        },
                        label: "Nom",
                        hintText: "Nom de l'utilisateur",
                        validate: (name) => Validator.textValidator(name),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Input(
                        suffixIcon: null,
                        controller: emailController,
                        focusNode: emailFocus,
                        inputAction: TextInputAction.next,
                        onSubmitted: (val) {
                          HelperFunction.fieldFocusChange(
                              context, emailFocus, passwordFocus);
                        },
                        label: "Email",
                        hintText: "Addresse E-mail de l'utilisateur",
                        validate: (name) => Validator.emailValidator(name),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      PasswordInput(
                        controller: passwordController,
                        focusNode: passwordFocus,
                        inputAction: TextInputAction.next,
                        onSubmitted: (val) {
                          HelperFunction.fieldFocusChange(
                              context, passwordFocus, confirmPassFocus);
                        },
                        label: "mot de passe",
                        hintText: "Mot de passe de l'utilisateur",
                        validate: (name) => Validator.passwordValidator(name),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      PasswordInput(
                        controller: confirmPasswordController,
                        focusNode: confirmPassFocus,
                        inputAction: TextInputAction.done,
                        onSubmitted: (val) {
                          confirmPassFocus.unfocus();
                        },
                        label: "confirmer mot de passe",
                        hintText: "Confirmez mot de passe de l'utilisateur",
                        validate: (name) => Validator.passwordValidator(name),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      DropdownButtonFormField<String>(
                        value: newUserRole,
                        items: [
                          DropdownMenuItem(
                              value: "user",
                              child: Text(
                                "Utilisateur",
                                style: TextStyle(
                                    color: upgradeBrown, fontSize: 17),
                              )),
                          DropdownMenuItem(
                              value: Config.supervisor,
                              child: Text(
                                "Superviseur",
                                style: TextStyle(
                                    color: upgradeBrown, fontSize: 17),
                              )),
                          DropdownMenuItem(
                              value: Config.administrator,
                              child: Text(
                                "Administrateur",
                                style: TextStyle(
                                    color: upgradeBrown, fontSize: 17),
                              )),
                          DropdownMenuItem(
                              value: Config.superAdministrator,
                              child: Text(
                                "Super Administrateur",
                                style: TextStyle(
                                    color: upgradeBrown, fontSize: 17),
                              )),
                        ],
                        onChanged: (val) {
                          print(val);
                          setState(() {
                            newUserRole = val;
                            if (val == Config.supervisor) {
                              showUserList = true;
                            } else {
                              if (supervisorTeamMembers.isNotEmpty) {
                                supervisorTeamMembers = [];
                              }
                              if (supervisorTeamMembersIds.isNotEmpty) {
                                supervisorTeamMembersIds = [];
                              }
                            }
                          });
                        },
                        decoration: InputDecoration(
                          prefixIcon: Icon(
                            Icons.supervised_user_circle,
                            color: upgradeBrown,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:
                                BorderSide(color: upgradeBrown, width: 2),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(color: upgradeBrown),
                          ),
                        ),
                      ),
                      newUserRole != Config.supervisor
                          ? SizedBox(
                              height: 30,
                            )
                          : Container(),
                      newUserRole == Config.user
                          ? DropdownButtonFormField<int>(
                              value: supervisor,
                              items: _generateSupervisorMenuItems(context),
                              onChanged: (val) {
                                print(val);
                                setState(() {
                                  supervisor = val;
                                });
                              },
                              decoration: InputDecoration(
                                labelText:
                                    supervisor != null ? "Superviseur" : "",
                                hintText: "Choisir un Superviseur",
                                hintStyle: TextStyle(
                                    color: upgradeBrown, fontSize: 15),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide:
                                      BorderSide(color: upgradeBrown, width: 2),
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide(color: upgradeBrown),
                                ),
                              ),
                            )
                          : Container(),
                      SizedBox(
                        height: 10,
                      ),
                      supervisorTeamMembers != [] &&
                              newUserRole == Config.supervisor
                          ? Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(left: 10),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Text(
                                            "Membres d'équipe:",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16,
                                                color: upgradeBrown),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          InkWell(
                                            child: Icon(
                                              Icons.edit,
                                              color: Colors.green,
                                            ),
                                            onTap: () {
                                              setState(() {
                                                showUserList = true;
                                              });
                                            },
                                          )
                                        ],
                                      ),
                                    )
                                  ] +
                                  _generateSelectedUsersList(
                                      supervisorTeamMembers, (user) {
                                    setState(() {
                                      supervisorTeamMembers.removeWhere(
                                          (member) =>
                                              member.userId == user.userId);

                                      supervisorTeamMembersIds.removeWhere(
                                          (id) => id == user.userId);
                                    });
                                  }),
                            )
                          : Container(),
                      SizedBox(
                        height: 30,
                      ),
                      !signUpLoading
                          ? Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal:
                                      MediaQuery.of(context).size.width * 0.2),
                              child: RaisedButton(
                                onPressed: () async {
                                  if (passwordController.text ==
                                      confirmPasswordController.text) {
                                    if (formKey.currentState.validate()) {
                                      FocusScopeNode currentFocus =
                                          FocusScope.of(context);

                                      if (!currentFocus.hasPrimaryFocus) {
                                        currentFocus.unfocus();
                                      }
                                      setState(() {
                                        signUpLoading = true;
                                      });

                                      final user =
                                          await Provider.of<AdminTaskProvider>(
                                                  context,
                                                  listen: false)
                                              .addNewUser(
                                                  nameController.text,
                                                  passwordController.text,
                                                  newUserRole,
                                                  supervisor,
                                                  context);

                                      //if the created user is a normal user and he was assigned a supervisor
                                      //we add him to the selected supervisor's team
                                      if (supervisor != null) {
                                        await Provider.of<AdminTaskProvider>(
                                                context,
                                                listen: false)
                                            .addTeamMembers(supervisor,
                                                [user.userId], context, true);
                                      }

                                      if (user != null) {
                                        nameController.clear();
                                        emailController.clear();
                                        passwordController.clear();
                                        confirmPasswordController.clear();

                                        Provider.of<AdminTaskProvider>(context,
                                                listen: false)
                                            .currentUsers
                                            .add(user);

                                        //else if the created user is a supervisor, if he was assigned team members,
                                        //we add these team members to his team
                                        if (supervisorTeamMembersIds != [] &&
                                            newUserRole == Config.supervisor) {
                                          print(
                                              "adding supervisor(${user.userId}) to ......-> $supervisorTeamMembers");
                                          await Provider.of<AdminTaskProvider>(
                                                  context,
                                                  listen: false)
                                              .addTeamMembers(
                                                  user.userId,
                                                  supervisorTeamMembersIds,
                                                  context,
                                                  false);
                                        }

                                        await Provider.of<AdminTaskProvider>(
                                                context,
                                                listen: false)
                                            .getAllUsers(widget.adminId);
                                        setState(() {
                                          signUpLoading = false;
                                        });

                                        Navigator.of(context).pop();
                                      } else {
                                        setState(() {
                                          signUpLoading = false;
                                        });
                                        showDialog(
                                            context: context,
                                            builder: (context) {
                                              return AlertDialog(
                                                title: Text("Erreur"),
                                                content: Text(
                                                    "Une erreur s'est produite lors de la creation du compte"),
                                              );
                                            });
                                      }
                                    } else {
                                      autovalidate = true;
                                      showDialog(
                                          context: context,
                                          builder: (context) {
                                            return AlertDialog(
                                              title: Text("Erreur"),
                                              content: Text(
                                                  "Vérifiez vos informations s'il vous plait"),
                                            );
                                          });
                                    }
                                  } else {
                                    showDialog(
                                        context: context,
                                        builder: (context) {
                                          return AlertDialog(
                                            title: Text("Erreur"),
                                            content: Text(
                                                "Les deux mots de passes ne correspondent pas"),
                                          );
                                        });
                                  }
                                },
                                child: Text(
                                  "Créer",
                                  style: TextStyle(
                                      fontSize: 18, color: Colors.white),
                                ),
                                color: upgradeBrown,
                                padding: EdgeInsets.symmetric(
                                    vertical: 20, horizontal: 40),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                              ),
                            )
                          : Padding(
                              padding: EdgeInsets.only(
                                  left:
                                      MediaQuery.of(context).size.width * 0.415,
                                  right:
                                      MediaQuery.of(context).size.width * 0.415,
                                  bottom: 10),
                              child: CircularProgressIndicator(
                                backgroundColor: upgradeOrange,
                              )),
                      SizedBox(
                        height: 15,
                      )
                    ],
                  ),
                ),
              ),
              showUserList
                  ? UserList(
                      userId: null,
                      userName: nameController.text ?? "",
                      alreadySelectedUsers: this.supervisorTeamMembers,
                      onWillPop: () async {
                        setState(() {
                          showUserList = false;
                        });

                        return false;
                      },
                      externalMode: true,
                      onUsersValidate: null,
                      onTeamMembersAdded: (memberIdList, memberList) {
                        setState(() {
                          supervisorTeamMembersIds = memberIdList;
                          supervisorTeamMembers = memberList;
                          showUserList = false;
                        });
                      },
                    )
                  : Container()
            ],
          ),
        ),
      ),
    );
  }

  List<DropdownMenuItem<int>> _generateSupervisorMenuItems(
      BuildContext context) {
    final supervisors = HelperFunction.filterUsers(Config.supervisor,
        Provider.of<AdminTaskProvider>(context).currentUsersAnnex);

    List<DropdownMenuItem<int>> items = [];

    supervisors.forEach((supervisor) {
      items.add(DropdownMenuItem<int>(
          value: supervisor.userId,
          child: Text(
            supervisor.userName,
            style: TextStyle(color: upgradeBrown, fontSize: 18),
          )));
    });

    return [
          DropdownMenuItem<int>(
              value: null,
              child: Text(
                "Aucun Superviseur",
                style: TextStyle(color: upgradeBrown, fontSize: 18),
              ))
        ] +
        items;
  }

  List<Widget> _generateSelectedUsersList(
      List<User> selectedUsers, Function(User user) onUserRemove) {
    List<Widget> usersWidgets = [];

    selectedUsers.forEach((user) {
      usersWidgets.add(Padding(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 7),
        child: Row(
          // mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              color: upgradeBrown,
              width: 7,
              height: 2,
            ),
            SizedBox(
              width: 5,
            ),
            Text(
              user.userName,
              style: TextStyle(color: upgradeBrown, fontSize: 16),
            ),
            SizedBox(
              width: 10,
            ),
            InkWell(
              child: Icon(
                Icons.cancel,
                color: Colors.red,
                size: 20,
              ),
              onTap: () {
                onUserRemove(user);
              },
            )
          ],
        ),
      ));
    });

    return usersWidgets;
  }
}
