import 'package:flutter/material.dart';

import 'package:loading_animations/loading_animations.dart';
import 'package:provider/provider.dart';
import 'package:upgrade_to_do/components/searchBar.dart';
import 'package:upgrade_to_do/components/userTile.dart';
import 'package:upgrade_to_do/main.dart';
import 'package:upgrade_to_do/providers/adminTaskProvider.dart';
import 'package:upgrade_to_do/providers/formProvider.dart';
import 'package:upgrade_to_do/screens/aboutScreen.dart';
import 'package:upgrade_to_do/screens/adminScreens/newUserScreen.dart';
import 'package:upgrade_to_do/screens/helpScreen.dart';
import 'package:upgrade_to_do/screens/loginOrSignup/loginOrSignupScreen.dart';
import 'package:upgrade_to_do/screens/summaryScreen.dart';
import 'package:upgrade_to_do/utils/Config.dart';
import 'package:upgrade_to_do/utils/helperFunctions.dart';
import 'package:upgrade_to_do/utils/settings.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:upgrade_to_do/components/scrollWidget.dart';

class SuperAdminHome extends StatefulWidget {
  SuperAdminHome({this.adminName, this.adminId, this.settings});

  final String adminName;
  final int adminId;

  final Settings settings;

  @override
  _SuperAdminHomeState createState() => _SuperAdminHomeState();
}

class _SuperAdminHomeState extends State<SuperAdminHome> {
  TextEditingController searchController;
  dynamic searchBy;
  FocusNode searchFocusNode;
  String roleToShow;
  RefreshController controller = new RefreshController(initialRefresh: false);
  ScrollController scrollController;
  bool scrollingDown;
  double referencePos;
  bool scrollable;
  @override
  void initState() {
    searchController = new TextEditingController();
    scrollController = new ScrollController();

    searchBy = "name";
    searchFocusNode = new FocusNode();
    roleToShow = "all";
    scrollingDown = true;
    referencePos = 0;
    scrollable = false;

    scrollController.addListener(() {
      final newPos = scrollController.offset;

      if (newPos > referencePos) {
        scrollingDown = true;
      } else if (newPos < referencePos) {
        scrollingDown = false;
      }

      setState(() {
        referencePos = newPos;
        if (scrollable == false && scrollController.offset > 0) {
          scrollable = true;
        }

        if (scrollController.offset >=
                scrollController.position.maxScrollExtent &&
            !scrollController.position.outOfRange) {
          scrollingDown = false;
        } else if (scrollController.offset <=
                scrollController.position.minScrollExtent &&
            !scrollController.position.outOfRange) {
          scrollingDown = true;
        }
      });
    });

    super.initState();
  }

  @override
  void dispose() {
    searchController?.dispose();
    searchFocusNode?.dispose();
    scrollController?.dispose();
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final tempUsers = Provider.of<AdminTaskProvider>(context)
        .currentUsers
        .where((user) => (user.role != Config.superAdministrator))
        .toList();

    final users = HelperFunction.filterUsers(roleToShow, tempUsers);
    return Scaffold(
      appBar: AppBar(
        title: Text("Acceuil"),
        actions: <Widget>[
          SafeArea(
              child: PopupMenuButton<String>(
                  padding: EdgeInsets.zero,
                  offset: Offset(-100, 0),
                  icon: Icon(
                    Icons.more_vert,
                    size: 30,
                  ),
                  itemBuilder: (context) => [
                        PopupMenuItem(
                            value: "roleToShow",
                            child: PopupMenuButton<String>(
                                onSelected: (value) {
                                  setState(() {
                                    print(value);
                                    roleToShow = value;
                                  });

                                  Navigator.of(context).pop();
                                },
                                onCanceled: () {
                                  Navigator.of(context).pop();
                                },
                                child: Text("Trier par",
                                    style: TextStyle(
                                        // backgroundColor: Colors.red,
                                        color: upgradeBrown,
                                        fontSize: 18)),
                                initialValue: roleToShow,
                                itemBuilder: (context) => [
                                      PopupMenuItem(
                                          value: "all",
                                          child: Text(
                                            "Tous",
                                            style: TextStyle(
                                                color: upgradeBrown,
                                                fontSize: 18),
                                          )),
                                      PopupMenuDivider(),
                                      PopupMenuItem(
                                          value: "user",
                                          child: Text(
                                            "Utilisateurs",
                                            style: TextStyle(
                                                color: upgradeBrown,
                                                fontSize: 18),
                                          )),
                                      PopupMenuDivider(),
                                      PopupMenuItem(
                                          value: Config.supervisor,
                                          child: Text(
                                            "Superviseurs",
                                            style: TextStyle(
                                                color: upgradeBrown,
                                                fontSize: 18),
                                          )),
                                      PopupMenuDivider(),
                                      PopupMenuItem(
                                          value: Config.administrator,
                                          child: Text(
                                            "Administrateurs",
                                            style: TextStyle(
                                                color: upgradeBrown,
                                                fontSize: 18),
                                          )),
                                      PopupMenuDivider(),
                                      PopupMenuItem(
                                          value: Config.superAdministrator,
                                          child: Text(
                                            "Supers Administrateurs",
                                            style: TextStyle(
                                                color: upgradeBrown,
                                                fontSize: 18),
                                          ))
                                    ])),
                        PopupMenuDivider(),
                        PopupMenuItem(
                          value: "searchBy",
                          child: PopupMenuButton<String>(
                            initialValue: searchBy,
                            child: Column(
                              children: <Widget>[
                                Icon(
                                  Icons.youtube_searched_for,
                                  size: 30,
                                  color: upgradeBrown,
                                ),
                                Text(
                                  "Chercher par",
                                  style: TextStyle(
                                      color: upgradeBrown, fontSize: 15),
                                ),
                              ],
                            ),
                            itemBuilder: (context) => [
                              PopupMenuItem(
                                  value: "name",
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Text(
                                        "Nom",
                                        style: TextStyle(fontSize: 18),
                                      ),
                                      Divider(
                                        color: upgradeBrown,
                                      )
                                    ],
                                  )),
                              PopupMenuItem(
                                  value: "id",
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Text(
                                        "Identifiant",
                                        style: TextStyle(fontSize: 18),
                                      ),
                                      Divider(
                                        color: upgradeBrown,
                                      )
                                    ],
                                  )),
                            ],
                            onSelected: (value) {
                              if (value == "name") {
                                setState(() {
                                  searchBy = value;
                                });
                              } else {
                                setState(() {
                                  searchBy = value;
                                });
                              }

                              Navigator.of(context).pop();
                            },
                            onCanceled: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        )
                      ])),
        ],
      ),
      drawer: Drawer(
        child: SafeArea(
          child: Container(
            color: upgradeOrange,
            child: Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(
                      height: 150,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          image: DecorationImage(
                              fit: BoxFit.contain,
                              image: AssetImage("assets/logo.png"))),
                    ),
                    Container(
                      height: 150,
                      alignment: Alignment.center,
                      color: Colors.black.withOpacity(.75),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Icon(
                            Icons.account_circle,
                            color: Colors.white,
                            size: 80,
                          ),
                          Text(
                            "${widget.adminName}",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 30,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Expanded(
                  child: ListView(
                    children: <Widget>[
                      ListTile(
                        leading: Icon(
                          Icons.assessment,
                          color: Colors.white,
                          size: 30,
                        ),
                        title: Text(
                          "Bilan",
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                        onTap: () async {
                          //go to help page
                          Navigator.of(context).pop();

                          await Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => SummaryScreen(
                                  executionRate: null,
                                  adminId: widget.adminId,
                                  admin: true)));
                        },
                      ),
                      Divider(
                        color: Colors.white,
                        thickness: 2,
                      ),
                      ListTile(
                        leading: Icon(
                          Icons.help_outline,
                          color: Colors.white,
                          size: 30,
                        ),
                        title: Text(
                          "Aide",
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                        onTap: () async {
                          //go to help page
                          Navigator.of(context).pop();

                          await Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => HelpScreen()));
                        },
                      ),
                      Divider(
                        color: Colors.white,
                        thickness: 2,
                      ),
                      ListTile(
                        leading: Icon(
                          Icons.info_outline,
                          color: Colors.white,
                          size: 30,
                        ),
                        title: Text(
                          "A Propos",
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                        onTap: () async {
                          //go to about page
                          Navigator.of(context).pop();
                          await Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => AboutScreen()));
                        },
                      ),
                      Divider(
                        color: Colors.white,
                        thickness: 2,
                      ),
                      ListTile(
                        leading: Icon(
                          Icons.exit_to_app,
                          color: Colors.red,
                          size: 30,
                        ),
                        title: Text(
                          "Se déconnecter",
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                        onTap: () async {
                          // implement logout
                          await widget.settings.userName.setValue('default');
                          await widget.settings.password.setValue('default');
                          await Navigator.of(context)
                              .pushReplacement(MaterialPageRoute(
                                  builder: (context) => ChangeNotifierProvider(
                                        create: (context) => FormProvider(),
                                        child: LoginOrSignUp(
                                            settings: widget.settings),
                                      )));
                        },
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => ChangeNotifierProvider(
                  create: (context) => FormProvider(),
                  child: CreateUserScreen(widget.adminId))));
        },
        child: Icon(Icons.group_add),
      ),
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Center(
          child: SmartRefresher(
            controller: controller,
            enablePullDown: true,
            header: WaterDropMaterialHeader(
              backgroundColor: upgradeOrange,
            ),
            onRefresh: () async {
              await Provider.of<AdminTaskProvider>(context, listen: false)
                  .getAllUsers(widget.adminId);

              controller.refreshCompleted();
            },
            child: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 10,
                      ),
                      SearchBar(
                        searchController: searchController,
                        onChange: (value) {
                          Provider.of<AdminTaskProvider>(context, listen: false)
                              .searchUser(value, searchBy, roleToShow);
                        },
                        searchFor: roleToShow == "all"
                            ? "Utilisateurs"
                            : roleToShow == Config.supervisor
                                ? "Superviseurs"
                                : roleToShow == Config.administrator
                                    ? "Administrateurs"
                                    : roleToShow == Config.superAdministrator
                                        ? "Supers Administrateurs"
                                        : "Utilisateurs",
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Expanded(
                        child: ListView.builder(
                            controller: scrollController,
                            itemCount: users.length,
                            itemBuilder: (context, index) => UserListTile(
                                  userId: users[index].userId,
                                  userRole: users[index].role ?? "user",
                                  isSuperAdmin: true,
                                  hasSupervisor:
                                      users[index].supervisor != null,
                                  userName: users[index].userName,
                                  settings: widget.settings,
                                  userIsSupervisor:
                                      users[index].role == Config.supervisor
                                          ? true
                                          : false,
                                  whenTapped: () {
                                    if (searchFocusNode.hasFocus)
                                      searchFocusNode.unfocus();
                                  },
                                )),
                      )
                    ],
                  ),
                ),
                Provider.of<AdminTaskProvider>(context).userLoading
                    ? Container(
                        alignment: Alignment.center,
                        color: Colors.black.withOpacity(.6),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            LoadingBouncingGrid.circle(
                              size: 40,
                              backgroundColor: upgradeOrange,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "Chargement...",
                              style: TextStyle(
                                  color: upgradeOrange,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ))
                    : Container(),
                scrollable == true
                    ? Positioned(
                        bottom: 0,
                        left: 5,
                        child: ScrollWidget(
                            scrollController: scrollController,
                            scrollingDown: scrollingDown,
                            referencePos: referencePos),
                      )
                    : Container()
              ],
            ),
          ),
        ),
      ),
    );
  }
}
