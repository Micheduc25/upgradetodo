import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:upgrade_to_do/components/loadingDialog.dart';
import 'package:upgrade_to_do/components/userList.dart';
import 'package:upgrade_to_do/components/userTile.dart';
import 'package:upgrade_to_do/main.dart';
import 'package:upgrade_to_do/providers/formProvider.dart';
import 'package:upgrade_to_do/providers/supervisorTaskProvider.dart';
import 'package:upgrade_to_do/providers/taskProvider.dart';
import 'package:upgrade_to_do/screens/aboutScreen.dart';
import 'package:upgrade_to_do/screens/helpScreen.dart';
import 'package:upgrade_to_do/screens/loginOrSignup/loginOrSignupScreen.dart';
import 'package:upgrade_to_do/screens/summaryScreen.dart';
import 'package:upgrade_to_do/utils/settings.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class SupervisorScreen extends StatefulWidget {
  SupervisorScreen(
      {@required this.settings,
      @required this.supId,
      this.adminMode = false,
      @required this.userName});
  final Settings settings;
  final String userName;
  final int supId;
  final bool adminMode;

  @override
  _SupervisorScreenState createState() => _SupervisorScreenState();
}

class _SupervisorScreenState extends State<SupervisorScreen> {
  bool loading;
  bool showUserList;
  RefreshController controller = new RefreshController(initialRefresh: false);

  @override
  void initState() {
    loading = false;
    showUserList = false;

    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final data = Provider.of<TaskProvider>(context);
    final teamMembers =
        Provider.of<SupervisorTaskProvider>(context).teamMembers;

    return Scaffold(
        floatingActionButton: widget.adminMode
            ? FloatingActionButton(
                child: Icon(Icons.person_add),
                onPressed: () {
                  setState(() {
                    showUserList = true;
                    print("showUserList is true");
                  });
                })
            : null,
        drawer: showUserList
            ? null
            : Drawer(
                child: SafeArea(
                  child: Container(
                    color: upgradeOrange,
                    child: Column(
                      children: <Widget>[
                        Stack(
                          children: <Widget>[
                            Container(
                              height: 150,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  image: DecorationImage(
                                      fit: BoxFit.contain,
                                      image: AssetImage("assets/logo.png"))),
                            ),
                            Container(
                              height: 150,
                              alignment: Alignment.center,
                              color: Colors.black.withOpacity(.75),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.account_circle,
                                    color: Colors.white,
                                    size: 80,
                                  ),
                                  Text(
                                    "${widget.userName}",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 30,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        Expanded(
                          child: ListView(
                            children: <Widget>[
                              ListTile(
                                leading: Icon(
                                  Icons.playlist_add_check,
                                  color: Colors.white,
                                  size: 30,
                                ),
                                title: Text(
                                  widget.adminMode
                                      ? "Page de ${widget.userName}"
                                      : "Mes Tâches",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                                onTap: () async {
                                  //go to help page
                                  Navigator.of(context).pop();

                                  Navigator.of(context).pop();
                                },
                              ),
                              Divider(
                                color: Colors.white,
                                thickness: 2,
                              ),
                              ListTile(
                                leading: Icon(
                                  Icons.assessment,
                                  color: Colors.white,
                                  size: 30,
                                ),
                                title: Text(
                                  "Bilan de l'Equipe",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                                onTap: () async {
                                  //go to help page
                                  Navigator.of(context).pop();

                                  double rate = 0;
                                  if (data.totalTasks != 0 &&
                                      data.completedTasks != 0) {
                                    rate = double.parse(((data.completedTasks /
                                                data.totalTasks) *
                                            100)
                                        .toStringAsFixed(2));
                                  }

                                  print("rate is $rate");

                                  await Navigator.of(context)
                                      .push(MaterialPageRoute(
                                          builder: (context) => SummaryScreen(
                                                executionRate: rate,
                                              )));
                                },
                              ),
                              Divider(
                                color: Colors.white,
                                thickness: 2,
                              ),
                              ListTile(
                                leading: Icon(
                                  Icons.help_outline,
                                  color: Colors.white,
                                  size: 30,
                                ),
                                title: Text(
                                  "Aide",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                                onTap: () async {
                                  //go to help page
                                  Navigator.of(context).pop();

                                  await Navigator.of(context).push(
                                      MaterialPageRoute(
                                          builder: (context) => HelpScreen()));
                                },
                              ),
                              Divider(
                                color: Colors.white,
                                thickness: 2,
                              ),
                              ListTile(
                                leading: Icon(
                                  Icons.info_outline,
                                  color: Colors.white,
                                  size: 30,
                                ),
                                title: Text(
                                  "A Propos",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                                onTap: () async {
                                  //go to about page
                                  Navigator.of(context).pop();
                                  await Navigator.of(context).push(
                                      MaterialPageRoute(
                                          builder: (context) => AboutScreen()));
                                },
                              ),
                              Divider(
                                color: Colors.white,
                                thickness: 2,
                              ),
                              !widget.adminMode
                                  ? ListTile(
                                      leading: Icon(
                                        Icons.exit_to_app,
                                        color: Colors.red,
                                        size: 30,
                                      ),
                                      title: Text(
                                        "Se déconnecter",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 20),
                                      ),
                                      onTap: () async {
                                        //logout
                                        await widget.settings.userName
                                            .setValue("default");
                                        await widget.settings.password
                                            .setValue("default");
                                        await Navigator.of(context)
                                            .pushReplacement(MaterialPageRoute(
                                                builder: (context) =>
                                                    ChangeNotifierProvider(
                                                      create: (context) =>
                                                          FormProvider(),
                                                      child: LoginOrSignUp(
                                                          settings:
                                                              widget.settings),
                                                    )));
                                      },
                                    )
                                  : ListTile(
                                      leading: Icon(
                                        widget.adminMode
                                            ? Icons.home
                                            : Icons.list,
                                        color: Colors.white,
                                        size: 30,
                                      ),
                                      title: Text(
                                        widget.adminMode
                                            ? "List d'Utilisateurs"
                                            : "Membres de mon équipe",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 20),
                                      ),
                                      onTap: () {
                                        Navigator.of(context).pop();
                                        Navigator.of(context).pop();
                                        Navigator.of(context).pop();
                                      },
                                    )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
        appBar: AppBar(
          title: Text("Superviseur"),
        ),
        body: Container(
          child: Stack(
            children: [
              teamMembers.isEmpty
                  ? Center(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Text(
                          "${widget.userName} n'a pas encore de membres dans son équipe",
                          textAlign: TextAlign.center,
                          style: TextStyle(color: upgradeBrown, fontSize: 19),
                        ),
                      ),
                    )
                  : !loading
                      ? SmartRefresher(
                          controller: controller,
                          enablePullDown: true,
                          enablePullUp: true,
                          header: WaterDropMaterialHeader(
                            backgroundColor: upgradeOrange,
                          ),
                          onRefresh: () async {
                            await Provider.of<SupervisorTaskProvider>(context,
                                    listen: false)
                                .getMyTeamMembers(widget.supId);

                            controller.refreshCompleted();
                          },
                          child: ListView.builder(
                              itemCount: teamMembers.length,
                              itemBuilder: (context, index) {
                                return UserListTile(
                                  whenTapped: () {
                                    showLoadingDialog("Chargement", context);
                                  },
                                  userRole: teamMembers[index].role,
                                  hasSupervisor:
                                      teamMembers[index].supervisor != null,
                                  isSuperAdmin: false,
                                  isAdmin: true,
                                  isSupervisor: true,
                                  supervisorId: widget.supId,
                                  userIsSupervisor: false,
                                  settings: Provider.of<TaskProvider>(context)
                                      .settings,
                                  userId: teamMembers[index].userId,
                                  userName: teamMembers[index].userName,
                                );
                              }),
                        )
                      : CircularProgressIndicator(
                          backgroundColor: upgradeOrange,
                        ),
              showUserList
                  ? UserList(
                      userId: widget.supId,
                      userName: widget.userName,
                      onWillPop: () async {
                        setState(() {
                          showUserList = false;
                        });
                        return false;
                      },
                      externalMode: false,
                      onUsersValidate: () {
                        setState(() {
                          showUserList = false;
                        });
                      },
                      alreadySelectedUsers: null)
                  : Container()
            ],
          ),
        ));
  }
}
