import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:upgrade_to_do/components/executionIndicator.dart';
import 'package:upgrade_to_do/main.dart';

import 'package:upgrade_to_do/models/taskModel.dart';
import 'package:upgrade_to_do/providers/taskProvider.dart';
import 'package:upgrade_to_do/utils/helperFunctions.dart';
import 'package:upgrade_to_do/utils/homeScreenSettings.dart';
import 'package:upgrade_to_do/utils/periods.dart';
import 'package:upgrade_to_do/utils/settings.dart';

class SummaryScreen extends StatefulWidget {
  SummaryScreen(
      {this.title,
      @required this.executionRate,
      this.adminId,
      this.admin = false,
      this.period,
      this.initialTab,
      this.adminMode,
      this.userName,
      this.supId,
      this.hasSupervisor,
      this.isSuperAdmin,
      this.isSupervisor,
      this.offlineMode,
      this.settings,
      this.userId,
      this.userRole,
      this.disableTaskCheck,
      this.isSupervisorMode,
      this.showDrawer}) {}
  final String title;
  final double executionRate;
  final bool admin;
  final int adminId;
  final bool isSupervisorMode;
  final String userName;
  final bool isSuperAdmin;
  final int supId;
  final int userId;
  final Settings settings;
  final bool showDrawer;
  final bool adminMode;
  final bool disableTaskCheck;
  final bool isSupervisor;
  final String userRole;
  final bool offlineMode;
  final bool hasSupervisor;
  final Periods period;
  final int initialTab;

  @override
  _SummaryScreenState createState() => _SummaryScreenState();
}

class _SummaryScreenState extends State<SummaryScreen> {
  bool loading;
  double executionRate;
  Map<String, int> globalStats;
  HomeScreenSettings homeSettings;
  @override
  void initState() {
    print(widget.executionRate);
    print(widget.admin);
    if (widget.executionRate != null && widget.admin == false) {
      loading = false;
      executionRate = widget.executionRate;
    } else {
      loading = true;

      Future.delayed(Duration.zero, () {
        HelperFunction.getTotalStats(context, widget.adminId).then((stats) {
          print(stats);
          setState(() {
            executionRate = double.parse(
                ((stats["completed"] / stats["total"]) * 100)
                    .toStringAsFixed(2));
            globalStats = stats;
            loading = false;
          });
        });
      });
    }

    homeSettings = HomeScreenSettings(
      userId: widget.userId,
      settings: widget.settings,
      userRole: widget.userRole,
      offlineMode: widget.offlineMode,
      hasSupervisor: widget.hasSupervisor,
      adminId: widget.adminId,
      period: widget.period,
      supId: widget.supId,
      isSupervisor: widget.isSupervisor,
      isSupervisorMode: widget.isSupervisorMode,
      userName: widget.userName,
      title: widget.title,
      showDrawer: widget.showDrawer,
      adminMode: widget.adminMode,
      disableTaskCheck: widget.disableTaskCheck,
      isSuperAdmin: widget.isSuperAdmin,
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final List<Task> tasks = Provider.of<TaskProvider>(context).tasks;
    final thisWeekStats = HelperFunction.getTasksStatistics(
        HelperFunction.thisWeeksMonthOrYearTasks(
            HelperFunction.filterTasksByPeriod(Periods.Week, tasks)));

    final thisMonthStats = HelperFunction.getTasksStatistics(
        HelperFunction.thisWeeksMonthOrYearTasks(
            HelperFunction.filterTasksByPeriod(Periods.Month, tasks)));
    final thisYearStats = HelperFunction.getTasksStatistics(
        HelperFunction.thisWeeksMonthOrYearTasks(
            HelperFunction.filterTasksByPeriod(Periods.Year, tasks)));

    return Scaffold(
      appBar: AppBar(
        title: Text(this.widget.title ?? "My Summary"),
      ),
      body: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: loading
            ? CircularProgressIndicator(
                backgroundColor: upgradeBrown,
              )
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: widget.executionRate != null
                      ? <Widget>[
                          ExecutionIndicator(
                              homeSettings: homeSettings,
                              period: null,
                              title: "Taux d'exécution Total",
                              stats: {
                                "total": Provider.of<TaskProvider>(context)
                                    .totalTasks,
                                "completed": Provider.of<TaskProvider>(context)
                                    .completedTasks,
                                "pending": Provider.of<TaskProvider>(context)
                                    .activeTasks
                              },
                              rate: this.widget.executionRate,
                              color: executionRate < 25
                                  ? Colors.red
                                  : executionRate < 50
                                      ? Colors.orange
                                      : executionRate < 75
                                          ? Colors.green[200]
                                          : Colors.green),
                          SizedBox(height: 40),
                          ExecutionIndicator(
                              title: "Taux d'exécution pour cette Semaine",
                              homeSettings: homeSettings,
                              period: Periods.Week,
                              stats: thisWeekStats,
                              rate: double.parse(((thisWeekStats["completed"] /
                                          thisWeekStats["total"]) *
                                      100)
                                  .toStringAsFixed(2)),
                              color: executionRate < 25
                                  ? Colors.red
                                  : executionRate < 50
                                      ? Colors.orange
                                      : executionRate < 75
                                          ? Colors.green[200]
                                          : Colors.green),
                          SizedBox(height: 40),
                          ExecutionIndicator(
                              title: "Taux d'exécution pour ce Mois",
                              homeSettings: homeSettings,
                              period: Periods.Month,
                              stats: thisMonthStats,
                              rate: double.parse(((thisMonthStats["completed"] /
                                          thisMonthStats["total"]) *
                                      100)
                                  .toStringAsFixed(2)),
                              color: executionRate < 25
                                  ? Colors.red
                                  : executionRate < 50
                                      ? Colors.orange
                                      : executionRate < 75
                                          ? Colors.green[200]
                                          : Colors.green),
                          SizedBox(height: 40),
                          ExecutionIndicator(
                              title: "Taux d'exécution pour cette Année",
                              homeSettings: homeSettings,
                              period: Periods.Year,
                              stats: thisYearStats,
                              rate: double.parse(((thisYearStats["completed"] /
                                          thisYearStats["total"]) *
                                      100)
                                  .toStringAsFixed(2)),
                              color: executionRate < 25
                                  ? Colors.red
                                  : executionRate < 50
                                      ? Colors.orange
                                      : executionRate < 75
                                          ? Colors.green[200]
                                          : Colors.green),
                        ]
                      : <Widget>[
                          ExecutionIndicator(
                              homeSettings: homeSettings,
                              period: null,
                              title: "Taux d'exécution total de l'entreprise",
                              stats: globalStats,
                              rate: executionRate,
                              color: executionRate < 25
                                  ? Colors.red
                                  : executionRate < 50
                                      ? Colors.orange
                                      : executionRate < 75
                                          ? Colors.green[200]
                                          : Colors.green),
                        ],
                ),
              ),
      ),
    );
  }
}
